<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login/index');
// });
Route::get('/', 'Dashboard\DashboardController@index');
Auth::routes();
Route::get('/home', 'Dashboard\DashboardController@index')->name('home');
Route::get('daftar', 'Daftar\DaftarController@index');
Route::post('daftar/simpan', 'Daftar\DaftarController@simpan');
Route::get('daftar/refreshCaptcha', 'Daftar\DaftarController@refreshCaptcha');
Route::post('daftar/get_lingkungan', 'Daftar\DaftarController@get_lingkungan');
Route::get('daftar/get_lingkungan', 'Daftar\DaftarController@get_lingkungan');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('api_ticket/{id}', 'ApiController@api_ticket')->name('api_ticket');

Route::group(['middleware' => 'auth'], function () {
	Route::group(['namespace'=> 'Dashboard'], function(){
		Route::resource('dashboard', 'DashboardController');
		Route::get('dashboard_show', 'DashboardController@show')->name('dashboard_show');
		Route::post('dashboard_grafik','DashboardController@get_grafik')->name('dashboard_grafik');
	});

	Route::group(['namespace'=> 'Booking'], function(){
		Route::resource('booking', 'BookingController');
		Route::post('booking_seats', 'BookingController@seats')->name('booking_seats');
		Route::post('booking_idseats', 'BookingController@idseats')->name('booking_idseats');
		// Route::get('booking_seats', 'BookingController@seats')->name('booking_seats');
		Route::post('booking_jadwal', 'BookingController@get_jadwal')->name('booking_jadwal');
		Route::post('booking_jadwal_tanggal', 'BookingController@get_jadwal_tanggal')->name('booking_jadwal_tanggal');
		Route::post('booking_jadwal_jam', 'BookingController@get_jadwal_jam')->name('booking_jadwal_jam');
		Route::post('booking_simpan','BookingController@simpan')->name('booking_simpan');
		Route::get('booking_coba','BookingController@index2')->name('booking_coba');
		Route::post('booking_member','BookingController@get_member')->name('booking_member');
		Route::post('booking_get_area','BookingController@get_area')->name('booking_get_area');

		// Route::resource('booking_scan', 'ScanController');	
		Route::get('booking_scan','ScanController@index');	
		Route::get('booking_scan/{gate}','ScanController@index');
		Route::post('booking_scan/get_data', 'ScanController@get_data')->name('booking_scan/get_data');
		Route::post('booking_scan/simpan', 'ScanController@simpan')->name('booking_scan/simpan');
		Route::post('booking_scan/simpan2', 'ScanController@simpan2')->name('booking_scan/simpan2');
		Route::get('booking_scan/show/{booking}/{gate}','ScanController@show')->name('booking_scan/show');

		Route::get('riwayat', 'RiwayatController@index')->name('riwayat');
		Route::get('riwayat/get_data', 'RiwayatController@get_data')->name('riwayat/get_data');
		Route::post('riwayat/get_ticket','RiwayatController@get_ticket')->name('riwayat/get_ticket');
		Route::post('riwayat/download','RiwayatController@download')->name('riwayat/download');
		Route::get('riwayat/download2/{id}','RiwayatController@download2')->name('riwayat/download2');
		Route::get('riwayat/cetak/{id}','RiwayatController@cetak')->name('riwayat/cetak');
		Route::get('riwayat/cek/{id}','RiwayatController@get_data_kursi')->name('riwayat/cek');
		Route::post('riwayat/historylog','RiwayatController@historylog')->name('riwayat/historylog');
	});

	Route::group(['namespace'=> 'Show'], function(){
		Route::get('lihat_show/get_data', 'ShowController@get_data');
		Route::post('lihat_show/get_data', 'ShowController@get_data');
		Route::get('lihat_show/detail/{id}/{tgl}', 'ShowController@detail');
		Route::post('lihat_show/get_duduk', 'ShowController@get_duduk');
		Route::post('lihat_show/get_detail_duduk', 'ShowController@get_detail_duduk');
		Route::get('lihat_show/get_member', 'ShowController@get_member');
		Route::post('lihat_show/simpan', 'ShowController@simpan');
		Route::post('lihat_show/get_lingkungan', 'ShowController@get_lingkungan');
		Route::post('lihat_show/get_area', 'ShowController@get_area');
		Route::resource('lihat_show', 'ShowController');

		Route::get('verifikasi_daftar', 'VerifikasiController@index');
		Route::get('verifikasi_daftar/get_data', 'VerifikasiController@get_data');
		Route::post('verifikasi_daftar/check', 'VerifikasiController@check');
		Route::post('verifikasi_daftar/get_detail', 'VerifikasiController@get_detail');

		Route::get('ots/get_data', 'OtsController@get_data');
		Route::post('ots/get_data', 'OtsController@get_data');
		Route::get('ots/detail/{id}/{tgl}', 'OtsController@detail');
		Route::post('ots/get_duduk', 'OtsController@get_duduk');
		Route::post('ots/get_detail_duduk', 'OtsController@get_detail_duduk');
		Route::get('ots/get_member', 'OtsController@get_member');
		Route::post('ots/simpan', 'OtsController@simpan');
		Route::post('ots/get_lingkungan', 'OtsController@get_lingkungan');
		Route::post('ots/get_area', 'OtsController@get_area');
		Route::post('ots/get_ticket', 'OtsController@get_ticket');
		Route::get('ots/download2/{id}', 'OtsController@download2');
		Route::post('ots/historylog', 'OtsController@historylog');
		Route::resource('ots', 'OtsController');

		Route::get('jadwal_petugas', 'JadwalpetugasController@index')->name('jadwal_petugas');
		Route::get('jadwal_petugas_show', 'JadwalpetugasController@show')->name('jadwal_petugas_show');
		Route::post('jadwal_petugas_simpan', 'JadwalpetugasController@simpan')->name('jadwal_petugas_simpan');
		Route::post('jadwal_petugas_hapus', 'JadwalpetugasController@hapus')->name('jadwal_petugas_hapus');
		Route::post('jadwal_petugas/get_jadwal', 'JadwalpetugasController@get_jadwal')->name('jadwal_petugas/get_jadwal');
		Route::get('jadwal_petugas/get_jadwal', 'JadwalpetugasController@get_jadwal')->name('jadwal_petugas/get_jadwal');
	});

	Route::group(['namespace'=> 'Master'], function(){
		Route::resource('seat', 'SeatController');
		Route::get('seat_show', 'SeatController@show')->name('seat_show');
		Route::post('seat_hapus', 'SeatController@hapus')->name('seat_hapus');
		Route::post('seat_simpan', 'SeatController@simpan')->name('seat_simpan');
		Route::get('seat_abjad', 'SeatController@abjad')->name('seat_abjad');
		Route::get('seat_ruang/{id}', 'SeatController@get_ruang')->name('seat_ruang');
		Route::get('seat_area/{id}', 'SeatController@get_area')->name('seat_area');

		Route::get('provinsi', 'ProvinsiController@index')->name('provinsi');
		Route::get('provinsi/get_data', 'ProvinsiController@get_data')->name('provinsi/get_data');
		Route::post('provinsi/simpan', 'ProvinsiController@simpan')->name('provinsi/simpan');
		Route::post('provinsi/hapus', 'ProvinsiController@hapus')->name('provinsi/hapus');

		Route::get('area', 'AreaController@index')->name('area');
		Route::get('area/get_data', 'AreaController@get_data')->name('get_data');
		Route::post('area/get_ruang', 'AreaController@get_ruang')->name('get_ruang');
		Route::post('area/simpan', 'AreaController@simpan')->name('simpan');
		Route::post('area/get_edit', 'AreaController@get_edit')->name('get_edit');
		Route::post('area/hapus', 'AreaController@hapus')->name('hapus');

		Route::get('jadwal', 'JadwalController@index')->name('jadwal');
		Route::get('jadwal_ruang/{id}', 'JadwalController@get_ruang')->name('jadwal_ruang');
		Route::post('jadwal_simpan', 'JadwalController@simpan')->name('jadwal_simpan');
		Route::post('jadwal_hapus', 'JadwalController@hapus')->name('jadwal_hapus');
		Route::get('jadwal_show', 'JadwalController@show')->name('jadwal_show');
		Route::post('jadwal_lingkungan','JadwalController@get_lingkungan')->name('jadwal_lingkungan');

		Route::get('lokasi', 'LokasiController@index')->name('lokasi');
		Route::post('lokasi/get_kabupaten', 'LokasiController@get_kabupaten')->name('lokasi/get_kabupaten');
		Route::post('lokasi/get_kecamatan', 'LokasiController@get_kecamatan')->name('lokasi/get_kecamatan');
		Route::post('lokasi/get_kelurahan', 'LokasiController@get_kelurahan')->name('lokasi/get_kelurahan');
		Route::post('lokasi/simpan', 'LokasiController@simpan')->name('lokasi/simpan');
		Route::get('lokasi/get_data', 'LokasiController@get_data')->name('lokasi/get_data');
		Route::post('lokasi/hapus', 'LokasiController@hapus')->name('lokasi/hapus');
		Route::post('lokasi/get_edit', 'LokasiController@get_edit')->name('lokasi/get_edit');
		Route::post('lokasi/get_parents','LokasiController@get_parents')->name('lokasi/get_parents');

		Route::get('wilayah', 'WilayahController@index')->name('wilayah');
		Route::get('wilayah/get_data', 'WilayahController@get_data')->name('wilayah/get_data');
		Route::post('wilayah/get_kabupaten', 'WilayahController@get_kabupaten')->name('wilayah/get_kabupaten');
		Route::post('wilayah/get_kecamatan', 'WilayahController@get_kecamatan')->name('wilayah/get_kecamatan');
		Route::post('wilayah/get_kelurahan', 'WilayahController@get_kelurahan')->name('wilayah/get_kelurahan');
		Route::post('wilayah/simpan', 'WilayahController@simpan')->name('wilayah/simpan');
		Route::post('wilayah/get_edit', 'WilayahController@get_edit')->name('wilayah/get_edit');
		Route::post('wilayah/hapus', 'WilayahController@hapus')->name('wilayah/hapus');

		Route::get('lingkungan', 'LingkunganController@index')->name('lingkungan');
		Route::get('lingkungan/get_data', 'LingkunganController@get_data')->name('lingkungan/get_data');
		Route::post('lingkungan/get_kabupaten', 'LingkunganController@get_kabupaten')->name('lingkungan/get_kabupaten');
		Route::post('lingkungan/get_kecamatan', 'LingkunganController@get_kecamatan')->name('lingkungan/get_kecamatan');
		Route::post('lingkungan/get_kelurahan', 'LingkunganController@get_kelurahan')->name('lingkungan/get_kelurahan');
		Route::post('lingkungan/simpan', 'LingkunganController@simpan')->name('lingkungan/simpan');
		Route::post('lingkungan/get_edit', 'LingkunganController@get_edit')->name('lingkungan/get_edit');
		Route::post('lingkungan/hapus', 'LingkunganController@hapus')->name('lingkungan/hapus');


		Route::get('member', 'MemberController@index')->name('member');
		Route::get('member/get_data', 'MemberController@get_data')->name('member/get_data');
		Route::post('member/simpan', 'MemberController@simpan')->name('member/simpan');
		Route::post('member/get_edit', 'MemberController@get_edit')->name('member/get_edit');
		Route::post('member/hapus', 'MemberController@hapus')->name('member/hapus');
		Route::post('member/get_kabupaten', 'MemberController@get_kabupaten')->name('member/get_kabupaten');
		Route::post('member/get_kecamatan', 'MemberController@get_kecamatan')->name('member/get_kecamatan');
		Route::post('member/get_kelurahan', 'MemberController@get_kelurahan')->name('member/get_kelurahan');
		Route::post('member/get_lingkungan', 'MemberController@get_lingkungan');

		Route::get('ruang', 'RuangController@index')->name('ruang');
		Route::get('ruang/get_data', 'RuangController@get_data')->name('ruang/get_data');
		Route::post('ruang/simpan', 'RuangController@simpan')->name('ruang/simpan');
		Route::post('ruang/get_edit', 'RuangController@get_edit')->name('ruang/get_edit');
		Route::post('ruang/hapus', 'RuangController@hapus')->name('ruang/hapus');

		//MASTER RANGE TANGGAL
		Route::get('rangetanggal','RangetanggalController@index')->name('rangetanggal');
		Route::get('rangetanggal/get_data','RangetanggalController@get_data')->name('rangetanggal/get_data');
		Route::post('rangetanggal/simpan','RangetanggalController@simpan')->name('rangetanggal/simpan');
	});

	Route::group(['namespace'=> 'Superadmin'], function(){
		Route::get('grup', 'GrupController@index')->name('grup');
		Route::get('grup/get_data', 'GrupController@get_data')->name('get_data');
		Route::post('grup/simpan', 'GrupController@simpan')->name('simpan');
		Route::post('grup/hapus', 'GrupController@hapus')->name('hapus');

		Route::get('user', 'UserController@index')->name('user');
		Route::get('user/get_data', 'UserController@get_data')->name('get_data');
		Route::get('user/get_member', 'UserController@get_member')->name('get_member');
		Route::post('user/simpan', 'UserController@simpan')->name('simpan');
		Route::post('user/get_edit', 'UserController@get_edit')->name('get_edit');
		Route::post('user/hapus', 'UserController@hapus')->name('hapus');

		Route::get('menu', 'MenuController@index')->name('menu');
		Route::get('menu/get_data', 'MenuController@get_data')->name('menu/get_data');
		Route::get('menu/get_data2', 'MenuController@get_data2')->name('menu/get_data2');
		Route::post('menu/simpan', 'MenuController@simpan')->name('menu/simpan');
		Route::post('menu/hapus', 'MenuController@hapus')->name('menu/hapus');

		Route::get('grup/detail/{id}', 'GrupController@detail');
		Route::post('grup/get_data_detail', 'GrupController@get_data_detail');
		Route::post('grup/detail/hapus', 'GrupController@hapus_detail');
		Route::post('grup/detail/get_menu', 'GrupController@get_menu');
		Route::get('grup/detail/get_menu/{id}', 'GrupController@get_menu');
		Route::post('grup/detail/simpan', 'GrupController@simpan_menu');
	});

	Route::group(['namespace'=> 'History'], function(){
		Route::get('history', 'HistorylogController@index')->name('history');
		Route::get('history/get_data', 'HistorylogController@get_data')->name('history/get_data');
	});

	Route::group(['namespace'=> 'Akun'], function(){
		Route::get('profil', 'ProfilController@index');
		Route::get('profil/edit', 'ProfilController@edit');
		Route::get('profil/get_lingkungan', 'ProfilController@get_lingkungan');
		Route::post('profil/get_lingkungan', 'ProfilController@get_lingkungan');
		Route::post('profil/simpan', 'ProfilController@simpan');

		Route::get('rubah_password', 'ProfilController@rubah_password')->name('rubah_password');
		Route::post('rubah_password/simpan', 'ProfilController@simpan_password');
	});

	Route::group(['namespace'=> 'Laporan'], function(){
		Route::get('laporan', 'LaporanController@index');
		Route::get('laporan/get_data', 'LaporanController@get_data');
		Route::post('laporan/get_gate', 'LaporanController@get_gate');
		Route::post('laporan/get_area', 'LaporanController@get_area');
		Route::get('laporan/laporan_pengunjung', 'LaporanController@laporan_pengunjung');
		Route::post('laporan/get_jam', 'LaporanController@get_jam');
	});
});