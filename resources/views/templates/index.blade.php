<?php date_default_timezone_set('Asia/Jakarta'); ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title')</title>
    
    <link rel="apple-touch-icon" href="{{ asset('themeforest/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstrap-extend.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/css/site.min.css') }}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/animsition/animsition.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/asscrollable/asScrollable.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/switchery/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/intro-js/introjs.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/slidepanel/slidePanel.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/flag-icon-css/flag-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/custom_login.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css') }}">
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/formvalidation/formValidation.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components.min.css') }}">

    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/magnific-popup/magnific-popup.css')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/ionicons/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/web-icons/web-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/brand-icons/brand-icons.min.css') }}">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <script src="{{ asset('themeforest/global/vendor/jquery/jquery.js') }}"></script>
    
    @yield('css')
    <!--[if lt IE 9]>
    <script src="{{ asset('themeforest/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="{{ asset('themeforest/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/vendor/breakpoints/breakpoints.js') }}"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition site-menubar-unfold site-menubar-keep">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    
      <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
          data-toggle="menubar">
          <span class="sr-only">Toggle navigation</span>
          <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
          data-toggle="collapse">
          <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle">
          <img class="navbar-brand-logo" src="{{ asset('images/logo_full.png') }}" title="Lenggah">
          
        </div>
      </div>
    
      <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
          <!-- Navbar Toolbar -->
          <ul class="nav navbar-toolbar">
            <li class="nav-item hidden-float" id="toggleMenubar">
              <a class="nav-link" data-toggle="menubar" href="#" role="button">
                <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
              </a>
            </li>
            <li class="nav-item hidden-sm-down" id="toggleFullscreen">
              <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                <span class="sr-only">Toggle fullscreen</span>
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar -->
          @php
          $id_user = Auth::user()['id'];
          $id_member = Auth::user()['id_member'];

          $data['member'] = _memberuser()->where(['m.id' => $id_member, "ua.id" => $id_user])->get()->first();
          $image = '';
          if(isset($data['member']->foto)){
            if($data['member']->foto != ''){
              $image = asset('images/member')."/".$data['member']->foto;  
            }else{
              $image = asset('images/no-image.png');
              }
            }
          @endphp
          <!-- Navbar Toolbar Right -->
          <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
            <li class="nav-item dropdown">
              <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                data-animation="scale-up" role="button">
                <span class="avatar avatar-online">
                  <img src="{{ $image }}" alt="...">
                  <i></i>
                </span>
              </a>
              <div class="dropdown-menu" role="menu">
                <a class="dropdown-item" href="{{ url("profil") }}" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Profil</a>
                
              </div>
            </li>
          </ul>
          <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
      </div>
    </nav>    

	@include('templates.menu')
    <!-- Page -->
    <div class="page">
      @yield('content')
    </div>
    <!-- End Page -->

    <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© {{ date("Y") }} Powered by AG SATU. All RIGHT RESERVED.</div>
    </footer>
    <!-- End Footer -->

    <!-- Core  -->
    <script src="{{ asset('themeforest/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/popper-js/umd/popper.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/animsition/animsition.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
    
    <!-- Plugins -->
    <script src="{{ asset('themeforest/global/vendor/switchery/switchery.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/intro-js/intro.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/screenfull/screenfull.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/datatables.net-responsive/dataTables.responsive.js') }}"></script>
    
    <script src="{{ asset('themeforest/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->
    <script src="{{ asset('themeforest/global/vendor/select2/select2.full.min.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
    <script src="{{ asset('themeforest/seat/jquery.seat-charts.js')}}"></script>
    <script src="{{ asset('themeforest/seat/jquery.seat-charts.min.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/formvalidation/formValidation.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/formvalidation/framework/bootstrap4.min.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap-sweetalert/sweetalert.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/jquery_ui/core.min.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/trees/fancytree_all.min.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/trees/fancytree_childcounter.js')}}"></script>

    <script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
    <script src="{{ asset('vendor/loadingoverlay/loadingoverlay.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/js/Component.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Base.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Config.js') }}"></script>
    
    <script src="{{ asset('themeforest/js/Section/Menubar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/GridMenu.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/Sidebar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/PageAside.js') }}"></script>
    <script src="{{ asset('themeforest/js/Plugin/menu.js') }}"></script>
    
    <script src="{{ asset('themeforest/global/js/config/colors.js') }}"></script>
    <script src="{{ asset('themeforest/js/config/tour.js') }}"></script>
    <script>Config.set('assets', "{{asset('themeforest')}}");</script>
    
    <!-- Page -->
    <script src="{{ asset('themeforest/js/Site.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/asscrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/slidepanel.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/switchery.js') }}"></script>
        <script src="{{ asset('themeforest/global/js/Plugin/jquery-placeholder.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/material.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/magnific-popup.js') }}"></script>
    <script src="{{ asset('js/lightbox.js')}}"></script>
    
    @yield('js')
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();

          $('.date_fromtoday').datepicker({
            format : 'dd-mm-yyyy',
            setDate: new Date(),
            endDate: new Date(),
            autoclose: true,
            clearBtn: true,
          });

        });
      })(document, window, jQuery);

      // $(document).ajaxStart(function(){
      //   $.LoadingOverlay("show",{
      //     // image : "{{ asset('public/admin/plugins/tumblr-spinner/dist/cogs/cog07.svg') }}",
      //     imageAnimation  : "rotate_right",
      //     imageColor : "#3c8dbc"
      //   });
      // }).ajaxStop(function(){
      //    $.LoadingOverlay("hide");
      // })

    </script>
  </body>
</html>
