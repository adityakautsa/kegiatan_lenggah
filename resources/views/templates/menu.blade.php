@php
$menu = menu_item();
$modul = get_role();
$uri_segement = Request::segment(1);

$c_akses = get_akses($uri_segement);
if($c_akses == 0){
  if(!in_array($uri_segement, exclude_menu())){
    echo '<script>window.location = "'.url('home').'";</script>';
  }
}
@endphp
  <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">Main Navigation</li>
              @php echo menu_create_limitless($menu, $modul, 'sidebar-menu', $uri_segement) @endphp
              <li class="site-menu-item has-sub">
                  <a href="{{ route('logout') }}" id="btn-logout" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="site-menu-icon fa fa-sign-out"></i> <span class="site-menu-title">Keluar</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
              </li>
            </ul>
            </div>
        </div>
      </div>
  </div>