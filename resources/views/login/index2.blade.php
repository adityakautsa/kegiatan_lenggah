
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Login Ticketing</title>
    
    <link rel="apple-touch-icon" href="{{ asset('themeforest/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('themeforest/images/favicon.ico') }}">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstra') }}p.min.css">
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstrap-exten') }}d.min.css">
    <link rel="stylesheet" href="{{ asset('themeforest/css/site.min.css') }}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/animsition/animsition.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/asscrollable/asScrollable.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/switchery/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/intro-js/introjs.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/slidepanel/slidePanel.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/flag-icon-css/flag-icon.css') }}">
        <link rel="stylesheet" href="{{ asset('themeforest/custom_login.css') }}">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/web-icons/web-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/brand-icons/brand-icons.min.css') }}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="{{ asset('themeforest/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="{{ asset('themeforest/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/vendor/breakpoints/breakpoints.js') }}"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-login-v3 layout-full">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
      <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <div class="panel">
          <div class="panel-body">
            <div class="brand">
              <img class="brand-img" src="{{ asset('themeforest/images/logo-colored.png') }}" alt="...">
              <h2 class="brand-text font-size-18">LOGIN 2222222222222</h2>
            </div>
            <form method="post" action="{{ route('login') }}">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="text" class="form-control" name="username" />
                <label class="floating-label">Username</label>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control" name="password" />
                <label class="floating-label">Password</label>
              </div>
              <!-- <div class="form-group clearfix">
                <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                  <input type="checkbox" id="inputCheckbox" name="remember">
                  <label for="inputCheckbox">Remember me</label>
                </div>
                <a class="float-right" href="forgot-password.html">Forgot password?</a>
              </div> -->
              <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
            </form>
            <div class="pull-right">
                    <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="icon ion-power"></i>
                    Sign Out
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
          </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
          <p>WEBSITE BY Creation Studio</p>
          <p>© 2018. All RIGHT RESERVED.</p>
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
          </div>
        </footer>
      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="{{ asset('themeforest/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/popper-js/umd/popper.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/animsition/animsition.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
    
    <!-- Plugins -->
    <script src="{{ asset('themeforest/global/vendor/switchery/switchery.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/intro-js/intro.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/screenfull/screenfull.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
        <script src="{{ asset('themeforest/global/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
    
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/js/Component.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Base.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Config.js') }}"></script>
    
    <script src="{{ asset('themeforest/js/Section/Menubar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/GridMenu.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/Sidebar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/PageAside.js') }}"></script>
    <script src="{{ asset('themeforest/js/Plugin/menu.js') }}"></script>
    
    <script src="{{ asset('themeforest/global/js/config/colors.js') }}"></script>
    <script src="{{ asset('themeforest/js/config/tour.js') }}"></script>
    <script>Config.set('assets', "{{asset('themeforest')}}");</script>
    
    <!-- Page -->
    <script src="{{ asset('themeforest/js/Site.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/asscrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/slidepanel.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/switchery.js') }}"></script>
        <script src="{{ asset('themeforest/global/js/Plugin/jquery-placeholder.js') }}"></script>
        <script src="{{ asset('themeforest/global/js/Plugin/material.js') }}"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
  </body>
</html>
