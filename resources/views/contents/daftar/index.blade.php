
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Daftar Lenggah</title>
    
    <link rel="apple-touch-icon" href="{{ asset('themeforest/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstra') }}p.min.css">
    <link rel="stylesheet" href="{{ asset('themeforest/global/css/bootstrap-exten') }}d.min.css">
    <link rel="stylesheet" href="{{ asset('themeforest/css/site.min.css') }}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/animsition/animsition.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/asscrollable/asScrollable.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/switchery/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/intro-js/introjs.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/slidepanel/slidePanel.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/flag-icon-css/flag-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/custom_login.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/formvalidation/formValidation.css') }}">
    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/web-icons/web-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/brand-icons/brand-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themeforest/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="{{ asset('themeforest/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="{{ asset('themeforest/global/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/vendor/breakpoints/breakpoints.js') }}"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-login-v3 layout-full">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    @php 
    $vh = "";
    if(Agent::isDesktop()){
      $vh = "width : 90vh";
    }else if(Agent::isTablet()){
      $vh = "width : 60vh";
    }else{
      $vh = "";
    }
    @endphp

    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
      <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <div class="panel"  style="background: transparent; {{ $vh }}">
          <div class="panel-body">
            <div class="brand">
              <img class="brand-img" width="40%" style="padding-left: 40" src="{{ asset('images/logo_full.png') }}" alt="...">
            </div>
          </div>
        </div>
        <div class="panel" style="{{ $vh }}">
          <div class="panel-body">
            <div class="brand">
            </div>
            <form method="post" action="{{ url('daftar/simpan') }}" autocomplete="off" id="form_input">
              {{ csrf_field() }}
              <div class="row">
              <div class="col-sm-6 col-xs-6 col-lg-6">

                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Nama</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Jenis Kelamin</label>
                  <div class="col-sm-9">
                    <div class="radio-custom radio-default radio-inline">
                      <input type="radio" id="inputBasicMale" name="jenkel" value="0"/>
                      <label for="inputBasicMale">Pria</label>
                    </div>
                    <div class="radio-custom radio-default radio-inline">
                      <input type="radio" id="inputBasicFemale" name="jenkel" value="1"/>
                      <label for="inputBasicMale">Wanita</label>
                    </div>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">HP</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="hp" />
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Alamat</label>
                  <div class="col-sm-9">
                    <textarea name="alamat" class="form-control" rows="2"></textarea>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Paroki</label>
                  <div class="col-sm-9">
                    <select class="form-control select_option" name="paroki">
                      <option value=""> -- Pilih --</option>
                      @foreach($data['paroki'] as $w)
                        <option value="{{ $w->id }}">{{ $w->nama }}</option>
                      @endforeach
                      <option value="9999"> Lainnya </option>
                    </select>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial" id="div_parokitext" style="display: none">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Paroki</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="paroki_text" />
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial" id="div_wilayah">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Wilayah</label>
                  <div class="col-sm-9">
                    <select class="form-control select_option" name="wilayah">
                      
                    </select>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial" id="div_ling">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Lingkungan</label>
                  <div class="col-sm-9">
                    <select class="form-control select_option" name="lingkungan">
                      
                    </select>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Foto</label>
                  <div class="col-sm-9">
                    <input type="file" accept="image/*" id="foto" name="foto" class="form-control" data-fv-file="true">
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xs-6 col-lg-6">
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Username</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="username" readonly />
                    <p class="help-block text-left" style="color: red">Username menggunakan hp</p>
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Password</label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control" name="pass" />            
                  </div>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Ulangi Password</label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control" name="pass_" />
                  </div>
                  <p class="help-block" id="help_pass"></p>
                </div>
                <div class="form-group row" data-plugin="formMaterial">
                  <div class="captcha col-sm-12">
                    <span>{!! captcha_img('flat') !!}</span>
                    <button type="button" class="btn btn-success" id="refresh"><i class="fa fa-refresh"></i></button>
                  </div>
                </div>
                 <div class="form-group row" data-plugin="formMaterial">
                  <label class="col-form-label col-lg-3 col-md-3 col-xs-3 col-sm-3">Captcha</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="captcha" />
                  </div>
                </div>
              </div>
              </div>
              <button type="button" class="btn btn-primary btn-block btn-lg mt-40" id="btn_daftar">Daftar</button>
            </form>
            <p>Sudah punya akun? Silahkan <a href="{{ url('/login') }}">Log in</a> </p>
          </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
          
          <p>© {{ date("Y") }} Powered by AG SATU. All RIGHT RESERVED.</p>
          
        </footer>
      </div>
    </div>
    <!-- End Page -->

    <input type="hidden" name="gereja" value="{{ isset($data['gereja']->id) ? $data['gereja']->id:'' }}">

    <!-- Core  -->
    <script src="{{ asset('themeforest/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/popper-js/umd/popper.min.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/animsition/animsition.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
    <script src="{{ asset('vendor/loadingoverlay/loadingoverlay.js')}}"></script>


    <!-- Plugins -->
    <script src="{{ asset('themeforest/global/vendor/switchery/switchery.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/intro-js/intro.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/screenfull/screenfull.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
    <script src="{{ asset('themeforest/global/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

    <script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/bootstrap-sweetalert/sweetalert.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/select2/select2.full.min.js')}}"></script>

    <script src="{{ asset('themeforest/global/vendor/formvalidation/formValidation.js')}}"></script>
    <script src="{{ asset('themeforest/global/vendor/formvalidation/framework/bootstrap4.min.js')}}"></script>
    
    <!-- Scripts -->
    <script src="{{ asset('themeforest/global/js/Component.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Base.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Config.js') }}"></script>
    
    <script src="{{ asset('themeforest/js/Section/Menubar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/GridMenu.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/Sidebar.js') }}"></script>
    <script src="{{ asset('themeforest/js/Section/PageAside.js') }}"></script>
    <script src="{{ asset('themeforest/js/Plugin/menu.js') }}"></script>
    
    <script src="{{ asset('themeforest/global/js/config/colors.js') }}"></script>
    <script src="{{ asset('themeforest/js/config/tour.js') }}"></script>
    <script>Config.set('assets', "{{asset('themeforest')}}");</script>

    <script src="{{ asset('js/daftar_custom.js') }}"></script>
    <script src="{{ asset('js/custom_upload.js') }}"></script>
    
    <!-- Page -->
    <script src="{{ asset('themeforest/js/Site.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/asscrollable.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/slidepanel.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/switchery.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/jquery-placeholder.js') }}"></script>
    <script src="{{ asset('themeforest/global/js/Plugin/material.js') }}"></script>
    
    <script>
      var interval_1;

      (function(document, window, $){
        'use strict';
        start();
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
          start();
          convert_image("foto", "form_input");

          $(".select_option").select2();

          $('.date_fromtoday').datepicker({
            format : 'dd-mm-yyyy',
            setDate: new Date(),
            endDate: new Date(),
            autoclose: true,
            clearBtn: true,
          });
        });
      })(document, window, jQuery);

      $("[name=pass_]").keyup(function(){
        var pass_ = $(this).val();
        var pass = $("[name=pass]").val();

        if(pass != pass_){
          btn_off();
          $("#help_pass").text("Password tidak sama");
        }else{
          btn_on();
          $("#help_pass").text("");
        }

      })

      $("#btn_daftar").click(function(){
        var jk = '';
        var hp = $("[name=hp]").val();
        var nama = $("[name=nama]").val();
        var username = $("[name=username]").val();
        var pass = $("[name=pass]").val();
        var pass_ = $("[name=pass_]").val();
        var wilayah = $("[name=wilayah]").val();
        var lingkungan = $("[name=lingkungan]").val();
        var foto = $("[name=foto]").val();
        if ($("[name=jenkel]").checked && $("[name=jenkel]").value == '0'){
          jk = $("[name=jenkel]").value;
        }else{
          jk = $("[name=jenkel]").value;
        }
        var gereja = $("[name=paroki]").val();
        

        if(jk != '' && hp != '' && nama != '' && username != '' && pass_ != ''){ //&& foto != '' && wilayah != '' && lingkungan != ''
          $.ajax({
            url : "{{ url('daftar/simpan') }}",
            type : "POST",
            dataType : "json",
            headers : {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $("#form_input").serialize(),
            success : function(respon){
              if(respon.status == 0){
                swal("", respon.ket, "error");
              }else{
                swal("", respon.ket, "success");
                $("#refresh").click();
                clear_input();
                window.location.href = "{{ url('login') }}";
              }
            }
          })
        }else{
          swal("", "Silahkan lengkapi data yang ada", "warning");
        }

      })

      function btn_off(){
        $("#btn_daftar").hide();
      }

      function btn_on(){
        $("#btn_daftar").show();
      }

      function clear_input(){
        $("[name=hp]").val("");
        $("[name=nama]").val("");
        $("[name=username]").val("");
        $("[name=pass]").val("");
        $("[name=pass_]").val("");
        $("[name=hp]").val("");
        $("[name=captcha]").val("");
        $("[name=alamat]").text("");
        $("[name=wilayah]").val("").trigger("change");
      }

      $('#refresh').click(function(){
        get_capcha();
      });

      $("[name=paroki]").change(function(){
        var parent = $(this).val();
        if(parent != '' && parent != 9999){
          $("#div_parokitext").hide();
          $("#div_wilayah").show();
          $("#div_ling").show();
          $.ajax({
            url : "{{ url('daftar/get_lingkungan') }}",
            type : "get",
            dataType : "html",
            data : { id : 1, parent : parent },
            headers : {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(respon){
              $("[name=wilayah]").html(respon);
            },
              complete : function(respon){
                  
            }
          })
        }else if(parent > 9000){
          $("#div_parokitext").show();
          $("#div_wilayah").hide();
          $("#div_ling").hide();
          $("[name=wilayah]").html("");
          $("[name=lingkungan]").html("");
        }else{
          $("#div_parokitext").hide();
          $("#div_wilayah").show();
          $("#div_ling").show();
          $("[name=wilayah]").html("");
          $("[name=lingkungan]").html("");
        }
      })

      $("[name=wilayah]").change(function(){
        var parent = $(this).val();
        if(parent != ''){
          $.ajax({
            url : "{{ url('daftar/get_lingkungan') }}",
            type : "get",
            dataType : "html",
            data : { id : 2, parent : parent },
            headers : {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(respon){
              $("[name=lingkungan]").html(respon);
            },
              complete : function(respon){
                  
            }
          })
        }else{
          $("[name=lingkungan]").html("");
        }
      })

      $("[name=hp]").keyup(function(){
        var isi = $(this).val();

        $("[name=username]").val(isi);
      })

    function start(){
      clearInterval(interval_1);
      interval_1 = setInterval(function () {
        get_capcha();
        console.log("");
      }, 100 * 1000);
    }

    function get_capcha(){
      $.ajax({
           type:'GET',
           dataType : "html",
           url: "{{ url('daftar/refreshCaptcha') }}",
           success:function(data){
              $(".captcha span").html(data);
           }
        });
    }

    $(document).ajaxStart(function(){
        $.LoadingOverlay("show",{
          imageAnimation  : "rotate_right",
          imageColor : "#3c8dbc"
        });
      }).ajaxStop(function(){
         $.LoadingOverlay("hide");
      })

    </script>
  </body>
</html>
