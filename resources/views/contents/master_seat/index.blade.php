@extends('templates.index')
@section('title', 'Master Kursi')
@section('css')
@endsection
@section('content')
<div class="page-header">
    <h1 class="page-title">Master Kursi</h1>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<div class="panel-heading">
        <h4 class="panel-title">
          <strong>Master Kursi</strong>
        </h4>
      </div>
		<div class="panel-body">
		  <div class="col-sm-12 col-md-12" style="margin-bottom: 1%;">
            <a class="btn btn-primary btn-tambah-seat" href="javascript:;"><i class='fa fa-plus'></i> Generate</a>
          </div>
          <div class="table-responsive">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%" id="table-seat">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Lokasi</th>
	                 	<th>Area</th>
	                 	<th>Nama Kursi</th>
	                 	<th>Is Aktif</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>
		</div>			
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-modul">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">...</h4>
      </div>

      <div class="modal-body">
        <form role="form" id="form-modul" method="post" action="#">
          <input type="hidden" name="popup_id">
          <div class="form-group">
            <label class="control-label">Lokasi</label>
            <!-- <input type="text" name="popup_lokasi" class="form-control"> -->
            
            <input type="text" class="form-control" name="popup_textlokasi" value="{{ isset($data['lokasi']->nama) ? $data['lokasi']->nama:"" }}" readonly>
            <input type="hidden" name="popup_lokasi" value="{{ isset($data['lokasi']->id) ? $data['lokasi']->id:"" }}">
          </div>
          
          <div class="form-group">
            <label class="control-label">Area</label>
            <select class="form-control" name="popup_area">
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Nama Kursi</label>
            <input type="number" name="popup_nama" class="form-control">
          </div>
          <div class="form-group">
            <label class="control-label">Baris</label>
            <input type="number" name="popup_baris" class="form-control">
          </div>
          <div class="form-group">
            <label class="control-label">Kolom</label>
            <input type="number" name="popup_kolom" class="form-control">
          </div>
          <div class="form-group">
            <label class="control-label">Is Aktif</label>
            <select class="form-control" name="popup_aktif">
            	<option value="0">Tidak Aktif</option>
            	<option value="1">Aktif</option>
            </select>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-batal-modul">Batal</button>
        <button type="button" class="btn btn-primary btn-simpan-modul">Simpan</button>
      </div>

    </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('js')
<script type="text/javascript">
	var table, store_area = '';
	var modalModul = $("#modal-modul");
	$(document).ready(function(){
    $('[name=popup_area], [name=popup_aktif]').select2();
		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
          searchDelay: 2000,
	        ajax: '{{ url('seat_show') }}',
	        columns: [
	            {data: 'id', name: 'id', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama_lokasi', name: 'nama_lokasi'},
	            {data: 'nama_area', name: 'nama_area'},
	            {data: 'nama_kursi', name: 'nama_kursi'},
	            {data: 'text_aktif', name: 'text_aktif'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
              
	        ],
		});

		$(".btn-tambah-seat").on("click", function(e){
		    resetForm();
	      $(".btn-simpan-modul").removeAttr("disabled");
	      modalModul.find(".modal-title").html("Tambah Seat");
	      showModal(modalModul);
	    })

	    $(".btn-batal-modul").on("click", function(e){
	      resetForm();
	      modalModul.find('.modal-title').html('...');
	      modalModul.modal('hide');
	    })

      get_data_area();
	})

	function btnEditData(id)
  {
    resetForm();
    setTimeout(function() {
    $(".btn-simpan-modul").removeAttr("disabled");
    
      var id_seat = $("#table_id"+id).val();
      var lokasi = $("#table_lokasi"+id).val();
      // var ruang = $("#table_ruang"+id).val();
      var area = $("#table_area"+id).val();
      var nama = $("#table_nama"+id).val();
      var baris = $("#table_baris"+id).val();
      var deret = $("#table_deret"+id).val();
      var isaktif = $("#table_isaktif"+id).val();

      store_area = area;

      $("[name=popup_id]").val(id_seat);
      $("[name=popup_lokasi]").val(lokasi);
      // $("[name=popup_ruang]").val(ruang);
      $("[name=popup_nama]").val(nama);
      $("[name=popup_area]").val(area);
      $("[name=popup_baris]").val(baris);
      $("[name=popup_kolom]").val(deret);
      $("[name=popup_aktif]").val(isaktif).trigger('change');

      get_data_area();
    }, 1000);

      
    modalModul.find('.modal-title').html("Edit Menu");
    showModal(modalModul);
  }

  function btnHapusData(id)
  {
    swal({
      title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },function(isConfirm){      
        if (isConfirm) {
          $.ajax({
          url: "{{ url('seat_hapus')}} ",
          type: 'post',
          data: {id : id},
          headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(respon){ 
          table.ajax.reload();
          swal("Deleted!", "Your imaginary file has been deleted!", "success");
         }
        })
        } else {
          swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    })

  }

  function showModal(selector) {
		$(selector).modal({
			keyboard : false,
			backdrop : false,
		});
	}

	function hideModal(selector) {
		$(selector).modal("hide");
	}

	function resetForm()
	{
    $("[name=popup_id]").val("");
    // $("[name=popup_lokasi]").val("");
    $("[name=popup_ruang]").val("");
    $("[name=popup_nama]").val("");
    $("[name=popup_area]").val("").trigger("change");
    $("[name=popup_baris]").val("");
    $("[name=popup_kolom]").val("");
    $("[name=popup_aktif]").val("0").trigger('change');
    store_area = '';
	}

  $(".btn-simpan-modul").click(function(){
    var nama = $("[name=popup_nama]").val();

    if(nama != ''){
      $.ajax({
        url: "{{ url('seat_simpan')}} ",
        type: 'post',
        data: $("form").serialize(),
        headers : {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(respon){
          table.ajax.reload();
          modalModul.find('.modal-title').html('...');
          modalModul.modal('hide');

        }
      })
    }
  })

  function get_data_area()
  {
    var id = $("[name=popup_lokasi]").val();
    if(id!=""){
    $.ajax({
      url : "{{url('seat_area')}}/"+id,
      type : "get",
      dataType : "json",
      success : function(result){

        $("[name=popup_area]").html("");
        $("[name=popup_area]").append("<option value=''>-- Pilih --</option>");


        if(result.length > 0){
          $.each(result, function(id, item){
            // var opt = new Option(item.nama.toUpperCase(), item.id);
            $("[name=popup_area]").append("<option value='"+item.id+"' digit='"+item.digit+"'>"+item.nama.toUpperCase()+"</option>");
          });
        }

      },
      complete : function(result){
        if(store_area != ''){
          $("[name=popup_area]").val(store_area).trigger("change");
        }
      }
    })
      
    }
  }

  $("[name=popup_area]").change(function(){
    var digit = $("[name=popup_area] :selected").attr("digit");

    $("[name=popup_nama]").val(digit);
  })
</script>
@endsection