@extends('templates.index')
@section('title', 'Master Provinsi')

@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Provinsi</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group">
	            	<label class="form-control-label">Nama Provinsi*</label>
	            	<input type="text" name="popup_nama" class="form-control">
	            </div>
            	
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('provinsi/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama', name: 'nama'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var nama = $("[name=popup_nama]").val();

		if(nama != ''){
			$.ajax({
				url : "{{ url('provinsi/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal("hide");
		      	}
			})
		}
	})

	function edit(this_){
		var data_id = $(this_).data("id");

		var id = $("#table_id"+data_id).val();
		var name = $("#table_nama"+data_id).val();

		$("[name=popup_id]").val(id);
		$("[name=popup_nama]").val(name);

		$("#examplemodal").modal("show");
	}

	function hapus(this_){
		var id = $(this_).data("id");
		var name = $("#table_nama"+id).val();
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data "+name,
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('provinsi/hapus')}} ",
	          	type: 'post',
	          	data: {id : id},
			       headers : {
			       	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			       },
	          	success: function(respon){ 
	          		table.ajax.reload();
	          		// swal("Deleted!", "Your imaginary file has been deleted!", "success");
	         	}
        	})
    	})
	}

	function clear_input(){
		$("[name=popup_id]").val('');
		$("[name=popup_nama]").val('');
	}
</script>
@endsection