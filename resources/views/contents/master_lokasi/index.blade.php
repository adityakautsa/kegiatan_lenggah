@extends('templates.index')
@section('title', 'Master Gereja')

@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Wilayah</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama Lokasi</th>
	                 	<th>Provinsi</th>
	                 	<th>Kabupaten</th>
	                 	<th>Kecamatan</th>
	                 	<th>Kelurahan</th>
	                 	<th>Status</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Lokasi</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_lokasi" class="form-control">
	            	</div>
	            </div>
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Provinsi</label>
	            	<div class="col-lg-9">
	            		<select name="popup_prov" id="popup_prov" class="form-control" style="width: 100%">
		            		<option value=""> -- Pilih -- </option>
		            		@foreach($data['prov'] as $p)
		            		<option value="{{ $p->id }}">{{ $p->name }}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Kabupaten</label>
	            	<div class="col-lg-9">
	            		<select name="popup_kab" class="form-control" style="width: 100%">
		            				            		
		            	</select>
	            	</div>
	            </div> 
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Kecamatan</label>
	            	<div class="col-lg-9">
	            		<select name="popup_kec" class="form-control" style="width: 100%">
		            				            		
		            	</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Kelurahan</label>
	            	<div class="col-lg-9">
	            		<select name="popup_kel" class="form-control" style="width: 100%">
		            				            		
		            	</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Alamat</label>
	            	<div class="col-lg-9">
	            		<textarea class="form-control" name="popup_alamat" rows="3"></textarea>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Telepon</label>
	            	<div class="col-lg-9">
	            		<input type="number" name="popup_telepon" class="form-control">
	            	</div>
	            </div>
	            {{-- <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Parents</label>
	            	<div class="col-lg-9">
	            		<select name="popup_parent" class="form-control" style="width: 100%">
	            			
	            		</select>
	            	</div>
	            </div> --}}
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Aktif</label>
	            	<div class="col-lg-9">
	            		<select name="popup_aktif" class="form-control" style="width: 100%">
	            			<option value="1">Aktif</option>
	            			<option value="0">Tidak Aktif</option>
	            		</select>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var table;
	var id_prop = '', id_kab = '', id_kec = '', id_kel = '', id_data = '', parent = '';
	$(document).ready(function(){
		
		/*$("[name=popup_prov],[name=popup_kab],[name=popup_kec],[name=popup_kel],[name=popup_aktif],[name=popup_parent]").select2({
			dropdownParent: $("#examplemodal")
		});*/
		$("[name=popup_prov],[name=popup_kab],[name=popup_kec],[name=popup_kel],[name=popup_aktif],[name=popup_parent]").select2();


		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('lokasi/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama', name: 'nama'},
	            {data: 'nama_prov', name: 'nama_prov'},
	            {data: 'nama_kab', name: 'nama_kab'},
	            {data: 'nama_kec', name: 'nama_kec'},
	            {data: 'nama_kel', name: 'nama_kel'},
	            {data: 'status', name: 'status'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		clear_input();
		loadParentMenu();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	function clear_input(){
		id = ''; id_kab = ''; id_kec = ''; id_kel = ''; id_data = '', parent = '';
		$("[name=popup_id]").val('');
		$("[name=popup_lokasi]").val('');
		$("[name=popup_prov]").val('').trigger("change");
		$("[name=popup_alamat]").val('');
		$("[name=popup_telepon]").val('');
		$("[name=popup_aktif]").val('1').trigger("change");
	}

	$("[name=popup_prov]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('lokasi/get_kabupaten') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=popup_kab]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=popup_kab]").val(id_kab).trigger("change");
		      	}
			})
		}else{
			trigger("[name=popup_kab]");
		}
	})

	$("[name=popup_kab]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('lokasi/get_kecamatan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=popup_kec]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=popup_kec]").val(id_kec).trigger("change");
		      	}
			})
		}else{
			trigger("[name=popup_kec]");
		}
	})

	$("[name=popup_kec]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('lokasi/get_kelurahan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=popup_kel]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=popup_kel]").val(id_kel).trigger("change");
		      	}
			})
		}else{
			trigger("[name=popup_kel]");
		}
	})

	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var nama = $("[name=popup_lokasi]").val();
		var prov = $("[name=popup_prov]").val();
		var kab = $("[name=popup_kab]").val();
		var kec = $("[name=popup_kec]").val();
		var kel = $("[name=popup_kel]").val();
		var alamat = $("[name=popup_alamat]").val();
		var telepon = $("[name=popup_telepon]").val();
		var aktif = $("[name=popup_aktif]").val();

		if(nama != '' && prov != '' && kab != '' && kec != '' && kel != ''){
			$.ajax({
				url : "{{ url('lokasi/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal('hide');
		      	}
			})
		}else{

		}

	})

	function edit(this_){
		var id = $(this_).data("id");
		$.ajax({
			url : "{{ url('lokasi/get_edit') }}",
			type : "POST",
			dataType : "json",
			data : { id : id },
			headers : {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
		    success : function(respon){
		    	if(respon.length > 0){
		    		show_edit(respon);
		    	}
		    }
		})
	}

	function show_edit(respon){
		var id = respon[0].id;
		var nama = respon[0].nama;
		id_prop = respon[0].provinsi;
		id_kab = respon[0].kabupaten;
		id_kec = respon[0].kecamatan;
		id_kel = respon[0].desa;
		parent = respon[0].parents;
		var alamat = respon[0].alamat;
		var telp = respon[0].no_telp;
		var is_aktif = respon[0].is_aktif;
		console.log(respon);

		$("[name=popup_id]").val(id);
		$("[name=popup_lokasi]").val(nama);
		$("[name=popup_prov]").val(id_prop).trigger("change");
		$("[name=popup_alamat]").val(alamat);
		$("[name=popup_telepon]").val(telp);
		$("[name=popup_aktif]").val(is_aktif).trigger("change");

		loadParentMenu();
		$("#modal_title").text("Edit");
		$("#examplemodal").modal('show');
	}

	function hapus(this_){
		var id = $(this_).data("id");
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ini",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('lokasi/hapus')}} ",
	          	type: 'post',
	          	dataType : 'json',
	          	data: {id : id},
			    headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	          	success: function(respon){
	          		if(respon.status == 1){
	          			swal("", respon.keterangan, "success");
	          		}else{
	          			swal("", respon.keterangan, "error");
	          		}
	          		table.ajax.reload();
	         	}
        	})
    	})
	}


	function trigger(value){
		$(value).html("<option value=''> -- Pilih -- </option>");
		$(value).val("").trigger("change");
	}

	function loadParentMenu() {
	    $.ajax({
	      type : "post",
	      url : "{{url('lokasi/get_parents')}}",
	      dataType : "json",
	      headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
	      success : function(result){
	        $("[name=parent]").html("");
	        if(result.length > 0){
	        	$("[name=popup_parent]").append('<option value=""> -- Pilih -- </option>');
	        	$("[name=popup_parent]").append('<option value="0"> Parent Gereja </option>');
	        	for(i in result){
	            $("[name=popup_parent]").append('<option value="'+result[i].id_lokasi+'">'+result[i].nama_lokasi+'</option');
	            }
	        }
	      },
		  complete : function(result){
		      	$("[name=popup_parent]").val(parent).trigger("change");
		    }
	    });
	  }

</script>
@endsection