@extends('templates.index')
@section('title', 'Dashboard')

@section('css')
<style type="text/css">
	.contenido_s {
		max-height: 430px;
		max-width: 245px;
		border: 2px dashed #dadde6;
	}

	.details_s {
		padding-left: 22px;
		padding-top: 5px;
		background: white;
		border-top: 1px dashed #c3c3c3;
	}

	.hqr_s {
		display: table;
		width: 100%;
		table-layout: fixed;
		margin: 0px auto;
	}
</style>
<link src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" rel="stylesheet" type="text/css">

@endsection
@section('content')
<div class="page-header">
	<h1 class="page-title">Dashboard</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item active"><a href="javascript:void(0)">Dashboard</a></li>
	</ol>
</div>

<div class="page-content container-fluid">
@if(count($data['data_f']) > 0)
<h4>Kegiatan Yang Akan Datang</h4>
	<div class="row">
		@foreach($data['data_f'] as $f)
		<div class="col-xxl-5 col-lg-4">
			<!-- Widget Stacked Bar -->
			<div class="card card-shadow" id="widgetStackedBar">
				<div class="card-block p-0">
					<div class="p-30 h-80">
						<p><i class="wb-calendar"></i> &nbspTanggal</p>
						<span class="font-size-20">{{$f['jadwal']}}</span>
					</div>
					<div class="p-30 h-80">
						<p><i class="wb-time"></i> &nbspJam : <span class="font-size-15">{{$f['jam']}} WIB</span></p>
					</div>
					<div class="counters pb-20 px-30">
						<div class="row no-space">
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-warning">
										<i class="icon wb-home"></i>
									</button>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-primary">
										<i class="icon wb-order"></i>
									</button>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-success">
										<i class="icon wb-check"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="counters pb-20 px-30">
						<div class="row no-space">
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Kapasitas</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related green-600">+</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['kapasitas']}}</span>
										<!-- <span class="counter-number-related green-600">%</span> -->
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Pesan</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related red-600">-</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['pesan']}}</span>
										<!-- <span class="counter-number-related red-600">%</span> -->
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Tersedia</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related green-600">+</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['kosong']}}</span>
										<!-- <span class="counter-number-related green-600">%</span> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Widget Stacked Bar -->
		</div>
		@endforeach		
	</div>
@endif

  <div class="panel" style="overflow-x: scroll;">
  	<div class="panel-heading">
  		<h3 class="panel-title">Grafik</h3>
  	</div>
  	<div class="panel-body" style="width:1000px;">
  		<div class="col-xxl-6 col-lg-12">
            <!-- Widget OVERALL VIEWS -->
            <div class="card card-shadow" id="widgetOverallViews">
  				<canvas id="daily-chart-status"></canvas>
            </div>
            <!-- End Widget OVERALL VIEWS -->
          </div>
  	</div>
  </div>

	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Petugas</h3>
		</div>
		<div class="panel-body">
			<h4>Kegiatan Terakhir</h4>
			<div class="table-responsive">
				<table class="table table-hover dtTable table-striped w-full display nowrap" data-plugin="datatables" style="width:100%">
					<thead>
						<tr>
							<th width="5%" class="text-center"></th>
							<th width="40%" class="text-center">Jadwal</th>
							<th width="15%" class="text-center">Kapasitas Tempat Duduk</th>
							<th width="10%" class="text-center">Pesan</th>
							<th width="10%" class="text-center">Hadir</th>
							<th width="10%" class="text-center">Tidak Hadir</th>
							<th width="10%" class="text-center">Kosong</th>
						</tr>
					</thead>
					<tbody>
						@php $no = 1; @endphp
						@foreach($data['data_p'] as $d)
						<tr>
							<td class="text-center">{{$no++}}</td>
							<td class="text-left">{{$d['jadwal']}}</td>
							<td class="text-center">{{$d['kapasitas']}}</td>
							<td class="text-center">{{$d['pesan']}}</td>
							<td class="text-center">{{$d['hadir']}}</td>
							<td class="text-center">{{$d['tidak_hadir']}}</td>
							<td class="text-center">{{$d['kosong']}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>


@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script type="text/javascript">
renderChartDaily();

function renderChartDaily(){
  $.ajax({
    url: "{{url('dashboard_grafik')}}",
    type:"POST",
    data:{id:5},
    datatype:"JSON",
    headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success:function(respon){
      console.log(respon);
      new Chart(document.getElementById("daily-chart-status"), {
        type: 'line',
        data: {
			labels: respon['labels'],
			datasets: respon['dataset']
        },
        options: {
          responsive: true,
        }
      });
    },
    error:function(jqXHR, textStatus, errorThrown){
      alert('Gagal Reload');
    }
  });
}
</script>
@endsection