@extends('templates.index')
@section('title', 'Dashboard')

@section('css')
<style type="text/css">
	.contenido_s {
		max-height: 430px;
		max-width: 245px;
		border: 2px dashed #dadde6;
	}

	.details_s {
		padding-left: 22px;
		padding-top: 5px;
		background: white;
		border-top: 1px dashed #c3c3c3;
	}

	.hqr_s {
		display: table;
		width: 100%;
		table-layout: fixed;
		margin: 0px auto;
	}
</style>
@endsection
@section('content')
<div class="page-header">
	<h1 class="page-title">Dashboard</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item active"><a href="javascript:void(0)">Dashboard</a></li>
	</ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Member</h3>
		</div>
		<div class="panel-body">
			<h4>Tiket Hari ini</h4>
			<div class="table-responsive">
				<table class="table table-hover dtTable table-striped w-full display nowrap" data-plugin="dataTable" style="width:100%">
					<thead>
						<tr>
							<th width="5%" class="text-center"></th>
							<th width="25%" class="text-center">Tanggal Kegiatan</th>
							<th width="10%" class="text-center">Jumlah</th>
							<th width="40%" class="text-center">Kursi</th>
							<th width="15%" class="text-center"></th>
						</tr>
					</thead>
					<tbody>
						@php $no = 1; @endphp
						@foreach($data['data'] as $d)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$d['tgl_show']}}</td>
							<td class="text-center">{{$d['jumlah_kursi']}}</td>
							<td>{{$d['kursi']}}</td>
							<td>@php echo $d['aksi'] @endphp</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

		</div>
	</div>
	<h4>Kegiatan Yang Akan Datang</h4>
	<div class="row">
		@foreach($data['data_f'] as $f)
		<div class="col-xxl-5 col-lg-4">
			<!-- Widget Stacked Bar -->
			<div class="card card-shadow" id="widgetStackedBar">
				<div class="card-block p-0">
					<div class="p-30 h-80">
						<p><i class="wb-calendar"></i> &nbspTanggal
							<a href="{{url('booking')}}" type="button" style="margin-left:120px;" class="btn btn-direction btn-left btn-success">Pesan</a>
						</p>
						<span class="font-size-20">{{$f['jadwal']}}</span>
					</div>
					<div class="p-30 h-80">
						<p><i class="wb-time"></i> &nbspJam : <span class="font-size-15">{{$f['jam']}} WIB</span></p>
					</div>
					<div class="counters pb-20 px-30">
						<div class="row no-space">
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-warning">
										<i class="icon wb-book"></i>
									</button>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-primary">
										<i class="icon wb-order"></i>
									</button>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<button type="button" class="btn btn-floating btn-sm btn-success">
										<i class="icon wb-check"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="counters pb-20 px-30">
						<div class="row no-space">
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Kapasitas</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related green-600">+</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['kapasitas']}}</span>
										<!-- <span class="counter-number-related green-600">%</span> -->
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Pesan</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related red-600">-</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['pesan']}}</span>
										<!-- <span class="counter-number-related red-600">%</span> -->
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="counter counter-sm">
									<div class="counter-label text-uppercase" style="font-size: smaller;">Tersedia</div>
									<div class="counter-number-group text-truncate">
										<!-- <span class="counter-number-related green-600">+</span> -->
										<span class="counter-number green-600" style="font-size: larger;">{{$f['kosong']}}</span>
										<!-- <span class="counter-number-related green-600">%</span> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Widget Stacked Bar -->
		</div>
		@endforeach
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="modal_title"></h4>
			</div>
			<div class="modal-body" id="modal_body">
				<input type="hidden" name="table_id" id="table_id">
				<div class="text-center" style="border: 2px dashed #dadde6;">
					<div class="ticket">
						<div class="hqr_s">
							<div class="text-center">
								<img id="qr_barcode" height="100px;" width="100px;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=&choe=UTF-8">
							</div>
						</div>
					</div>
					<div class="details_s">
						<div class="row table-responsive" style="margin-bottom: 30px;">
							<table width="100%">
								<tr>
									<td width="100%" class="text-center" colspan="3">
										<h2 id="gate"></h2>
									</td>
								</tr>
								<tr>
									<td width="20%" class="text-left"><b>Nama</b></td>
									<td width="5%" class="text-center">:</td>
									<td width="75%" class="text-left" id="nama_ktp">-</td>
								</tr>
								<tr>
									<td width="20%" class="text-left"><b>Lokasi</b></td>
									<td width="5%" class="text-center">:</td>
									<td width="75%" class="text-left" id="gereja">-</td>
								</tr>
								<tr>
									<td width="20%" class="text-left"><b>Gate</b></td>
									<td width="5%" class="text-center">:</td>
									<td width="75%" class="text-left" id="t_gate">-</td>
								</tr>
								<tr>
									<td width="20%" class="text-left"><b>Jadwal</b></td>
									<td width="5%" class="text-center">:</td>
									<td width="75%" class="text-left" id="jam">-</td>
								</tr>
							</table>
							<table border="1" width="100%" id="table_ticket">
								<thead>
									<tr>
										<td width="40%">Kursi</td>
										<td width="60%">Pengunjung</td>
									</tr>
								</thead>
								<tbody id="kursi">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="image-priview hide"></div>
			<div class="modal-footer" id="modal_footer">
				<button type="button" class="btn btn-default btn-batal-modul" data-dismiss="modal">Batal</button>
				@if(Agent::isDesktop())
				<button type="button" class="btn btn-primary btn-download-modul" data-dismiss="modal">Download</button>
				@endif
				@if(Agent::isTablet() || Agent::isMobile())
				<button type="button" onclick="showAndroidToast()" class="btn btn-primary" data-dismiss="modal">Print</button>
				@endif

			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	function show(data_id) {
		$("[name=table_id]").val("");
		$("#no_ktp").text("");
		$("#nama_ktp").text("");
		$("#gereja").text("");
		$("#t_gate").text("");
		$("#jadwal").text("");
		$("#table_ticket tbody").html('');
		$.ajax({
			url: "{{url('riwayat/get_ticket')}}",
			type: "post",
			data: {
				id: data_id
			},
			dataType: "json",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(result) {
				$("[name=table_id]").val(data_id);
				$("#nama_ktp").text(result['data']['nama_ktp']);
				$("#gereja").text(result['data']['nama_lokasi']);
				$("#t_gate").text(result['gate']);
				$("#jam").text(result['waktu']);
				$("#gate").text(result['gate']);
				for (i in result.detail) {
					$("#table_ticket tbody").append(result.detail[i]);
				}


			}
		})
		$('#qr_barcode').attr('src', "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" + data_id + "&choe=UTF-8");
		$("#modal_title").text("Ticket QRCode");
		$("#examplemodal").modal("show");
	}
</script>
@endsection