<style type="text/css">
table {
	border-collapse: collapse;
}

th {
	border: 1px solid #000;
}
.border {
	border: 1px solid #000;
}
</style>

<table>
<tr>
	<td colspan="4"></td>
</tr>
<tr>
	<td colspan="2">Periode</td>
	<td>: </td>
	<td>{{$data['judul']}}</td>
</tr>
<tr>
	<td colspan="2">Area</td>
	<td>: </td>
	<td>{{$data['pengunjung'][0]->nama_area}}</td>
</tr>
<tr>
	<td colspan="2">Umat yang hadir</td>
	<td>:</td>
	<td>{{($data['pengunjung'][0]->hadir > 0)?$data['pengunjung'][0]->hadir:'0'}}</td>
</tr>
<tr>
	<td colspan="2">Umat yang tidak hadir</td>
	<td>:</td>
	<td>{{($data['pengunjung'][0]->tidak_hadir > 0)?$data['pengunjung'][0]->tidak_hadir:'0'}}</td>
</tr>
<tr>
	<td colspan="2">Total Umat Keseluruhan</td>
	<td>:</td>
	<td>{{($data['pengunjung'][0]->hadir+$data['pengunjung'][0]->tidak_hadir > 0)?$data['pengunjung'][0]->hadir+$data['pengunjung'][0]->tidak_hadir:'0'}}</td>
</tr>
</table>
<br>
<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Area</th>
			<th>No. Kursi</th>
			<th>Nama User</th>
			<th>Hadir</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total = 0;
		$no = 1;
		foreach($data['data'] as $d){
		?>
		<tr>
			<td>{{$no++}}</td>
			<td>{{$d->nama_area}}</td>
			<td>{{$d->row.$d->kolom}}</td>
			<td>{{$d->nama_pemesan}}</td>
			<td>{{($d->status_pesan==0)?'':'✓'}}</td>
		</tr>
	<?php } ?>
		
	</tbody>
</table>