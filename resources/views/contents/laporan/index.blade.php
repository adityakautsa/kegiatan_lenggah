@extends('templates.index')
@section('title', 'Laporan')

@section('content')
<div class="page-header">
    <h1 class="page-title">Laporan</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Pengunjung</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="row">
		<div class="col-sm-6">
			<div class="panel">
				<header class="panel-heading">
		            <h3 class="panel-title">
		              	Laporan Pengunjung
		            </h3>
		          </header>
				<div class="panel-body">
					<form id="form_pengunjung" action="{{ url("laporan/laporan_pengunjung") }}" method="GET" target="_blank">
						<div class="form-group row">
							<label class="col-form-label col-sm-4">Tanggal</label>
							<div class="col-sm-8">
								<input type="text" name="tanggal" class="form-control datepicker" value="{{ date('d-m-Y') }}" readonly>								
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-4">Jadwal</label>
							<div class="col-sm-8">
								<select class="form-control" name="jadwal" style="width: 100%" required>
									
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-4">Gate</label>
							<div class="col-sm-8">
								<select class="form-control" name="gate" style="width: 100%" required>
									
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-4">Area</label>
							<div class="col-sm-8">
								<select class="form-control" name="area" style="width: 100%" required>
									
								</select>
							</div>
						</div>
						<div class="form-group row">
							<button type="submit" id="btn_cetak" class="btn btn-primary">Cetak</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var enddate = "{{ $data['tgl'] }}";
	$(document).ready(function(){
		$("[name=jadwal], [name=gate], [name=area]").select2();

		$('.datepicker').datepicker({
           	format : 'dd-mm-yyyy',
            setDate: new Date(),
            endDate: new Date(),
            autoclose: true,
            clearBtn: true,
        });

        $(".datepicker").datepicker("setEndDate", enddate);
        $(".datepicker").on("changeDate", function (e) {
	    	get_jam($("[name=tanggal]").val());
	    });

        var tgl = $("[name=tanggal]").val();
        get_jam(tgl);
	})

	$("[name=jadwal]").change(function(){
		var id_jadwal = $(this).val();
		var jenis = '1';

		if(id_jadwal != ''){
			$.ajax({
				url : "{{ url('laporan/get_gate') }}",
				type : "post",
				dataType : "json",
				data : {id : id_jadwal, jenis : jenis},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      		},
	      		success : function(respon){
	      			if(respon.length > 0){
	      				$("[name=gate]").html("");
	      				for(i in respon){
	      					$("[name=gate]").append("<option value='"+respon[i].id_gate+"'>"+respon[i].nama_gate+"</option>");
	      				}
	      			}else{
	      				$("[name=gate]").html("");
	      			}
	      		},
	      		complete : function(){
	      			$('[name=gate]').val($('[name=gate] option:first-child').val()).trigger('change');
	      		}
			})
		}
	})

	$("[name=gate]").change(function(){
		var id = $("[name=gate] :selected").val();

		if(id != ''){
			$.ajax({
				url : "{{ url('laporan/get_area') }}",
				type : "post",
				dataType : "json",
				data : {id : id, jenis : 0, jadwal : $("[name=jadwal]").val()},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      		},
	      		success : function(respon){
	      			if(respon.length > 0){
	      				$("[name=area]").html("");
	      				$("[name=area]").append("<option value='0'>Semua Area</option>");
	      				for(i in respon){
	      					$("[name=area]").append("<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>");
	      				}
	      			}else{
	      				$("[name=area]").html("");
	      			}
	      		}
			})
		}
	})

	function get_jam(tgl){
		if(tgl != ''){
			$.ajax({
				url : "{{ url('laporan/get_jam') }}",
				type : "POST",
				dataType : "html",
				data : {tgl : tgl},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      		},
	      		success : function(respon){
	      			$("[name=jadwal]").html(respon);
	      		},
	      		complete : function(respon){
	      			$('[name=jadwal]').val($('[name=jadwal] option:first-child').val()).trigger('change');
	      		}
			})
		}
	}

</script>
@endsection