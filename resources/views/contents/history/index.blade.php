@extends('templates.index')
@section('title', 'Riwayat Log')

@section('content')
<div class="page-header">
    <h1 class="page-title">Riwayat Log</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Riwayat Log</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	Riwayat Log
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Tanggal</th>
	                 	<th>Nama User</th>
	                 	<th>Status</th>
	                 	<th>Keterangan</th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>


@endsection

@section('js')
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('history/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'tanggal', name: 'tanggal'},
	            {data: 'nama_user', name: 'nama_user'},
	            {data: 'status', name: 'status'},
	            {data: 'keterangan', name: 'keterangan'}
	        ],
		});
	})

</script>
@endsection