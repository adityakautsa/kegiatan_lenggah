@extends('templates.index')
@section('title', 'Master Menu')
@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-treeview/bootstrap-treeview.css')}}">
@endsection
@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('menu') }}">Menu</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body table-responsive">
			<table class="table table-hover dtTable table-striped w-full display nowrap" id="tabel-menu" style="width:100%">
				<thead>
	                <tr>
	                 	<th>ID</th>
                      	<th>MENU</th>
                      	<th>LINK</th>
                      	<th>ICON</th>
                      	<th>PARENT MENU</th>
                      	<th>TIPE SITE</th>
                      	<th>URUTAN</th>
                      	<th>AKSI</th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Menu</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_name" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Url</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_url" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Icon</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_icon" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Tipe Site</label>
	            	<div class="col-lg-9">
	            		<select name="popup_aktif" class="form-control" style="width: 100%">
	            			<option value="1">1</option>
	            			<option value="0">0</option>
	            		</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Urutan</label>
	            	<div class="col-lg-9">
	            		<input type="number" name="popup_urutan" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Parent</label>
	            	<div class="col-lg-9">
	            		<select name="popup_parent" class="form-control" style="width: 100%">
	            			<option value="0">Parent Menu</option>
	            			@foreach($data['menu'] as $d)
	            			<option value="{{$d->id}}">{{($d->parent==0)?$d->name:'['.$d->parent_menu.'] '.$d->name}}</option>
	            			@endforeach
	            		</select>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/fancybox.min.js')}}"></script>
<script type="text/javascript">
	var tabelModul;
  	var modalModul = $("#modal-modul");
  	var formModul = $("#form-modul");

  	var dataForm = {};
  	var url;
	$(document).ready(function(){
		
		$("[name=popup_prov], [name=popup_kab], [name=popup_kec], [name=popup_kel], [name=popup_aktif]").select2({
			dropdownParent: $("#examplemodal")
		});

		// initTabelModul();

		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('menu/get_data2') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'menu', name: 'menu'},
	            {data: 'link', name: 'link'},
	            {data: 'icon', name: 'icon'},
	            {data: 'parent', name: 'parent'},
	            {data: 'tipe', name: 'tipe'},
	            {data: 'urutan', name: 'urutan'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	function clear_input(){
		$("[name=popup_id]").val('');
		$("[name=popup_name]").val('');
		$("[name=popup_url]").val('');
		$("[name=popup_icon]").val('');
		$("[name=popup_urutan]").val('');
		$("[name=popup_parent]").val('').trigger("change");
		$("[name=popup_aktif]").val('1').trigger("change");
	}

	function edit(id){
		$("[name=popup_id]").val(id);
		$("[name=popup_name]").val($("#table_nama"+id).val());
		$("[name=popup_url]").val($("#table_url"+id).val());
		$("[name=popup_icon]").val($("#table_icon"+id).val());
		$("[name=popup_urutan]").val($("#table_urutan"+id).val());
		$("[name=popup_parent]").val($("#table_parent"+id).val()).trigger("change");
		$("[name=popup_aktif]").val($("#table_tipe"+id).val()).trigger("change");

		$("#modal_title").text("Edit");
		$("#examplemodal").modal('show');
	}


	$("#btn_simpan").click(function(){
		var nama 	= $("[name=popup_name]").val();
		var url 	= $("[name=popup_url]").val();
		var icon 	= $("[name=popup_icon]").val();
		var urutan 	= $("[name=popup_urutan]").val();
		var parent 	= $("[name=popup_parent]").val();
		if(nama != '' && url != '' &&  icon != '' && urutan != '' && parent != ''){
			$.ajax({
				url : "{{ url('menu/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal("hide");
		      	}
			})
		}
	})

	function hapus(id){
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('menu/hapus')}} ",
	          	type: 'post',
	          	data: {id : id},
			       headers : {
			       	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			       },
	          	success: function(respon){ 
	          		table.ajax.reload();
	          		// swal("Deleted!", "Your imaginary file has been deleted!", "success");
	         	}
        	})
    	})
	}

	/*function initTabelModul(){
    tabelModul = $("#tabel-menu").fancytree({
    extensions: ["table", "filter"],
    checkbox: false,
    table: {
          indentation: 10,      // indent 20px per node level
          nodeColumnIdx: 1,     // render the node title into the 1st column
          // checkboxColumnIdx: 0  // render the checkboxes into the 1st column
      },
      source: {
          url: "{{ url("menu/get_data") }}"
      },
      postProcess: function(event, data){
        data.result = convertData(data.response);
      },
      // lazyLoad: function(event, data) {
      //     data.result = {url: "ajax-sub2.json"}
      // },
      renderColumns: function(event, data) {
          var node = data.node,
          $tdList = $(node.tr).find(">td");
          
          $tdList.eq(0).text(node.getIndexHier()).attr("style", "text-align: right !important");

          var ndata = node.data;
          // (index #1 is rendered by fancytree)
          $tdList.eq(2).text(ndata.url);
          $tdList.eq(3).text(node.icon);
          $tdList.eq(4).text(ndata.parent_menu);
          $tdList.eq(5).text(ndata.tipe_site);
          $tdList.eq(6).text(ndata.urutan);
          $tdList.eq(7).html(ndata.aksi);

          var row = $(node.tr);

          $( row ).attr('data-id', ndata.id);
          $( row ).attr('data-name', ndata.name);
          $( row ).attr('data-url', ndata.url);
          $( row ).attr('data-icon', node.icon);
          $( row ).attr('data-parent', ndata.parent);
          $( row ).attr('data-site', ndata.tipe_site);
          $( row ).attr('data-urutan', ndata.urutan);

          // Style checkboxes
          // $(".styled").uniform();
      }
    });
  }*/
</script>
@endsection