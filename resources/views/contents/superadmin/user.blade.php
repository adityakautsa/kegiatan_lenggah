@extends('templates.index')
@section('title', 'Manajemen Pengguna')

@section('content')
<div class="page-header">
    <h1 class="page-title">Manajemen Pengguna</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Username</th>
	                 	<th>Nama</th>
	                 	<th>Gereja</th>
	                 	<th>Grup</th>
	                 	<th>Status</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal" autocomplete="off">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Member</label>
	            	<div class="col-lg-9">
	            		<select name="popup_member" id="popup_member" class="form-control" style="width: 100%">
		            		<option value=""> -- Pilih -- </option>
		            		@foreach($data['member'] as $p)
		            		<option value="{{ $p->id }}">{{ $p->nama }}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Grup</label>
	            	<div class="col-lg-9">
	            		<select name="popup_grup" id="popup_grup" class="form-control" style="width: 100%">
		            		<option value=""> -- Pilih -- </option>
		            		@foreach($data['grup'] as $p)
		            		<option value="{{ $p->id }}">{{ $p->nama_group }}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Username</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_user" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Password</label>
	            	<div class="col-lg-9">
	            		<input type="password" name="popup_pass" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Aktif</label>
	            	<div class="col-lg-9">
	            		<select name="popup_aktif" class="form-control" style="width: 100%">
	            			<option value="1">Aktif</option>
	            			<option value="0">Tidak Aktif</option>
	            		</select>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var table, store_pass = '-';
	$(document).ready(function(){
		
		$("[name=popup_grup], [name=popup_member], [name=popup_aktif]").select2({
			dropdownParent: $("#examplemodal")
		});


		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('user/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'username', name: 'username'},
	            {data: 'nama', name: 'nama'},
	            {data: 'nama_lokasi', name: 'nama_lokasi'},
	            {data: 'nama_group', name: 'nama_group'},
	            {data: 'status', name: 'status'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	function clear_input(){
		store_pass = '-';
		$("[name=popup_id]").val('');
		$("[name=popup_member]").val('').trigger('change');
		$("[name=popup_grup]").val('').trigger("change");
		$("[name=popup_user]").val('');
		$("[name=popup_pass]").val('');
		$("[name=popup_aktif]").val('1').trigger("change");
	}

	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var member = $("[name=popup_member]").val();
		var grup = $("[name=popup_grup]").val();
		var user = $("[name=popup_user]").val();
		var pass = store_pass;
		var aktif = $("[name=popup_aktif]").val();

		if(member != '' && grup != '' && user != '' && pass != ''){
			$.ajax({
				url : "{{ url('user/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal('hide');
		      	}
			})
		}else{

		}

	})

	function edit(this_){
		var id = $(this_).data("id");
		$.ajax({
			url : "{{ url('user/get_edit') }}",
			type : "POST",
			dataType : "json",
			data : { id : id },
			headers : {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
		    success : function(respon){
		    	if(respon.length > 0){
		    		show_edit(respon);
		    	}
		    }
		})
	}

	function show_edit(respon){
		var id = respon[0].id_user;
		var member = respon[0].id_member;
		var group = respon[0].id_group;
		var username = respon[0].username;
		var is_aktif = respon[0].active;

		$("[name=popup_id]").val(id);
		$("[name=popup_member]").val(member).trigger('change');
		$("[name=popup_grup]").val(group).trigger("change");
		$("[name=popup_user]").val(username);
		$("[name=popup_pass]").val('');
		$("[name=popup_aktif]").val(is_aktif).trigger("change");

		$("#modal_title").text("Edit");
		$("#examplemodal").modal('show');
	}

	function hapus(this_){
		var id = $(this_).data("id");
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ini",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('user/hapus')}} ",
	          	type: 'post',
	          	dataType : 'json',
	          	data: {id : id},
			    headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	          	success: function(respon){
	          		if(respon.status == 1){
	          			swal("", respon.keterangan, "success");
	          		}else{
	          			swal("", respon.keterangan, "error");
	          		}
	          		table.ajax.reload();
	         	}
        	})
    	})
	}

	function trigger(value){
		$(value).html("<option value=''> -- Pilih -- </option>");
		$(value).val("").trigger("change");
	}

	$("[name=popup_pass]").keyup(function(){
		store_pass = $(this).val();
	})

</script>
@endsection