@extends('templates.index')
@section('title', 'Master Ruang')

@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Ruang</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama Lokasi</th>
	                 	<th>Nama Ruang</th>
	                 	<th>Status</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal" autocomplete="off">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Gereja</label>
	            	<div class="col-lg-9">
	            		<select name="popup_lokasi" id="popup_lokasi" class="form-control" style="width: 100%">
		            		{{-- <option value=""> -- Pilih -- </option> --}}
		            		@foreach($data['lokasi'] as $p)
		            		<option value="{{ $p->id }}">{{ $p->nama }}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Ruang</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_ruang" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Aktif</label>
	            	<div class="col-lg-9">
	            		<select name="popup_aktif" class="form-control" style="width: 100%">
	            			<option value="1">Aktif</option>
	            			<option value="0">Tidak Aktif</option>
	            		</select>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$('#popup_lokasi').val($('#popup_lokasi option:first-child').val()).trigger('change');
		$("[name=popup_aktif], [name=popup_lokasi]").select2({
			dropdownParent: $("#examplemodal")
		});


		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('ruang/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama_lokasi', name: 'nama_lokasi'},
	            {data: 'nama', name: 'nama'},
	            {data: 'status', name: 'status'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	function clear_input(){
		$("[name=popup_id]").val('');
		$("[name=popup_ruang]").val('');
		// $("[name=popup_lokasi]").val('').trigger("change");
		$('#popup_lokasi').val($('#popup_lokasi option:first-child').val()).trigger('change');
		$("[name=popup_aktif]").val('1').trigger("change");
	}

	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var lokasi = $("[name=popup_lokasi]").val();
		var ruang = $("[name=popup_ruang]").val();
		var aktif = $("[name=popup_aktif]").val();

		if(lokasi != '' && ruang != ''){
			$.ajax({
				url : "{{ url('ruang/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal('hide');
		      	}
			})
		}else{

		}

	})

	function edit(this_){
		var id = $(this_).data("id");
		$.ajax({
			url : "{{ url('ruang/get_edit') }}",
			type : "POST",
			dataType : "json",
			data : { id : id },
			headers : {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
		    success : function(respon){
		    	if(respon.length > 0){
		    		show_edit(respon);
		    	}
		    }
		})
	}

	function show_edit(respon){
		var id = respon[0].id;
		var nama = respon[0].nama;
		var id_locations = respon[0].id_locations;
		var is_aktif = respon[0].is_aktif;

		$("[name=popup_id]").val(id);
		$("[name=popup_lokasi]").val(id_locations).trigger("change");
		$("[name=popup_ruang]").val(nama);
		$("[name=popup_aktif]").val(is_aktif).trigger("change");

		$("#modal_title").text("Edit");
		$("#examplemodal").modal('show');
	}

	function hapus(this_){
		var id = $(this_).data("id");
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ini",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('ruang/hapus')}} ",
	          	type: 'post',
	          	dataType : 'json',
	          	data: {id : id},
			    headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	          	success: function(respon){
	          		if(respon.status == 1){
	          			swal("", respon.keterangan, "success");
	          		}else{
	          			swal("", respon.keterangan, "error");
	          		}
	          		table.ajax.reload();
	         	}
        	})
    	})
	}


	function trigger(value){
		$(value).html("<option value=''> -- Pilih -- </option>");
		$(value).val("").trigger("change");
	}
</script>
@endsection