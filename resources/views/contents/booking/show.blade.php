@extends('templates.index')
@section('title', 'Booking')

@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/formvalidation/formValidation.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/timepicker/jquery-timepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/clockpicker/clockpicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/switchery/switchery.css')}}">
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<style type="text/css">
    #preview{
       width: 100%;
       height: 250;
       margin:0px auto;
    }

    .select2-container{
        z-index:100000;
    }
    .select2-container.select2-container--default.select2-container--open  {
      z-index: 5000;
    }
</style>
@endsection
@section('content')
<!-- Page header -->
<div class="page-header">
    <h1 class="page-title">Show</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Show</a></li>
      <li class="breadcrumb-item active"><a href="{{url('booking')}}">Scan QR</a></li>
    </ol>
</div>
<!-- /page header -->

<div class="page-content">
  <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">
          <strong>Petugas</strong>
        </h4>
      </div>
      <div class="panel-body">
        <div class="row row-lg">

          </div>
          <div class="col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label">Gate</label>
                <select class="form-control" id="gate" name="gate">
                    <option value=""> --- Pilih --- </option>
                    @foreach($data['gate'] as $d)
                    <option value="{{$d->id_gate}}" nama="{{$d->nama}}" {{($data['selected']==$d->id_gate)?'selected':''}}>{{$d->nama}}</option>
                    @endforeach
                </select>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 c_btn">
            @if(Agent::isDesktop() ||  Agent::browser())
            <button type="button" class="btn btn-success" id="scan_qr"><i class="icon-camera"></i> Scan QR (Website)</button>
            @endif
            <!-- <button type="button" class="btn btn-primary" onclick="get_data()"><i class="icon-plus-0"></i> Modal</button> -->
            @if(Agent::isTablet() || Agent::isMobile())
                <button type="button" class="btn btn-warning" id="scan_qr_native" onclick="addBarcode()"><i class="icon-camera"></i> Scan QR (APK)</button>
            @endif
            
          </div>
          <br>
          
        </div>

      </div>
    </div>
</div>

<div class="modal fade in bs-example-modal-lg" id="modal_qr" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title">Scan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12" id="div_select" style="display: none">
                    <div class="form-group">
                        <label class="control-label">Kamera</label>
                        <select class="form-control select" id="popup_camera">
                            
                        </select>
                    </div>
                </div>
                
                <div class="col-12 col-lg-12 col-md-12 col-sm-12" id="">
                    <video id="preview"></video>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <!-- <a type="button" class="btn btn-primary" id="btn_download" href="#">Download</a> -->
        </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modal_view" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
    <div class="modal-dialog modal-simple modal-top">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title">Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <form id="form_kursi" enctype="multipart/form-data" id="div_viewer">
            <input type="hidden" name="id_member">            
              <!-- <div class="col-md-12"> -->
                <select class="form-control" name="div_member">
                    <option value="0">Not Member</option>
                    @foreach($data['member'] as $d)
                    <option value="{{$d->id}}">{{$d->nama}}</option>
                    @endforeach
                </select>
                <!-- Example Basic Form (Form row) -->
                <div class="example-wrap">
                  <div class="example">
                    <center><h2 id="popup_gate"></h2></center>
                    <table width="100%">
                       <tr>
                        <td width="45%" class="text-left">Tgl Pesan</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%" class="text-left"><b id="popup_tglpesan"></b></td>
                      </tr> 
                      <tr>
                        <td width="45%" class="text-left">Tgl Acara</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%" class="text-left"><b id="popup_tglshow"></b></td>
                      </tr>
                    </table>
                    <br>
                    <table width="100%">
                      <tr>
                        <td width="45%">No.KTP</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%" id="no_ktp"></td>
                      </tr>
                      <tr>
                        <td>Nama Sesuai KTP</td>
                        <td class="text-center">:</td>
                        <td id="nama_ktp"></td>
                      </tr>
                      <tr>
                        <td>No. Telp</td>
                        <td class="text-center">:</td>
                        <td id="no_telp"></td>
                      </tr>
                      <tr>
                        <td>Alamat</td>
                        <td class="text-center">:</td>
                        <td id="alamat"></td>
                      </tr>
                      <tr>
                        <td>Paroki Asal</td>
                        <td class="text-center">:</td>
                        <td id="paroki"></td>
                      </tr>
                      <tr>
                        <td>Wilayah (Jika Algonz)</td>
                        <td class="text-center">:</td>
                        <td id="wilayah"></td>
                      </tr>
                      <tr>
                        <td>Lingkungan (Jika Algonz)</td>
                        <td class="text-center">:</td>
                        <td id="lingkungan"></td>
                      </tr>
                      <tr>
                        <td>Pernah Sakit Covid 19</td>
                        <td class="text-center">:</td>
                        <td id="covid"></td>
                      </tr>
                      <tr>
                        <td>Tanggal Dinyatakan Sembuh</td>
                        <td class="text-center">:</td>
                        <td id="tgl_covid"></td>
                      </tr>
                    </table>
                    <br>
                    <table class="table table-hover table-striped" id="tabel_kursi" style="width: 100%;">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">Kursi</th>
                                <th width="35%">Hadir</th>
                                <th width="35%">Pengunjung</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_kursi">
                        </tbody>
                    </table>
                  </div>
                </div>
                <!-- End Example Basic Form (Form row) -->
              <!-- </div> -->
              </form>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
        </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('themeforest/global/vendor/formvalidation/formValidation.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/formvalidation/framework/bootstrap.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/jquery.datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/switchery/switchery.js')}}"></script>


<script src="{{ asset('themeforest/global/js/Plugin/jquery-wizard.js') }}"></script>
<script src="{{ asset('themeforest/global/js/Plugin/matchheight.js') }}"></script>
<script src="{{ asset('themeforest/js/forms/wizard.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('themeforest/seat/jquery.seat-charts.js')}}"></script>
<script src="{{ asset('themeforest/seat/jquery.seat-charts.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.select2').select2();
    $('.select2_down').select2({
        dropdownParent: $("#modal_qr")
    });
    $('.timepicker2').timepicker({ 'timeFormat': 'H:i' });
});
</script>
<script type="text/javascript">

    // $(".reload").click(function(){
    //     table.ajax.reload();
    // })
    var tombol = "{{$data['tombol']}}";
    $('[name=div_member]').hide();
    
    if(tombol == 0){
        $('.c_btn').hide();
    }else{
        $('.c_btn').show();
    }
    var tabel = $('#tabel_kursi').DataTable( {
        responsive: true,
        processing  : true,
        searchDelay: 3000,
        pageLength: 5,
        dom: "frtip",
        bFilter: false, 
        bInfo: false,
        paging:false,
        "order": [[ 2, "asc" ]]
    });

    tabel.on( 'order.dt search.dt', function () {
        tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    // $('.select').select2();

   /* $("#gate").change(function(){

    })*/

    var scanner = new this.Instascan.Scanner({ video: document.getElementById("preview") });

    $("#scan_qr").click(function(){
        Instascan.Camera.getCameras().then(function (cameras){
            if(cameras.length>0){                
                
                if(cameras.length > 1){
                    $("#div_select").show();
                    $("#popup_camera").html("");
                    $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                    for(i in cameras){
                        $("#popup_camera").append("<option value='"+[i]+"'>"+cameras[i].name+"</option>");
                    }

                    if(cameras[1]!=""){
                        scanner.start(cameras[1]);
                        $("#popup_camera").val(1).change();
                    }else{
                        alert('Kamera tidak ditemukan!');
                    }
                }else{
                    if(cameras[0]!=""){
                        scanner.start(cameras[0]);
                        $("#popup_camera").val(0).change();
                    }else{
                        alert('Kamera tidak ditemukan!');
                    }
                }

                $("#popup_camera").change(function(){
                    var id = $("#popup_camera :selected").val();

                    if(id != ''){
                        if(cameras[id]!=""){
                            scanner.start(cameras[id]);
                        }else{
                            alert('Kamera tidak ditemukan!');
                        }
                    }
                })

            }else{
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e){
            console.error(e);
            alert(e);
        });
        tabel.clear().draw();
        $("#modal_qr").modal("show");
    })

    var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });
    scanner.addListener('scan',function(content){
        var gate = $("[name=gate]").val();
        if(gate != ''){
            if(content != ''){
                $("#modal_qr").modal("hide");
                // console.log(content);
                var item = content.split("||");
                window.location.href = "{{url('booking_scan/show')}}/"+item+'/'+gate;
                //get_data(item,gate);
                //$(".select2").select2();
            }
        }else{
            setTimeout(function(){
                swal({title:"Data Error!", text:"Gate Harus Diisi", type:"error"},function(){
                    $("#modal_qr").modal("hide");
                },1000);
              })
        }
        // get_data();
    });

    $('#modal_qr').on('hidden.bs.modal', function () {
        scanner.stop();
    })

    function get_data(id=32,gate){
        $("[name=id_member]").val(id);
        $("#no_ktp").text('');
        $("#nama_ktp").text('');
        $("#alamat").text('');
        $("#covid").text('');
        $("#tgl_sembuh").text('');
        tabel.clear().draw();
        $(".select2").html('');
        $.ajax({
              url: "{{ url('booking_scan/get_data')}} ",
              type: 'post',
              data: {id : id,gate:gate},
              dataType: 'JSON',
              headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(respon){
                
                if(respon.data.length > 0){
                    $("#qr_barcode").attr("src","https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl="+id+"&choe=UTF-8");
                    $("#no_ktp").text(respon.data[0].no_ktp);
                    $("#nama_ktp").text(respon.data[0].nama_ktp);
                    $("#alamat").text(respon.data[0].alamat);
                    $("#covid").text(respon.data[0].status_covid);
                    $("#tgl_covid").text(respon.data[0].tgl_sembuh);
                    $("#paroki").text(respon.data[0].paroki);
                    $("#wilayah").text(respon.data[0].wilayah);
                    $("#lingkungan").text(respon.data[0].lingkungan);
                    $("#popup_gate").text($("[name=gate] :selected").attr('nama'));
                    $("#popup_tglshow").text(respon.data[0].tgl_show);
                    $("#popup_tglpesan").text(respon.data[0].tgl_pesan);
                    $("#no_telp").text(respon.data[0].no_telp);
                }

                
                var arr_member = [];
                var arr_idmember = [];
                if(respon.detail.length > 0){
                    for(i in respon.detail){
                        var checked = '';
                        if(respon.detail[i].status == 1){
                            var checked = "checked";
                        }
                        
                        tabel.row.add(['<div><center></center></div>',
                        '<div id="list_kursi'+respon.detail[i].id+'"><center>'+respon.detail[i].nama_seat+'</center></div>',
                        '<div id="list_hadir'+respon.detail[i].id+'" class="text-center"><input type="checkbox" data-plugin="switchery" data-color="#17b3a3" class="js-check-click" id="text_switch" value="1" name="tabel_hadir[]" '+checked+'/></div>',
                        /*'<div id="list_pengunjung'+respon.detail[i].id+'" class="text-center"><select class="form-control select2" width="100%" name="tabel_member[]" id_member="tabel_member'+respon.detail[i].id+'"></select></div>'+*/    
                        '<div id="list_pengunjung'+respon.detail[i].id+'" class="text-center"><select class="form-control select2" width="100%" name="tabel_member[]" id="tabel_member'+respon.detail[i].id+'">'+
                            $('[name=div_member]').html()+'</select></div>'+                    
                        "<input type='hidden' name='tabel_idmember[]' value='"+respon.detail[i].id+"'>"
                        ]).draw(false);
                        arr_member.push(respon.detail[i].id);
                        arr_idmember.push(respon.detail[i].id_member);
                        $('#tabel_member'+respon.detail[i].id).val(respon.detail[i].id_member).trigger('change');
                    }
                    console.log(arr_member);
                    console.log(arr_idmember);

                }
                 // for(i in arr_member){
                 //        $('#tabel_member'+arr_member[i]).val(arr_idmember[i]).trigger('change');
                 //    }


                // if(respon.member.length > 0){
                //     $(".select2").append("<option value='0'>Not Member</option>");
                //     for(i in respon.member){
                //         $(".select2").append("<option value='"+respon.member[i].id+"'>"+respon.member[i].nama+"</option>");
                //     }
                // }
              }
          })
        $("#modal_view").modal("show");
    }

    function popup(this_){
        var show = "";
        var link = this_.attr('link');
        var link_2 = this_.attr('link_2');
        var nama_file = this_.attr('nama_file');
        var tipe = this_.attr('tipe-file');
        var arr_image = ['jpg', 'jpeg', 'png'];
        var id = this_.attr('id_lampiran');
        $("#div_viewer").html("");
        
        var ext = (/[.]/.exec(nama_file)) ? /[^.]+$/.exec(nama_file) : "";
        var html  = '<iframe id="iframe_data" src="" width="100%" height="400" style="min-height: 300px; max-height: 80%"></iframe>';
        
        if(ext[0] == 'pdf'){
            $("#div_viewer").append(html);
            show = link;
        }else{
            for(i in arr_image){
                if(arr_image[i] == ext[0]){
                    $("#div_viewer").append(html);
                }
            }
            show = link_2;
        }

        $("#iframe_data").attr("src", show);
        $("#btn_download").attr("href", "<?= url('hasillab_mobile/unduh')."/" ?>"+id);
        $("#modal_view").modal('show');
    }

    $("#btn_simpan").click(function(){
            $.ajax({
                url : "{{ url('booking_scan/simpan') }}",
                type : "POST",
                dataType : "json",
                data : $("#form_kursi").serialize(),
                headers : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(respon){
                    // tabel.ajax.reload();
                    tabel.clear().draw();
                    $("#modal_view").modal("hide");
                }
            })
    })

    function addBarcode(){
        var gate = $("[name=gate]").val();
        if(gate != ''){
        Android.moveQrcode(gate);
        }else{
           setTimeout(function(){
                swal({title:"Data Error!", text:"Gate Harus Diisi", type:"error"},function(){
                   window.location.href = "{{url('booking_scan')}}"; 
                },1000);
              }) 
        }
    }
</script>

@endsection