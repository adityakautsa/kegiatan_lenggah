@extends('templates.index')
@section('title', 'Booking')

@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/formvalidation/formValidation.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/timepicker/jquery-timepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/clockpicker/clockpicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/switchery/switchery.css')}}">
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<style type="text/css">
    #preview{
       width: 100%;
       height: 250;
       margin:0px auto;
    }

    .select2-container{
        z-index:100000;
    }
    .select2-container.select2-container--default.select2-container--open  {
      z-index: 5000;
    }
</style>
@endsection
@section('content')
<!-- Page header -->
<div class="page-header" id="div_header">
    <h1 class="page-title">Show</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Show</a></li>
      <li class="breadcrumb-item active"><a href="{{url('booking')}}">Scan QR</a></li>
    </ol>
</div>
<!-- /page header -->

<div class="page-content" id="div_content">
  <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">
          <strong>Detail Scan QR</strong>
        </h4>
      </div>
      <div class="panel-body">
        <div class="row row-lg">
        	<div class="col-sm-12 col-md-12">
            <form action="{{url('booking_scan/simpan2')}}" id="form_kursi" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_member" value="{{$data['id']}}">
            <input type="hidden" name="id_gate" value="{{$data['gate']->id_gate}}">
                <div class="example-wrap">
                  <div class="example">
                    <center><h2 id="popup_gate">{{$data['gate']->nama}}</h2></center>
                    <table width="100%">
                       <tr>
                        <td width="45%" class="text-left">Tgl Pesan</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%" class="text-left"><b>{{tgl_full($data['data']->created_at,'78')}}</b></td>
                      </tr> 
                      <tr>
                        <td width="45%" class="text-left">Tgl Acara</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%" class="text-left"><b>{{tgl_full($data['data']->waktu,'1').' '.tgl_full($data['data']->jam,'100')}}</b></td>
                      </tr>
                    </table>
                    <br>
                    <table width="100%">
                      <tr>
                        <td width="45%">No.HP</td>
                        <td width="10%" class="text-center">:</td>
                        <td width="45%">{{$data['data']->no_hp}}</td>
                      </tr>
                      <tr>
                        <td>Nama Sesuai KTP</td>
                        <td class="text-center">:</td>
                        <td>{{$data['data']->nama_ktp}}</td>
                      </tr>
                      <tr>
                        <td>Alamat</td>
                        <td class="text-center">:</td>
                        <td>{{$data['data']->alamat}}</td>
                      </tr>
                      <tr>
                        <td>Paroki Asal</td>
                        <td class="text-center">:</td>
                        <td>{{$data['data']->nama_lokasi}}</td>
                      </tr>
                      <tr>
                        <td>Wilayah (Jika Algonz)</td>
                        <td class="text-center">:</td>
                        <td>{{$data['data']->nama_wilayah}}</td>
                      </tr>
                      <tr>
                        <td>Lingkungan (Jika Algonz)</td>
                        <td class="text-center">:</td>
                        <td>{{$data['data']->nama_lingkungan}}</td>
                      </tr>
                      <tr>
                        <td>Pernah Sakit Covid 19</td>
                        <td class="text-center">:</td>
                        <td>{{($data['data']->status_covid==0)?'Tidak':'Ya'}}</td>
                      </tr>
                      <tr>
                        <td>Tanggal Dinyatakan Sembuh</td>
                        <td class="text-center">:</td>
                        <td>{{($data['data']->status_covid==0)?'':tgl_full($data['data']->tgl_sembuhcovid,'1')}}</td>
                      </tr>
                    </table>
                    <br>
                    <div class="table-responsive">
                    <table class="table table-hover table-striped" id="tabel_kursi" style="width: 100%;">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">Kursi</th>
                                <th width="35%">Hadir</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_kursi">
                        	@php
                        	$no = 1;
                        	@endphp
                        	@foreach($data['detail'] as $d)
                        	<tr>
                        		<td>{{$no++}}</td>
                        		<td>{{$d->nama_area.' - '.$d->baris.$d->kolom}}</td>
                        		<td>
                            @if($d->status == 1)
                            Sudah Hadir
                            @endif
                            @if($d->status == 0)
                            <input type="checkbox" data-plugin="switchery" data-color="#17b3a3" class="js-check-click text_switch" id="text_switch" idtable="{{$d->id}}" onchange="getcheckbox($(this))" name="tabel_idhadir[]" {{($d->status==1)?'checked':''}} />
                            @endif
                        		<input type="hidden" name="tabel_idmember[]" value="{{$d->id_member}}">
                        		<input type="hidden" name="tabel_id[]" value="{{$d->id}}">
                            <input type="hidden" name="tabel_hadir[]" id="tabel_hadir{{$d->id}}" value="{{$d->status}}">
                        		</td>
                        	</tr>
                        	@endforeach
                        </tbody>
                    </table>
                    </div>
                  </div>

                  <div class="footer">
                  		<a href="{{url('booking_scan')}}" class="btn btn-default"><i class="icon wb-arrow-left"></i> Kembali</a>
                    	<button type="submit" class="btn btn-primary float-right"><i class="icon fa-save"></i>Simpan</button>
              	  </div>
                </div>
                <!-- End Example Basic Form (Form row) -->
              <!-- </div> -->
              </form>
          	</div>
          </div>
          <br>
          
        </div>

      </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('themeforest/global/vendor/formvalidation/formValidation.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/formvalidation/framework/bootstrap.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/jquery.datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/switchery/switchery.js')}}"></script>


<script src="{{ asset('themeforest/global/js/Plugin/jquery-wizard.js') }}"></script>
<script src="{{ asset('themeforest/global/js/Plugin/matchheight.js') }}"></script>
<script src="{{ asset('themeforest/js/forms/wizard.js') }}"></script>


<script src="{{ asset('themeforest/seat/jquery.seat-charts.js')}}"></script>
<script src="{{ asset('themeforest/seat/jquery.seat-charts.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#div_header,#div_content').hide();
		var status = "{{$data['status']['id']}}";
    var gate   = "{{$data['gate']->id_gate}}";

		$('.select2').select2();
		var tabel = $('#tabel_kursi').DataTable( {
	        responsive: true,
	        processing  : true,
	        searchDelay: 3000,
	        pageLength: 5,
	        dom: "frtip",
	        bFilter: false, 
	        bInfo: false,
	        paging:false,
	        "order": [[ 2, "asc" ]]
	    });

	    tabel.on( 'order.dt search.dt', function () {
	        tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    if(status == 1){
	    	$('#div_header,#div_content').show();
	    }else{
	    	$('#div_header,#div_content').hide();
	    	var s_title = "{{$data['status']['title']}}";
	    	var s_text  = "{{$data['status']['text']}}";
	    	var s_type  = "{{$data['status']['type']}}";
	    	swal({title:s_title, text:s_text, type:s_type},function(){
                window.location.href = "{{url('booking_scan')}}/"+gate;
            },1000);
	    }

    

	})

  function getcheckbox(thiss){
    var id = thiss.attr('idtable');
    var cek = thiss.attr('checked',true);
      if(cek[0].checked==true){
          $("#tabel_hadir"+id).val(1);
      }else{
          $("#tabel_hadir"+id).val(0);
      }
    }
</script>
@endsection