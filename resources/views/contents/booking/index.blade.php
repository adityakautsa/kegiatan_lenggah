@extends('templates.index')
@section('title', 'Pesan')

@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/formvalidation/formValidation.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">

<!-- <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/select2/select2.css') }}"> -->
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<!-- <link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css')}}"> -->
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/timepicker/jquery-timepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/clockpicker/clockpicker.css')}}">
<style type="text/css">
.front{margin: 0 0 32px 32px;background-color: #f0f0f0; color: #666;text-align: center;padding: 3px;border-radius: 5px;} 
div.seatCharts-cell {color: #182C4E;height: 25px;width: 25px;line-height: 25px;margin: 3px;float: left;text-align: center;outline: none;font-size: 13px;} 
.demo {
  width: 100%;
  text-align: center;
}
.container {
  margin: 0 auto;
  width: 500px;
  text-align: left;
}
div.seatCharts-seat {color: #fff;cursor: pointer;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius: 5px;} 
div.seatCharts-row {height: 35px;} 
div.seatCharts-seat.available {background-color: #B9DEA0;} 
div.seatCharts-seat.focused {background-color: #76B474;border: none;} 
div.seatCharts-seat.selected {background-color: #E6CAC4;} 
div.seatCharts-seat.unavailable {background-color: #E6CAC4;cursor: not-allowed;} 
div.seatCharts-container {width: 100%;float: center;} 
div.seatCharts-seat.available_s {background-color: #3a78c3;}
div.seatCharts-legend {padding-left: 20px;bottom: 300px;} 
ul.seatCharts-legendList {padding-left: 0px;} 
.seatCharts-legendItem{float:left; margin-top: 10px;line-height: 2; padding-right: 30px;} 
span.seatCharts-legendDescription {margin-left: 5px;line-height: 30px;} 
.checkout-button {display: block;width:80px; height:24px; line-height:20px;margin: 10px auto;border:1px solid #999;font-size: 14px; cursor:pointer} 
#selected-seats {max-height: 150px;overflow-y: auto;overflow-x: none;width: 350px;} 
#selected-seats li{float:left; line-height:10px; border:1px solid #d3d3d3; background:#f7f7f7; margin:6px; font-size:14px; font-weight:bold; text-align:left; width: 50%;} 
</style>
@endsection
@section('content')
<!-- Page header -->
<div class="page-header">
    <h1 class="page-title">Pesan</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Pesan</a></li>
      <li class="breadcrumb-item active"><a href="{{url('booking')}}">Pesan</a></li>
    </ol>
</div>
<!-- /page header -->

      <div class="page-content container-fluid">
        <div class="row">
          <div class="col-12 col-lg-12 col-md-12 col-sm-12">
            <!-- Panel Wizard Form Container -->
            <div class="panel" id="exampleWizardFormContainer">
              <div class="panel-heading">
                <h3 class="panel-title">Pesan Steps</h3>
              </div>
              <div class="panel-body">
                <!-- Steps -->
                <div class="pearls row">
                  <div class="pearl current col-4">
                    <div class="pearl-icon"><i class="icon fa-ticket" aria-hidden="true"></i></div>
                    <span class="pearl-title">Pesan</span>
                  </div>
                  <div class="pearl col-4">
                    <div class="pearl-icon"><i class="icon fa-braille" aria-hidden="true"></i></div>
                    <span class="pearl-title">Kursi</span>
                  </div>
                  <div class="pearl col-4">
                    <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                    <span class="pearl-title">Konfirmasi</span>
                  </div>
                </div>
                <!-- End Steps -->

                <!-- Wizard Content -->
                <form action="{{url('booking_simpan')}}" id="exampleFormContainer" autocomplete="off" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="wizard-content">
                    <div class="wizard-pane active" id="exampleAccountOne" role="tabpanel">
                      <div class="form-group">
                        <label class="form-control-label" for="inputPasswordOne">Tanggal/Jam</label>
                          <div class="input-daterange">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                              </div>
                              <input type="text" class="form-control datepicker" name="tanggal" readonly="">
                            </div>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon wb-time" aria-hidden="true"></i>
                                </span>
                              </div>
                              <!-- <input type="text" class="form-control datepair-time datepair-start timepicker2" data-plugin="timepicker2" data-timeFormat="H:i" name="waktu"  /> -->
                              <select class="form-control select2" name="waktu" required="">
                              </select>
                              <!-- <input type="form-control select2" name="waktu" required=""> -->
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="inputPasswordOne">Pengunjung</label>
                        <div class="input-group">
                        <input type="text" class="form-control" name="pengunjung" data-plugin="TouchSpin" data-min="1" data-max="5" value="1" required="" readonly="" />
                        </div>
                      </div>
                      <br>
                    </div>
                    <div class="wizard-pane" id="exampleBillingOne" role="tabpanel">
                      <div class="col-sm-12 col-md-12">

                        <div class="form-group">
                            <label class="form-control-label" for="inputUserNameOne">Area</label>
                            <!-- <div class="input-group"> -->
                            <select class="form-control" name="area" required="" style="width: 100%;">
                                @foreach($data['area'] as $d)
                                <option value="{{$d->id}}" gate="{{$d->nama_gate}}" foto="{{$d->foto}}">{{$d->nama}} Jumlah Kursi ({{($d->jumlah_kursi-$d->jumlah_booking)}})</option>
                                @endforeach
                            </select>
                            <!-- </div> -->
                        </div>
                        <div class="front"><img src="" width="300px" height="300px" id="foto_area"></div>
                        <div><center><h5 class="mt-0 mb-5" style="color: red"><i id="catatan_area">“Mohon diperhatikan gambar layout ruangan, tombol kursi tidak merepresentasikan layout ruangan sebenarnya”</i></h5 class="mt-0 mb-5"></center></div>
                        <div class="table-responsive">
                        <div class="demo">
                          <div class="container">
                              <div id="seat-map">                                
                                <div id="seat-info"></div>                                
                              </div>
                              <div id="selected-seats"></div>
                              <div id="selected_kursi"></div>
                            <div style="clear:both"></div>
                          </div>
                         </div>
                       </div>
                      <div id="legend"></div>
                      <div style="clear:both"></div>

                      </div>


                    </div>
                    <div class="wizard-pane" id="exampleGettingOne" role="tabpanel">
                      <div class="text-center my-20">
                        <h4>Detail Booking</h4>
                        <div class="table-responsive">
                          <input type="hidden" name="txt_jadwal" value="1">
                          <input type="hidden" name="txt_tgljadwal" value="2020-12-11">
                          <input type="hidden" name="txt_jenisjadwal" value="1">
                          <input type="hidden" name="txt_idmember" value="{{$data['member']->id}}">
                          <input type="hidden" name="txt_idlocation" value="1">
                          <table>
                            <tbody>
                              <tr>
                                <td class="text-left">Pernah Sakit Covid 19</td>
                                <td>:</td>
                                <td id="txt_statusCovid"><select class="form-control select2" id="covid" name="covid" style="width: 100%;"><option value="0">Tidak</option><option value="1">Ya</option></select></td>
                              </tr>
                              <tr>
                                <td class="text-left">Tanggal Dinyatakan Sembuh</td>
                                <td>:</td>
                                <td id="txt_tglSembuh"><input type="text" name="tanggal_covid" class="form-control datepickerall"></td>
                              </tr>
                              <tr><td><br></td></tr>
                              <tr>
                                <td class="text-left" colspan="4">Dengan Pemesanan kursi ini, saya menyatakan bahwa saya beserta rombongan dalam keadaan kesehatan yang baik dan bersedia ditolak masuk lokasi apabila suhu diatas 38&#8451; dan atau mengalami sakit flu.
                                    Dan saya bersedia mematuhi semua protokol kesehatan Covid19 yang dijalankan secara tegas oleh para petugas dilokasi Surabaya.</td>
                              </tr>
                              <tr>
                                <td class="text-left" colspan="4"></td>
                              </tr>
                              <tr>
                                <td class="text-left" colspan="4">Lokasi Pass dan KTP wajib dibawa saat akan menghadiri Kegiatan Datanglah 30 menit sebelum Kegiatan dimulai.
                                Petugas berhak menolak anda masuk untuk mengikuti kegiatan, apabila terjadi ketidasamaan data yang anda</td>
                              </tr>
                              <tr><td><br></td></tr>
                              <tr>                                 
                                <td class="text-left" style="color: red;"><input type="checkbox" class="icheckbox-grey txt_syarat" id="inputColorGrey" name="inputiCheckColorCheckboxes"
                                  data-plugin="iCheck" data-checkbox-class="icheckbox_flat-grey" value="1" required="">&nbsp;&nbsp;&nbsp;*) Saya menyetujui dengan aturan-aturan dan ketentuan yang diatas</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <!-- Wizard Content -->
              </div>
            </div>
            <!-- End Panel Wizard Form Container -->
            
        <div class="panel" id="panel-qrcode">
          <div class="panel-heading">
            <h3 class="panel-title">QRCode & Detail Pesan</h3>
          </div>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12">
                <center><h2 id="gate"></h2></center>
                <table width="100%">
                  <tr>
                    <td width="22.5%" class="text-left">Tgl Pesan</td>
                    <td width="5%" class="text-center">:</td>
                    <td width="22.5%" class="text-left"><b id="kon_booking"></b></td>
                    <td width="22.5%" class="text-left">Tgl Acara</td>
                    <td width="5%" class="text-center">:</td>
                    <td width="22.5%" class="text-left"><b id="kon_show"></b></td>
                  </tr>
                </table>
              </div>
              <div class="col-md-6">
                <!-- Example Basic Form (Form grid) -->
                <div class="example-wrap">
                  <div class="example">
                      <div class="form-group">
                        <center><img id="qr_barcode"></center>
                      </div>
                  </div>
                </div>
                <!-- End Example Basic Form (Form grid) -->
              </div>

              <div class="col-md-6">
                <!-- Example Basic Form (Form row) -->
                <div class="example-wrap">
                  <div class="example">
                    <table width="100%" style="margin-top: 30px;">
                      <tr>
                        <td width="40%">No.KTP</td>
                        <td width="5%">:</td>
                        <td width="55%" id="qr_ktp">{{$data['member']->nik}}</td>
                      </tr>
                      <tr>
                        <td>Nama Sesuai KTP</td>
                        <td>:</td>
                        <td id="qr_nama">{{$data['member']->nama}}</td>
                      </tr>
                      <tr>
                        <td width="40%">No. Telp</td>
                        <td width="5%">:</td>
                        <td width="55%" id="qr_ktp">{{$data['member']->no_telp}}</td>
                      </tr>
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td id="qr_alamat">{{$data['member']->alamat}}</td>
                      </tr>
                      <tr>
                        <td>Paroki Asal</td>
                        <td>:</td>
                        <td id="qr_paroki">{{ucwords($data['member']->nama_lokasi)}}</td>
                      </tr>
                      <tr>
                        <td>Wilayah (Jika Algonz)</td>
                        <td>:</td>
                        <td id="qr_wilayah">{{ucwords($data['member']->nama_wilayah)}}</td>
                      </tr>
                      <!-- <tr>
                        <td>Lingkungan (Jika Algonz)</td>
                        <td>:</td>
                        <td id="qr_lingkungan">{{ucwords($data['member']->nama_lingkungan)}}</td>
                      </tr> -->
                      <tr>
                        <td>Jumlah Pemesanan Kursi</td>
                        <td>:</td>
                        <td id="qr_jumlahKursi"></td>
                      </tr>
                      <tr>
                        <td>Pernah Sakit Covid 19</td>
                        <td>:</td>
                        <td id="qr_covid"></td>
                      </tr>
                      <tr>
                        <td>Tanggal Dinyatakan Sembuh</td>
                        <td>:</td>
                        <td id="qr_tglCovid"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- End Example Basic Form (Form row) -->
              </div>

              <div class="col-md-12" style="margin-top: -80px;">
                <!-- Example Basic Form (Form row) -->
                <div class="example-wrap">
                  <div class="example">
                    <div class="form-group">
                      <h4 class="example-title">Menyetujui Disclaimer,</h4>
                      <p>Dengan Pemesanan kursi ini, saya menyatakan bahwa saya beserta rombongan dalam keadaan kesehatan yang baik dan bersedia ditolak masuk lokasi apabila suhu diatas 38&#8451; dan atau mengalami sakit flu. Dan saya bersedia mematuhi semua protokol kesehatan Covid19 yang dijalankan secara tegas oleh para petugas dilokasi Surabaya.</p>
                      <p>Lokasi Pass dan KTP wajib dibawa saat akan menghadiri Kegiatan Datanglah 30 menit sebelum Kegiatan dimulai.Petugas berhak menolak anda masuk untuk mengikuti kegiatan, apabila terjadi ketidasamaan data yang anda</p>
                    </div>
                    <div class="form-group text-right"><a href="{{url('booking')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a></div>
                  </div>
                </div>
                <!-- End Example Basic Form (Form row) -->
              </div>
            </div>
          </div>
        </div>

          </div>
        </div>

      </div>

<div class="modal fade" id="examplePositionSidebar" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog">
  <div class="modal-dialog modal-simple modal-sidebar modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title">Detail</h4>
            </div>
            <form id="form_modal">
            <div class="modal-body" id="modal_body">
              
              
            </div>
            <div class="modal-footer" id="modal_footer">
              
            </div>
            </form>
        </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('themeforest/global/vendor/formvalidation/formValidation.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/formvalidation/framework/bootstrap.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/jquery-wizard/jquery-wizard.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/datepair/jquery.datepair.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>


<script src="{{ asset('themeforest/global/js/Plugin/jquery-wizard.js') }}"></script>
<script src="{{ asset('themeforest/global/js/Plugin/matchheight.js') }}"></script>
<script src="{{ asset('themeforest/js/forms/wizard.js') }}"></script>

<!-- <script src="{{ asset('themeforest/global/vendor/select2/select2.full.min.js')}}"></script> -->
<script src="{{ asset('themeforest/seat/jquery.seat-charts.js')}}"></script>
<script src="{{ asset('themeforest/seat/jquery.seat-charts.min.js')}}"></script>
<script type="text/javascript">
  var table_kursi;
  var date_now = new Date();
  var rangetanggal = parseInt('{{$data['rangetanggal']}}');
$(document).ready(function() {
    $('.select2').select2();
    $('.select2_area').select2({dropdownParent: $('#examplePositionSidebar')});
    $('.datepicker_covid').datepicker({
      format : 'dd-mm-yyyy',
      setDate : new Date(),
      startDate : new Date(),
      autoclose : true
    })
    $('.datepicker').datepicker({
        format : 'dd-mm-yyyy',
        setDate: new Date(),
        startDate: new Date(),
        endDate: new Date(date_now.getFullYear(), date_now.getMonth(), date_now.getDate()+rangetanggal),
        autoclose: true,
        clearBtn: true,
    });
    $('.datepickerall').datepicker({
        format : 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true,
        clearBtn: true,
    });
    $(".datepicker").on("changeDate", function (e) {
          var tgl = e.date;
          var tanggalFull = tgl.getDate()+'-'+(tgl.getMonth()+1)+'-'+tgl.getFullYear();
          var hari = tgl.getDay();
          jadwal_tanggal(tanggalFull,hari);

      });
    // $('.timepicker2').timepicker({ 'timeFormat': 'H:i' });
    // $('.timepicker2').attr('readonly',true);
    $("[name=waktu]").on("change", function (e){
        var time = e.target.value;
        jadwal_jam(time);
    })
    $("#panel-qrcode").hide();

});
var price = 10; //price
$(document).ready(function() {
    getseats();
   
});
$("[name=area]").change(function(){ 
    var id = $(this).val();
    var id_jadwal = $("[name=txt_jadwal]").val();
    var foto_area = $("[name=area] :selected").attr('foto');
    var tanggal   = $("[name=tanggal]").val();
    getseats(id,id_jadwal,tanggal);
    $("#selected_kursi").html('');
    $("#foto_area").attr('src','{{url("images_area")}}/'+foto_area);
});
function getseats(id=1,jadwal='',tanggal=''){   
    $('.seatCharts-row').remove();
    $('.seatCharts-legendItem').remove();
    $('#seat-map,#seat-map *').unbind().removeData();
   $.ajax({
      url: "{{ url('booking_seats')}} ",
      type: 'post',
      data: {id : id, jadwal : jadwal, tanggal : tanggal},
      dataType: 'JSON',
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(respon){
      $('.container').css({
          "margin": "0 auto",
          "width": respon.jum_kolom+"px",
          "text-align": "left"
      });
      var $cart = $('#selected_kursi'), //Sitting Area
      $counter = $('#counter'), //Votes
      $total = $('#total'); //Total mondey
  
  var sc = $('#seat-map').seatCharts({
    map: respon.map,
    seats: { //Definition seat property
      f: {
        price   : 100,
        classes : 'first-class', 
        category: '1st'
      },
      e: {
        price   : 40,
        classes : 'economy-class', 
        category: '2nd'
      },

    },
    naming : { //Define the ranks of other information
      top : true,
      columns: respon.column,
      rows: respon.rows,
      getLabel : function (character, row, column) {
        return row+column;
        // return respon.area+column
      }
    },
    legend : { //Definition legend
      node : $('#legend'),
      items : [
        [ 'f', 'available_s',   'Sudah Ditempati' ],
        [ 'e', 'available',   'Belum Ditempati'],
        [ 'f', 'unavailable', 'Pilihan Kursi']
      ]         
    },
    click: function () {
      if (this.status() == 'available') {
    //Optional seat    
        if(sc.find('selected').length+1 > $("[name=pengunjung]").val()){
        return 'available';
        }else{
        $("<input type='hidden' value='"+idSeats(respon.seats,this.settings.id)+"' kursi='"+this.settings.id+"' id='kursi_"+this.settings.id+"' name='kursi[]'>").data('seatId',this.settings.id).appendTo($cart);
        $("<input type='hidden' value='"+$("[name=txt_idmember]").val()+"' kursi='"+this.settings.id+"' id='member_"+this.settings.id+"' name='member[]'>").data('seatId',this.settings.id).appendTo($cart);

        // fill_modal(this.settings.id);
        // $("#examplePositionSidebar").modal("show");
        //Update tickets
        $counter.text(sc.find('selected').length+1);
        var arr_kursi = $("input[name='kursi[]']").map(function(){return $(this).attr('kursi').replace('_','');}).get().join(',');
        var jum_kursi = sc.find('selected').length+1;
        var text_kursi = jum_kursi+" ("+arr_kursi+")";
        $("#qr_jumlahKursi").text(text_kursi);
        $("#txt_jumlahPengunjung").text(text_kursi);
        // input_text();
        return 'selected';    
        }
      } else if (this.status() == 'selected') {//choosen
        $counter.text(sc.find('selected').length-1);
        var arr_kursi = $("input[name='kursi[]']").map(function(){return $(this).attr('kursi').replace('_','');}).get().join(',');
        var jum_kursi = sc.find('selected').length-1;
        var text_kursi = jum_kursi+" ("+arr_kursi+")";
        $("#qr_jumlahKursi").text(text_kursi);
        $("#txt_jumlahPengunjung").text(text_kursi);
        //Delete reservation
        $('#cart-item-'+this.settings.id).remove();
        $('#kursi_'+this.settings.id).remove();
        $('#member_'+this.settings.id).remove();
        return 'available';
      } else if (this.status() == 'unavailable') {//sold
        //sold
        return 'unavailable';
      } else {
        return this.style();
      }
      
    },
  });
  //sold seat
  sc.get(respon.booking).status('available_s');
   
   }
 })
}

function idSeats(respon, id){
    var item = respon.filter(item => item.seats===id);
    return item[0].id;
}

function input_text(){
    $("#txt_wilayah").text($("[name=area]").val());
    $("#txt_jumlahPengunjung").text($("[name=pengunjung]").val());
}

$('.txt_syarat').change(function(){
  if($(this).checked){
    $('#exampleWizardFormContainer').attr('role','submit');
  }
});

function submit_wizard(){
  $.ajax({
      url : "{{url('booking_simpan')}}",
      type : "post",
      data : $('#exampleFormContainer').serialize(),
      dataType : "json",
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success : function(result){
        if(result.status == 1){
          $("#exampleWizardFormContainer").hide();
          $("#panel-qrcode").show();
          $("#qr_barcode").attr("src","https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl="+result.id+"&choe=UTF-8");
          $("#gate").text($("[name=area] :selected").attr('gate'));
          $("#kon_booking").text(result.tgl_booking);
          $("#kon_show").text(result.tgl_show);
        }else if(result.status == 2){
          setTimeout(function(){
            swal({title:"Kursi Sudah Ditempati", text:"Kursi yang dipilih sudah ditempati", type:"error"},function(){
              // window.location = "{{url('booking')}}";
            },1000);
          })
        }else if(result.status == 3){
          setTimeout(function(){
            swal({title:"Batas Pesan Sudah Habis", text:"Batas Pemesanan kursi maksimal 5", type:"error"},function(){
              window.location = "{{url('booking')}}";
            },1000);
          })
        }else{
          setTimeout(function(){
            swal({title:"Data Error!", text:"Pesan Error", type:"error"},function(){
            },1000);
          })
        }
      }
  })
}

function jadwal_tanggal(tanggal,hari){
    var id_lokasi = $("[name=txt_idlocation]").val();
    $.ajax({
      url: "{{ url('booking_jadwal_tanggal') }}",
      type: 'post',
      dataType: 'json',
      data: { id_lokasi : id_lokasi, tanggal : tanggal, hari:hari},
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(respon){
        $('[name=waktu]').html('');
        if(respon.length > 0){
          $('.timepicker2').attr('readonly',false);
          $('[name=txt_tgljadwal]').val(respon[0]['tanggal']);
          for(i in respon){
          $('[name=waktu]').append('<option value="'+respon[i].id_jadwal+'">'+respon[i].jam+'</option>');
          }
          $('[name=waktu]').val(respon[0].id_jadwal).trigger('change');
        }else{
          $('.timepicker2').attr('readonly',true);
          $('[name=txt_tgljadwal]').val('');
          swal("Tidak Ditemukan", "Jadwal tidak ada untuk tanggal ini", "error");
        }
      }
    });
}

function jadwal_jam(id){
    $.ajax({
      url: "{{ url('booking_jadwal_jam') }}",
      type: 'post',
      dataType: 'json',
      data: { id:id},
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(respon){
        if(respon.length > 0){
          get_area(respon[0]['area'],respon[0]['id_jadwal'],respon[0]['jenis'],respon[0]['id_lokasi']);
        }else{
          $("[name=txt_jadwal]").val('');
          $("[name=txt_jenisjadwal]").val('');
          $("[name=txt_idlocation]").val('');
          swal("Tidak Ditemukan", "Jadwal untuk jam ini tidak ada", "error");
        }
        
      }

    });
}

function get_area(area='',jadwal='',jenis='',lokasi=''){
    var tanggal = $('[name=tanggal]').val();
    $.ajax({
      url: "{{ url('booking_get_area') }}",
      type: 'post',
      dataType: 'json',
      data: { area:area,tanggal:tanggal},
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(respon){
        if(respon.length > 0){
          $("[name=area]").html('');
          for(i in respon){
            $("[name=area]").append('<option value="'+respon[i].id_area+'" gate="'+respon[i].nama_area+'" foto="'+respon[i].foto+'">'+respon[i].nama_area+' Jumlah Kursi ('+respon[i].jumlah_kursi+')</option>');
          }
          $("[name=txt_jadwal]").val(jadwal);
          $("[name=txt_jenisjadwal]").val(jenis);
          $("[name=txt_idlocation]").val(lokasi);
          var area = $("[name=area]").val();
          getseats(area,jadwal,tanggal);

          $("#foto_area").attr('src','{{url("images_area")}}/'+$("[name=area] :selected").attr('foto'));
        }
        
      }

    });
}

function fill_modal(kursi){
    $("#modal_body").html('');
    $("#modal_footer").html('');
      var html_header = 'Tambah Booking';
      var html = '<div class="form-group">'+
                '<label class="form-control-label">Member</label>'+
                '<select class="form-control select2" name="popup_member" id="popup_member" style="width: 100%" required="">'+
                   // '<option value="">-- Pilih --</option>'+
                   // '<option value="0">Member</option>'+
                   // '<option value="1">Bukan Member</option>'+
                '</select>'+
              '<br>';
      var html_footer = '<button type="button" id="btn_simpan" class="btn btn-primary btn-block" onclick="simpan_kursi()">Simpan</button>'+
                '<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>';
      var html_ = "<h4>Kursi : </h4>"+kursi.replace('_','')+"<br>";

          $.ajax({
                url: "{{ url('booking_member') }}",
                type: 'post',
                dataType: 'json',
                headers : {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(respon){
                $('[name=popup_member]').select2();
                  if(respon.length > 0){
                    $("[name=popup_member]").html('');
                    for(i in respon){
                      $("[name=popup_member]").append('<option value="'+respon[i].id_member+'" kursi="'+kursi+'">'+respon[i].nama_member+' ('+respon[i].nik_member+')</option>');
                    }
                  }
            }
          })
          $("#modal_title").text(html_header);
          $("#modal_body").append(html_+html);
          $("#modal_footer").append(html_footer);
          $("#examplePositionSidebar").modal('show');
}
function simpan_kursi(){
  var id = $("[name=popup_member]").val();
  var kursi = $("[name=popup_member] :selected").attr('kursi');

  $('#member_'+kursi).val(id);
  $("#examplePositionSidebar").modal("hide");
}

$(document).ajaxStart(function(){
        $.LoadingOverlay("show",{
          imageAnimation  : "rotate_right",
          imageColor : "#3c8dbc"
        });
      }).ajaxStop(function(){
         $.LoadingOverlay("hide");
      })

</script>

@endsection