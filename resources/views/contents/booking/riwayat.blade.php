@extends('templates.index')
@section('title', 'Riwayat Pesan')
@section('css')
<style type="text/css">
.contenido_s {
  max-height: 430px;
  max-width: 245px;
  border: 2px dashed #dadde6;
}
.details_s {
  padding-left: 22px;
  padding-top: 5px;
  background:white;
  border-top: 1px dashed #c3c3c3;
}

.hqr_s{
  display: table;
  width: 100%;
  table-layout: fixed;
  margin: 0px auto;
}

</style>
@endsection
@section('content')
<div class="page-header">
    <h1 class="page-title">Pesan</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="javascript:void(0)">Pesan</a></li>
      <li class="breadcrumb-item active"><a href="{{ url('riwayat') }}">Riwayat Pesan</a></li>      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th width="5%" class="text-center"></th>
	                 	<th width="25%" class="text-center">Tanggal Kegiatan</th>
	                 	<th width="25%" class="text-center">Tanggal Pesan</th>
	                 	<th width="10%" class="text-center">Jumlah</th>
	                 	<th width="40%" class="text-center">Kursi</th>
	                 	<th width="15%" class="text-center"></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-sm">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <div class="modal-body" id="modal_body">
            	<input type="hidden" name="table_id" id="table_id">
	            <div class="text-center" style="border: 2px dashed #dadde6;">
				  <div class="ticket">
				    <div class="hqr_s">
				      <div class="text-center">
				        <img id="qr_barcode" height="100px;" width="100px;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=&choe=UTF-8">
				      </div>
				    </div>
				    </div>
				    <div class="details_s">
				    <div class="row table-responsive" style="margin-bottom: 30px;">
				     <table width="100%">
				     	<tr>
				     		<td width="100%" class="text-center" colspan="3"><h2 id="gate"></h2></td>
				     	</tr>
				     	<tr>
				     		<td width="20%" class="text-left"><b>Nama</b></td>
				     		<td width="5%" class="text-center">:</td>
				     		<td width="75%" class="text-left" id="nama_ktp">-</td>
				     	</tr>
				     	<tr>
				     		<td width="20%" class="text-left"><b>Lokasi</b></td>
				     		<td width="5%" class="text-center">:</td>
				     		<td width="75%" class="text-left" id="gereja">-</td>
				     	</tr>
				     	<tr>
				     		<td width="20%" class="text-left"><b>Gate</b></td>
				     		<td width="5%" class="text-center">:</td>
				     		<td width="75%" class="text-left" id="t_gate">-</td>
				     	</tr>
				     	<tr>
				     		<td width="20%" class="text-left"><b>Jadwal</b></td>
				     		<td width="5%" class="text-center">:</td>
				     		<td width="75%" class="text-left" id="jam">-</td>
				     	</tr>
				     </table>
				     <table border="1" width="100%" id="table_ticket">
				     	<thead>
				     		<tr>
					     		<td width="40%">Kursi</td>
					     		<td width="60%">Pengunjung</td>
				     		</tr>
				     	</thead>
				     	<tbody id="kursi">
				     		
				     	</tbody>
				     </table>
				     </div>
				    </div>
				  </div>
            </div>
            <div class="image-priview hide"></div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-default btn-batal-modul" data-dismiss="modal">Batal</button>
            	@if(Agent::isDesktop())
            	<button type="button" class="btn btn-primary btn-download-modul" data-dismiss="modal">Download</button>
            	@endif
            	@if(Agent::isTablet() || Agent::isMobile())
				<button type="button" onclick="showAndroidToast()" class="btn btn-primary" data-dismiss="modal">Print</button>
				@endif
				
            </div>
        </div>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">
	var table;
	var table_ticket;
	$(document).ready(function(){
		// table_ticket = $("#table_ticket").DataTable({
  //           "paging": false,
  //       });

		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('riwayat/get_data') }}',
	        columns: [
	            {data: 'nama_lokasi', name: 'nama_lokasi', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
			  	{data: 'tgl_show_format', name: 'tgl_show_format'},
			  	{data: 'tgl_booking_format', name: 'tgl_booking_format'},
	            {data: 'jumlah', name: 'jumlah'},
	            {data: 'kursi', name: 'kursi'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
	        createdRow: function( row, data, dataIndex ) {
	        	 $( row ).find('td:eq(0),td:eq(1),td:eq(2),td:eq(3)').addClass('text-center');
	        }
		});

	})

	$(".btn-batal-modul").on("click", function(e){
	    $("#modal_title").text("");
	    $("#examplemodal").modal("hide");
	});

	$(".btn-download-modul").on('click',function(e){
		var id = $("[name=table_id]").val();
		window.open("riwayat/download2/"+id,'_blank');
	})   

	function show(data_id){
		$("[name=table_id]").val("");
		$("#no_ktp").text("");
		$("#nama_ktp").text("");
		$("#gereja").text("");
		$("#t_gate").text("");
		$("#jadwal").text("");
		$("#table_ticket tbody").html('');
		$.ajax({
		      url : "{{url('riwayat/get_ticket')}}",
		      type : "post",
		      data : {id:data_id},
		      dataType : "json",
		      headers : {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      },
		      success : function(result){
		      	  $("[name=table_id]").val(data_id);
		          $("#nama_ktp").text(result['data']['nama_ktp']);
		          $("#gereja").text(result['data']['nama_lokasi']);
		          $("#t_gate").text(result['gate']);
		          $("#jam").text(result['waktu']);
		        //   $("#gate").text(result['gate']);
		          for(i in result.detail){
		        		$("#table_ticket tbody").append(result.detail[i]);
		        	}
		       

		      }
		  })
		$('#qr_barcode').attr('src',"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl="+data_id+"&choe=UTF-8");
		$("#modal_title").text("Ticket QRCode");
		$("#examplemodal").modal("show");
	}
</script>
<script type="text/javascript">
		
		function showAndroidToast() {
			var id_tiket = $("#table_id").val();
			$.ajax({
		      url : "{{url('riwayat/historylog')}}",
		      type : "post",
		      data : {id:id_tiket},
		      dataType : "json",
		      headers : {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      },
		      success : function(result){
		      	
		      }

		  	})

			Android.CetakQR(id_tiket);
		}
</script>
@endsection