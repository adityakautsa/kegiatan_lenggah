<!DOCTYPE html>
<html>
<head>
  <title></title>
<style type="text/css">
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400');
.contenido {
  max-height: 430px;
  max-width: 245px;
  border: 2px dashed #dadde6;
}
.details {
  padding: 26px;
  background:white;
  border-top: 1px dashed #c3c3c3;
}
.tinfo {
  font-size: 0.5em;
  font-weight: 300;
  color: #c3c3c3;
  font-family: 'Roboto', sans-serif;
  text-transform: uppercase;
  letter-spacing: 1px;
  margin:7px 0;
}

.tdata {
  font-size: 0.7em;
  font-weight: 400;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.5px;
  margin:7px 0;
}

.name {
  font-size: 1.3em;
  font-weight: 300;
}

.masinfo{
  display:block;
}
.left,.right{
  width:49%;
  display:inline-table;
}

.nesp{
  letter-spacing: 0px;
}
</style>
</head>
<body>
<div class="contenido">
  <div class="ticket">
    <div class="hqr">
        <div id="qrcode"><center><img id="qr_barcode" height="100px;" width="100px;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=&choe=UTF-8"></center></div>
    </div>
    </div>
    <div class="details">
      <div class="tinfo">
        attendee
      </div>
      <div class="tdata name">
        Johnny Harmon
      </div>
      <div class="tinfo">
        ticket
      </div>
      <div class="tdata">
        Monday General Admission
      </div>      
      <div class="tinfo">
        event
      </div>
      <div class="tdata">
        Goal Summit NYC 2016 BetterWorks
      </div>  
      <div class="masinfo">
        <div class="left">
         <div class="tinfo">
        date
      </div>
      <div class="tdata nesp">
        MON. APR 09, 7:00PM
      </div>  
        </div>
        <div class="right">
        <div class="tinfo">
        location
      </div>
      <div class="tdata nesp">
        Octavia Street. San Francisco, CA
      </div> 
        </div>
      </div>
      
      <div class="link">
        <a href="#">SEE MORE</a>
      </div>
    </div>
  </div>
</div>

</body>
</html>