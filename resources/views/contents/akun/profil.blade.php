@extends('templates.index')
@section('title', 'Profil')

@section('content')

<style type="text/css">
	.left{
		text-align: left;
	}
</style>


<div class="page-content container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<div class="card card-shadow text-center">
				<div class="card-block">

	                @php
	                $nama = isset($data['member']->nama) ? $data['member']->nama:"";
	                $nama_group = isset($data['member']->nama_group) ? $data['member']->nama_group:"";
	                $image = '';
	                if(isset($data['member']->foto)){
	                	if($data['member']->foto != ''){
	                		$image = asset('images/member')."/".$data['member']->foto;	
	                	}else{
	                		$image = asset('images/no-image.png');
	                	}
	                }
	                

	                $isi_arr[] = isset($data['member']->nik) ? $data['member']->nik:"";
	                $isi_arr[] = $nama;
	                $isi_arr[] = $data['member']->tempat_lahir;
	                $isi_arr[] = ($data['member']->tgl_lahir == "") ? "":tgl_full($data['member']->tgl_lahir, 98);
	                $isi_arr[] = ($data['member']->jenis_kelamin == '0') ? "Pria":"Wanita";
	                $isi_arr[] = ($data['member']->nama_prov == "") ? "":$data['member']->nama_prov;
	                $isi_arr[] = ($data['member']->nama_kab == "") ? "":$data['member']->nama_kab;
	                $isi_arr[] = ($data['member']->nama_kec == "") ? "":$data['member']->nama_kec;
	                $isi_arr[] = ($data['member']->nama_kel == "") ? "":$data['member']->nama_kel;
	                $isi_arr[] = ($data['member']->alamat == "") ? "":$data['member']->alamat;
	                $isi_arr[] = ($data['member']->no_telp == "") ? "":$data['member']->no_telp;
	                $isi_arr[] = ($data['member']->tgl_registrasi == "0000-00-00" || $data['member']->tgl_registrasi == null) ? "":tgl_full($data['member']->tgl_registrasi, 98);
	                $isi_arr[] = ($data['member']->tgl_aktivasi == "0000-00-00" || $data['member']->tgl_aktivasi == null) ? "":tgl_full($data['member']->tgl_aktivasi, 98);
					$text_arr = ["NIK", "Nama", "Tempat Lahir", "Tanggal Lahir", "Jenis Kelamin", "Provinsi", "Kab/Kota", "Kecamatan", "Kelurahan", "Alamat", "Telp", "Tgl Registrasi", "Tgl Aktivasi", "Gereja", "Wilayah", "Lingkungan"];
					@endphp
						                <!-- $isi_arr[] = ($data['member']->nama_lokasi == "") ? "":$data['member']->nama_lokasi;
	                $isi_arr[] = ($data['member']->nama_wilayah == "") ? "":$data['member']->nama_wilayah;
	                $isi_arr[] = ($data['member']->nama_lingkungan == "") ? "":$data['member']->nama_lingkungan; -->

					<!-- $text_arr = ["NIK", "Nama", "Tempat Lahir", "Tanggal Lahir", "Jenis Kelamin", "Provinsi", "Kab/Kota", "Kecamatan", "Kelurahan", "Alamat", "Telp", "Tgl Registrasi", "Tgl Aktivasi", "Gereja", "Wilayah", "Lingkungan"]; -->
					<a class="avatar avatar-lg" href="javascript:void(0)">
	                  	<img src="{{ $image }}" alt="...">
	                </a>
	                <h4 class="profile-user">{{ $nama }}</h4>
	                <p class="profile-job">{{ $nama_group }}</p>
	            	<p>
	                	<div class='btn-group' role='group'>
	                		<a type="button" href="{{ url('profil/edit') }}" class="btn btn-warning btn-icon"><i class='fa fa-pencil-square-o'></i> Ubah Profil</a>
	                	</div>
	            	</p>
				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="card card-shadow text-center">
				<div class="card-block">
					<h5 class="card-title">
                  		<span>Profil</span>
                	</h5>
                	<div class="table-responsive">
                		<table class="table table-striped">
                			@foreach($isi_arr as $r => $r1)
                			<tr>
                				<td width="30%" class="left">{{ isset($text_arr[$r]) ? $text_arr[$r]:"" }}</td>
                				<td width="5%">:</td>
                				<td class="left">{{ $r1 }}</td>
                			</tr>
                			@endforeach
                		</table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		
	})
</script>
@endsection