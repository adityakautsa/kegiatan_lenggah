@extends('templates.index')
@section('title', 'Rubah Profil')

@section('content')

<div class="page-content container-fluid">
	<div class="panel">
		<div class="panel-head">
			<h5 class="panel-title">Edit Profil</h5>
		</div>
		<div class="panel-body">
			<form id="form_input" autocomplete="off" enctype="multipart/form-data" method="POST" action="{{ url("profil/simpan") }}">
				{{ csrf_field() }}
				<div class="row">
					<input type="hidden" name="id" value="{{ isset($data['member']->id) ? $data['member']->id:"" }}">
					<div class="col-lg-6">
						<div class="form-group row">
							<label class="col-form-label col-lg-3">NIK</label>
							<div class="col-lg-9">
			            		<input type="number" name="nik" class="form-control" value="{{ isset($data['member']->nik) ? $data['member']->nik:"" }}" required>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Nama</label>
							<div class="col-lg-9">
			            		<input type="text" name="nama" class="form-control" value="{{ isset($data['member']->nama) ? $data['member']->nama:"" }}" required>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Tempat Lahir</label>
							<div class="col-lg-9">
			            		<input type="text" name="tmp_lahir" class="form-control" value="{{ isset($data['member']->tempat_lahir) ? $data['member']->tempat_lahir:"" }}" required>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Tgl Lahir</label>
							<div class="col-lg-9">								
								@php 
								$tgl = isset($data['member']->tgl_lahir) ? $data['member']->tgl_lahir:date('d-m-Y');
								if($tgl != ''){
									$tgl = tgl_full($tgl, 98);
								}else{
									$tgl = date("d-m-Y");
								}
								@endphp
			            		<input type="text" name="tgl_lahir" class="form-control date_fromtoday" value="{{ $tgl }}" readonly required>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Jenis Kelamin</label>
							<div class="col-lg-9">
								@php
								$jk = isset($data['member']->jenis_kelamin) ? $data['member']->jenis_kelamin:"0";
								@endphp
			            		<select name="jk" class="form-control" required>
			            			<option value="0" {{ ($jk == 0) ? "selected":"" }}>Pria</option>
			            			<option value="1" {{ ($jk == 1) ? "selected":"" }}>Wanita</option>
			            		</select>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Provinsi</label>
							<div class="col-lg-9">
			            		<select name="provinsi" class="form-control" required>
			            			@foreach($data['prov'] as $p)
			            			<option value="{{ $p->id }}">{{ $p->name }}</option>
			            			@endforeach
			            		</select>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Kabupaten</label>
							<div class="col-lg-9">
			            		<select name="kabupaten" class="form-control" required>
			            			
			            		</select>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Kecamatan</label>
							<div class="col-lg-9">
			            		<select name="kecamatan" class="form-control" required>
			            			
			            		</select>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Kelurahan</label>
							<div class="col-lg-9">
			            		<select name="kelurahan" class="form-control" required>
			            			
			            		</select>
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Alamat</label>
							<div class="col-lg-9">
			            		<textarea name="alamat" class="form-control" rows="3">{{ isset($data['member']->alamat) ? $data['member']->alamat:"" }}</textarea>
			            	</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group row">
							<label class="col-form-label col-lg-3">No. Telepon</label>
							<div class="col-lg-9">
			            		<input type="number" name="telepon" class="form-control" value="{{ isset($data['member']->no_telp) ? $data['member']->no_telp:"" }}" required readonly>
			            	</div>
						</div>
						@php
							$last_foto = "";
							$image = '';
				            if(isset($data['member']->foto)){
				              	if($data['member']->foto != ''){
				               		$image = asset('images/member')."/".$data['member']->foto;	
				               	}else{
				               		$image = asset('images/no-image.png');
				               	}
				            }
						@endphp
						@php
						// if (file_exists($image)){
						@endphp
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Foto Sebelum</label>
							<div class="col-lg-9">
			            		<img src="{{ $image }}" class="w-120">
			            	</div>
						</div>
						@php
						// }
						@endphp
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Foto</label>
							<div class="col-lg-9">
			            		<input type="file" accept="image/*" id="foto" name="foto" class="form-control">
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3">Paroki</label>
							<div class="col-lg-9">
								@php 
								$id_paroki = isset($data['member']->id_lokasi) ? $data['member']->id_lokasi:"";
								$sel = "";
								if(!is_numeric($id_paroki)){
									$id_paroki = 9999;
								}
								@endphp
			            		<select class="form-control" name="paroki">
			            			<option value=""> -- Pilih --</option>
				                    @foreach($data['lokasi'] as $w)
				                    <option value="{{ $w->id }}" {{ ($id_paroki == $w->id) ? "selected":""  }}>{{ $w->nama }}</option>
				                    @endforeach
				                    <option value="9999" {{ ($id_paroki == '9999') ? "selected":""  }}> Lainnya </option>
			            		</select>
			            	</div>
						</div>
						<div class="form-group row" id="div_parokitext">
							<label class="col-form-label col-lg-3">Nama Paroki</label>
							<div class="col-lg-9">
			            		<input type="text" name="gereja" class="form-control" value="{{ isset($data['member']->nama_lokasi) ? $data['member']->nama_lokasi:"" }}">
			            	</div>
						</div>
						<!-- <div class="form-group row" id="div_wil">
							<label class="col-form-label col-lg-3">Wilayah</label>
							<div class="col-lg-9">
			            		<select class="form-control" name="wilayah">
			            			
			            		</select>
			            	</div>
						</div>
						<div class="form-group row" id="div_ling">
							<label class="col-form-label col-lg-3">Lingkungan</label>
							<div class="col-lg-9">
			            		<select class="form-control" name="lingkungan">
			            			
			            		</select>
			            	</div>
						</div> -->
						<div class="pull-right">
							<button type="submit" id="btn_simpan" class="btn btn-primary">Simpan</button>
						</div>
					</div>
			</form>		
		</div>
	</div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/custom_upload.js') }}"></script>
<script type="text/javascript">
	var table;
	var id_prop = '{{ isset($data['member']->provinsi) ? $data['member']->provinsi:"35" }}', 
	id_kab = '{{ isset($data['member']->kabupaten) ? $data['member']->kabupaten:"" }}', id_kec = '{{ isset($data['member']->kecamatan) ? $data['member']->kecamatan:"" }}', id_kel = '{{ isset($data['member']->desa) ? $data['member']->desa:"" }}', id_data = '',
	id_gereja = '{{ isset($data['member']->id_lokasi) ? $data['member']->id_lokasi:"" }}',
	id_wil = '{{ isset($data['member']->id_wilayah) ? $data['member']->id_wilayah:"" }}',
	id_ling = '{{ isset($data['member']->id_lingkungan) ? $data['member']->id_lingkungan:"" }}';
	$(document).ready(function(){
		$("[name=jk], [name=provinsi], [name=kabupaten], [name=kecamatan], [name=kelurahan], [name=wilayah], [name=lingkungan], [name=paroki]").select2();

		if(id_kab != ''){
			$("[name=provinsi]").val(id_prop).trigger("change");
		}

		if(id_gereja != ''){
			// get_wilayah('1', id_gereja);
			if($.isNumeric(id_gereja) == false){
				$("[name=paroki]").val("9999").trigger("change");
			}else{
				$("[name=paroki]").val(id_gereja).trigger("change");
			}
		}

		convert_image("foto", "form_input");
	})

	$("#btn_tambah").click(function(){
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
	})

	function clear_input(){
		id = ''; id_kab = ''; id_kec = ''; id_kel = ''; id_data = '';
		$("[name=popup_id]").val('');
		$("[name=popup_nama]").val('');
		$("[name=popup_nik]").val('');
		$("[name=popup_prov]").val('').trigger("change");
		$("[name=popup_alamat]").val('');
		$("[name=popup_telepon]").val('');
		$("[name=popup_aktif]").val('1').trigger("change");
		$("[name=popup_lokasi]").val('').trigger("change");
	}

	$("[name=provinsi]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('member/get_kabupaten') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=kabupaten]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=kabupaten]").val(id_kab).trigger("change");
		      	}
			})
		}else{
			trigger("[name=kabupaten]");
		}
	})

	$("[name=kabupaten]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('member/get_kecamatan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=kecamatan]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=kecamatan]").val(id_kec).trigger("change");
		      	}
			})
		}else{
			trigger("[name=kecamatan]");
		}
	})

	$("[name=kecamatan]").change(function(){
		var id = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('member/get_kelurahan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id },
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=kelurahan]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=kelurahan]").val(id_kel).trigger("change");
		      	}
			})
		}else{
			trigger("[name=kelurahan]");
		}
	})

	function get_wilayah(id = '', parent = ''){
		if(id != ''){
			$.ajax({
				url : "{{ url('profil/get_lingkungan') }}",
				type : "POST",
				dataType : "html",
				data : { id : 1, parent : parent},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=wilayah]").html(respon);
		      	},
		      	complete : function(respon){
		      		$("[name=wilayah]").val(id_wil).trigger("change");
		      	}
			})
		}else{
			trigger("[name=wilayah]");
		}
	}

	$("[name=paroki]").change(function(){
		var id = '1';
		var parent = $(this).val();
		if(parent != '' && parent < 9000){
			$("#div_parokitext").hide();
			$("[name=gereja]").val(id_gereja);
			$("#div_wil").show();
			$("#div_ling").show();
			$("[name=wilayah]").attr("required", "true");
			$("[name=lingkungan]").attr("required", "true");
			$.ajax({
				url : "{{ url('profil/get_lingkungan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id, parent : parent},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=wilayah]").html(respon);
		      	},
		      	complete : function(respon){
		      		if(id_wil != '0'){
		      			$("[name=wilayah]").val(id_wil).trigger("change");
		      		}else{
		      			$("[name=wilayah]").val('').trigger("change");
		      		}
		      	}
			})
		}else if(parent == 9999){
			$("[name=gereja]").val(id_gereja);
			$("#div_parokitext").show();
			$("#div_wil").hide();
			$("#div_ling").hide();
			trigger("[name=wilayah]");
			trigger("[name=lingkungan]");
			$("[name=wilayah]").removeAttr("required");
			$("[name=lingkungan]").removeAttr("required");
		}
		else{
			$("[name=gereja]").val(id_gereja);
			$("#div_parokitext").hide();
			$("#div_wil").show();
			$("#div_ling").show();
			trigger("[name=wilayah]");
			$("[name=wilayah]").attr("required", "true");
			$("[name=lingkungan]").attr("required", "true");
		}
	})

	$("[name=wilayah]").change(function(){
		var id = '2';
		var parent = $(this).val();
		if(id != ''){
			$.ajax({
				url : "{{ url('profil/get_lingkungan') }}",
				type : "POST",
				dataType : "html",
				data : { id : id, parent : parent},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("[name=lingkungan]").html(respon);
		      	},
		      	complete : function(respon){
		      		
		      		if(id_ling != '0'){
		      			$("[name=lingkungan]").val(id_ling).trigger("change");
		      		}else{
		      			$("[name=lingkungan]").val('').trigger("change");
		      		}
		      	}
			})
		}else{
			trigger("[name=lingkungan]");
		}
	})


	function trigger(value){
		$(value).html("<option value=''> -- Pilih -- </option>");
		$(value).val("").trigger("change");
	}

	function paroki_change(){

	}
</script>
@endsection