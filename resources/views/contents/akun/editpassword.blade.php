@extends('templates.index')
@section('title', 'Rubah Password')

@section('content')



<div class="page-content container-fluid">

@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
        	<strong>{{ $message }}</strong>
	</div>
@endif
@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
        	<strong>{{ $message }}</strong>
	</div>
@endif
	<div class="panel">
		<div class="panel-head">
			<h5 class="panel-title">Edit Password</h5>
		</div>
		<div class="panel-body">
			<form id="form_input" autocomplete="off" enctype="multipart/form-data" method="POST" action="{{ url("rubah_password/simpan") }}">
				{{ csrf_field() }}
				
					<input type="hidden" name="id" value="">
						<div class="form-group row">
							<label class="col-form-label col-lg-4">Password Lama</label>
							<div class="col-lg-8">
			            		<input type="password" name="password_lama" class="form-control" value="">
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-4">Password Baru</label>
							<div class="col-lg-8">
			            		<input type="password" name="password" class="form-control" data-fv-notempty="true" data-fv-notempty-message="Wajib diisi" data-fv-identical="true" data-fv-identical-field="password_"
                                data-fv-identical-message="Password baru tidak sama">
			            	</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-4">Ulangi Password</label>
							<div class="col-lg-8">
			            		<input type="password" name="password_" class="form-control" data-fv-notempty="true" data-fv-notempty-message="Wajib diisi" data-fv-identical="true" data-fv-identical-field="password"
                                data-fv-identical-message="Password baru tidak sama">
			            	</div>
						</div>
						<div class="pull-right">
							<button type="submit" id="btn_simpan" class="btn btn-primary">Simpan</button>
						</div>
			</form>		
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
	
	
	})

	$('#form_input').formValidation({
    	framework: "bootstrap4",
    	button: {
        	selector: '#btn_simpan',
        	disabled: 'disabled'
    	},
      	icon: null,
		fields: {
			password_lama : {
				validators: {
		            notEmpty: {
		              message: 'Wajib diisi'
		            }
		        }
			},password : {
				validators: {
		            notEmpty: {
		              message: 'Wajib diisi'
		            },
		            stringLength: {
                        min: 5,
                        message: 'Minimal 5 karakter',
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Gunakan huruf alfabet, angka dan garis bawah',
                    },
		        }
			},
			password_: {
                validators: {
                	notEmpty: {
		              message: 'Wajib diisi'
		            },
		            stringLength: {
                        min: 5,
                        message: 'Minimal 5 karakter',
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Gunakan huruf alfabet, angka dan garis bawah',
                    },
                }
            },
		},
		err: {
        	clazz: 'invalid-feedback'
      	},
      	control: {
        	valid: 'is-valid',
        	invalid: 'is-invalid'
      	},
      	row: {
        	invalid: 'has-danger'
      	}
	})
</script>
@endsection