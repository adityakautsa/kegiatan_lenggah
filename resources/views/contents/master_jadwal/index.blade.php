@extends('templates.index')
@section('title', 'Master Jadwal')
@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/select2/select2.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css') }}">

<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/timepicker/jquery-timepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
<style type="text/css">
	.bootstrap-timepicker-widget.dropdown-menu {
    z-index: 99999!important;
}
</style>
@endsection
@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Jadwal</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i> Tambah</button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Lokasi</th>
	                 	<th>Nama Jadwal</th>
	                 	<th>Jenis Jadwal</th>
	                 	<th>Tanggal</th>
	                 	<th>Hari</th>
	                 	<th>Jam</th>
	                 	<th>Aktif</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">            	
	            <div class="form-group">
	            	<label class="form-control-label">Nama</label>
	            	<input type="text" class="form-control" name="popup_nama">
	            </div>	
            	<div class="form-group" id="div_gereja">
	            	<label class="form-control-label">Gereja</label>
	            	<select class="form-control select2" name="popup_lokasi" readonly="">
	            		@foreach($data['lokasi'] as $d)
	            		<option value="{{$d->id}}">{{$d->nama}}</option>
	            		@endforeach
	            	</select>
	            </div>
	            <div class="form-group">
					<label class="form-control-label">Area</label>
					<br>			
					<div class="col-md-10">
						@foreach($data['area'] as $d)													
							<input type="checkbox" name="popup_area[]" onclick="checkbox($(this))"
							value="{{$d->id}}" class="checkbox{{$d->id}}" style="margin: 4px;">
							<label for="popup_area">{{$d->nama}}</label>
						@endforeach			
					</div>		            						
	            </div>
	            <div class="form-group">
	            	<label class="form-control-label">Jenis Jadwal</label>
	            	<select class="form-control" name="popup_jenis">	            		
	            		<option value="1">Rutin</option>
	            		<option value="0">Khusus</option>
	            	</select>
	            </div>
            	<div class="form-group" id="div-tanggal">
	            	<label class="form-control-label">Tanggal</label>
	            	<input type="text" name="popup_tanggal" class="form-control datepicker" readonly>
	            </div>
	            <div class="form-group">
	            	<label class="form-control-label">Hari</label>
	            	<select class="form-control" name="popup_hari">
	            		<option value=""> -- Pilih -- </option>
	            		<option value="0">Minggu</option>
	            		<option value="1">Senin</option>
	            		<option value="2">Selasa</option>
	            		<option value="3">Rabu</option>
	            		<option value="4">Kamis</option>
	            		<option value="5">Jum'at</option>
	            		<option value="6">Sabtu</option>
	            	</select>
	            </div>
	            <div class="form-group">
	            	<label class="form-control-label">Jam</label>
	            	<select class="form-control select2" name="popup_jam">
	            		<option value="01:00">01:00</option>
	            		<option value="01:30">01:30</option>
	            		<option value="02:00">02:00</option>
	            		<option value="02:30">02:30</option>
	            		<option value="03:00">03:00</option>
	            		<option value="03:30">03:30</option>
	            		<option value="04:00">04:00</option>
	            		<option value="04:30">04:30</option>
	            		<option value="05:00">05:00</option>
	            		<option value="05:30">05:30</option>
	            		<option value="06:00">06:00</option>
	            		<option value="06:30">06:30</option>
	            		<option value="07:00">07:00</option>
	            		<option value="07:30">07:30</option>
	            		<option value="08:00">08:00</option>
	            		<option value="08:30">08:30</option>
	            		<option value="09:00">09:00</option>
	            		<option value="09:30">09:30</option>
	            		<option value="10:00">10:00</option>
	            		<option value="10:30">10:30</option>
	            		<option value="11:00">11:00</option>
	            		<option value="11:30">11:30</option>
	            		<option value="12:00">12:00</option>
	            		<option value="12:30">12:30</option>
	            		<option value="13:00">13:00</option>
	            		<option value="13:30">13:30</option>
	            		<option value="14:00">14:00</option>
	            		<option value="14:30">14:30</option>
	            		<option value="15:00">15:00</option>
	            		<option value="15:30">15:30</option>
	            		<option value="16:00">16:00</option>
	            		<option value="16:30">16:30</option>
	            		<option value="17:00">17:00</option>
	            		<option value="17:30">17:30</option>
	            		<option value="18:00">18:00</option>
	            		<option value="18:30">18:30</option>
	            		<option value="19:00">19:00</option>
	            		<option value="19:30">19:30</option>
	            		<option value="20:00">20:00</option>
	            		<option value="20:30">20:30</option>
	            		<option value="21:00">21:00</option>
	            		<option value="21:30">21:30</option>
	            		<option value="22:00">22:00</option>
	            		<option value="22:30">22:30</option>
	            		<option value="23:00">23:00</option>
	            		<option value="23:30">23:30</option>
	            		<option value="24:00">24:00</option>
	            		<option value="24:30">24:30</option>
	            	</select>
	            </div>
	            <div class="form-group">
	            	<label class="form-control-label">Keterangan</label>
	            	<textarea class="form-control" name="popup_keterangan"></textarea>
	            </div>	            
	            <div class="form-group">
	            	<label class="form-control-label">Aktif</label>
	            	<select class="form-control" name="popup_aktif">
	            		<option value="1">Aktif</option>
	            		<option value="0">Tidak Aktif</option>
	            	</select>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-default btn-batal-modul">Batal</button>
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="{{ asset('themeforest/global/vendor/select2/select2.full.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>

<script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script type="text/javascript">
	var table;
	var id_lokasi = <?php echo $data['auth']['id_lokasi']?>;	
	$(document).ready(function(){		
		var start = new Date();
		$('.select2').select2();

	    $('.timepicker').timepicker({ template: 'modal' , 'timeFormat': 'H:i' });

		$('.datepicker').datepicker({
	      format : 'dd-mm-yyyy',
	      setDate: new Date(),
	      startDate: new Date(),
	      autoclose: true,
	      clearBtn: true,
	    });

	    $(".datepicker").on("changeDate", function (e) {
	      	var hari = e.date.getDay();
	      	$("[name=popup_hari]").val(hari).trigger('change');
	    });

	    $('[name=popup_lokasi]').val(id_lokasi).trigger('change');
	    $('#div-tanggal').hide();
	    $('[name=popup_jenis]').on("change",function(){
	    	var id = $(this).val();
	    	if(id==0){
	    		$('#div-tanggal').show();
	    	}else{
	    		$('#div-tanggal').hide();
	    	}
	    })

		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: "{{ url('jadwal_show') }}",
	        columns: [
	            {data: 'id', name: 'id', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
			  	{data: 'nama_lokasi', name: 'nama_lokasi'},
	            {data: 'nama', name: 'nama'},
	            {data: 'jenis', name: 'jenis'},
	            {data: 'tanggal', name: 'tanggal'},
	            {data: 'hari', name: 'hari'},
	            {data: 'jam', name: 'jam'},
	            {data: 'aktif', name: 'aktif'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});

		$("#div_gereja").hide();
	})

	$("#btn_tambah").click(function(){			
		clear_input();					
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
		$('.timepicker').timepicker({ template: '#examplemodal' , 'timeFormat': 'H:i' });
	})

	$(".btn-batal-modul").on("click", function(e){
	      $("#modal_title").text("Tambah");
	      $("#examplemodal").modal('hide');
	})
	
	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var ruang = $("[name=popup_ruangan]").val();
		var jenis = $("[name=popup_jenis]").val();
		var tanggal = $("[name=popup_tanggal]").val();
		var hari = $("[name=popup_hari]").val();
		var jam = $("[name=popup_jam]").val();
		
		if(jenis != '' && jam != '' && (tanggal != '' || hari != '') ){
			$.ajax({
				url : "{{ url('jadwal_simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){					  
					  table.ajax.reload();					  
		      		$("#examplemodal").modal("hide");
		      	}
			})
		}
	})

	var oldcheckbox = "";
	// function checkbox(){
	// 	oldcheckbox = "";
	// 	var cb = document.getElementsByName("popup_area[]");
	// 	var cbid = document.getElementById("popup_area");					
	// 	for(var i = 0; i < cb.length; i++){
 //            if(cb[i].checked){                
	// 			if (oldcheckbox=="") {
	// 				oldcheckbox = cb[i].value;
	// 			}else{					
	// 				oldcheckbox = oldcheckbox+" "+cb[i].value;
	// 			}				
 //            }
 //        }	
	// }

	function checkbox(this_){
		if($(this_).prop("checked") == true){
            console.log("Checkbox is checked.");
        }
        else if($(this_).prop("checked") == false){
            console.log("Checkbox is unchecked.");
        }
	}

	function edit(data_id){
		var id_jadwal = $("#table_id"+data_id).val();
		var nama = $("#table_nama"+data_id).val();
		var idlokasi = $("#table_idlokasi"+data_id).val();
		var idarea = $("#table_idarea"+data_id).val().split(',');				
		clear_checkbox();		
		for (var index = 0; index < idarea.length; index++) {			
			oldcheckbox = idarea;									
			$(".checkbox"+(idarea[index])).prop("checked",true);
			
		}				

		var jenis = $("#table_jenis"+data_id).val();
		var tanggal = $("#table_tanggal"+data_id).val();
		var jam = $("#table_jam"+data_id).val();
		var hari = $("#table_hari"+data_id).val();
		var aktif = $("#table_aktif"+data_id).val();
		var ket = $("#table_keterangan"+data_id).val();
		
		$("[name=popup_id]").val(data_id);
		$("[name=popup_nama]").val(nama);
		$("[name=popup_lokasi]").val(idlokasi).trigger('change');		
		// $("#popup_area").val(idarea).trigger('click');;
		$("[name=popup_jenis]").val(jenis).trigger('change');
		$("[name=popup_tanggal]").val(tanggal);
		$("[name=popup_hari]").val(hari).trigger('change');
		$("[name=popup_jam]").val(jam).trigger('change');
		$("[name=popup_aktif]").val(aktif);
		$("[name=popup_keterangan]").val(ket);

		$("#examplemodal").modal("show");
	}

	function hapus(id){
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('jadwal_hapus')}} ",
	          	type: 'post',
	          	data: {id : id},
			       headers : {
			       	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			       },
	          	success: function(respon){ 
	          		table.ajax.reload();
	          		// swal("Deleted!", "Your imaginary file has been deleted!", "success");
	         	}
        	})
    	})
	}

	function clear_checkbox(){
		$('input:checkbox').each(function() { 
			// this.checked = false; 
			if(this.checked){
				this.checked = false;
			}
		});		
	}

	function clear_input(){		
		clear_checkbox();
		$("[name=popup_id]").val('');
		$("[name=popup_nama]").val('');				
		$("[name=popup_jenis]").val('1').trigger('change');
		$("[name=popup_tanggal]").val('');
		$("[name=popup_hari").val('').trigger('change');
		$("[name=popup_jam]").val('').trigger('change');
		$("[name=popup_aktif]").val('1').trigger('change');
		$("[name=popup_keterangan]").val('');
	}

	function get_lingkungan(this_){
		var id = this_.val();
		if(id!=""){
		    $.ajax({
		      url : "{{url('jadwal_lingkungan')}}",
		      type : "post",
		      data : {id:id},
		      dataType : "json",
		      headers : {
			       	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			       },
		      success : function(result){
		        $("[name=popup_lingkungan]").html("");
		        var opt = new Option("--- Pilih Lingkungan ---", 0);
		        $("[name=popup_lingkungan]").append(opt);

		        if(result.length > 0){
		          $.each(result, function(id, item){
		            var opt = new Option(item.nama, item.id);
		            $("[name=popup_lingkungan]").append(opt);
		          });
		        }

		      },
		      complete : function(result){
		        var id_temp = $("[name=popup_lingkungan_temp]").val();
		        if(id_temp==""){
		          id_temp = 0;
		        }
		        $("[name=popup_lingkungan]").val(id_temp).trigger("change");
		        
		      }
		    })
	      
	    }
	}

	// function get_data_ruang(this_){
	//     var id = this_.val();
	//     if(id!=""){
	//     $.ajax({
	//       url : "{{url('jadwal_ruang')}}/"+id,
	//       type : "get",
	//       dataType : "json",
	//       success : function(result){

	//         $("[name=popup_ruangan]").html("");
	//         var opt = new Option("--- Pilih Ruang ---", 0);
	//         $("[name=popup_ruangan]").append(opt);

	//         if(result.length > 0){
	//           $.each(result, function(id, item){
	//             var opt = new Option(item.nama.toUpperCase(), item.id);
	//             $("[name=popup_ruangan]").append(opt);
	//           });
	//             // formData.find("[name=id_kab]").select2();
	//         }

	//       },
	//       complete : function(result){
	//         var id_temp = $("[name=popup_ruangan_temp]").val();
	//         if(id_temp==""){
	//           id_temp = 0;
	//         }
	//         $("[name=popup_ruangan]").val(id_temp).trigger("change");
	        
	//       }
	//     })
	      
	//     }
	//   }
</script>
@endsection