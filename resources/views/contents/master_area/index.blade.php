@extends('templates.index')
@section('title', 'Master Area')

@section('content')
<div class="page-header">
    <h1 class="page-title">Master</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('area') }}">Area</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i></button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama Lokasi</th>
	                 	<th>Pintu</th>
	                 	<th>Area</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal" autocomplete="off">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Lokasi</label>
	            	<div class="col-lg-9">
		            	<input type="text" name="gereja_text" class="form-control" value="{{ isset($data['lokasi']->nama) ? $data['lokasi']->nama:"" }}" readonly>
		            	<input type="hidden" name="popup_gereja" value="{{ isset($data['lokasi']->id) ? $data['lokasi']->id:"" }}">
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Pintu</label>
	            	<div class="col-lg-9">
	            		<select name="popup_pintu" id="popup_pintu" class="form-control" style="width: 100%">
		            		<option value=""> -- Pilih -- </option>
		            		@foreach($data['pintu'] as $p)
		            		<option value="{{ $p->id_gate }}">{{ $p->nama }}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Nama Area</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_area" class="form-control">
	            	</div>
	            </div>
	            <div class="form-group row" id="div_foto">
	            	<label class="col-form-label col-lg-3">Foto Sebelumnya</label>
	            	<div class="col-lg-9">
	            		<div class="example">
	            			<a class="inline-block" href="" data-plugin="magnificPopup" data-main-class="mfp-img-mobile">
                      		<img class="img-fluid" src="" alt="..." width="220"/>
                    		</a>
	                    </div>
	            	</div>
	            </div>
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-3">Gambar Area</label>
	            	<div class="col-lg-9">
	            		<input type="file" accept="image/*" id="foto" name="foto" class="form-control" data-fv-file="true">
	            		<p class="help-block">Disarankan menggunakan gambar dengan ukuran maksimal 600 x 400 piksel</p>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/custom_upload.js') }}"></script>
<script type="text/javascript">
	var table;
	var id_ruang = '';
	$(document).ready(function(){
		
		$("[name=popup_ruang], [name=popup_aktif], [name=popup_pintu]").select2({
			dropdownParent: $("#examplemodal")
		});

		convert_image("foto", "form_modal");

		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('area/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama_lokasi', name: 'nama_lokasi'},
	            {data: 'nama_gate', name: 'nama_gate'},
	            {data: 'nama', name: 'nama'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_tambah").click(function(){
		id_ruang = '';
		clear_input();
		$("#modal_title").text("Tambah");
		$("#examplemodal").modal('show');
		$("#div_foto").hide();
	})

	function clear_input(){
		$("[name=popup_id]").val('');
		$("[name=popup_pintu]").val('').trigger("change");
		$("[name=popup_area]").val('');
		$("[name=foto]").val('');
		$("[name=img_foto]").remove();
	}


	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var lokasi = $("[name=popup_gereja]").val();
		var pintu = $("[name=popup_pintu]").val();
		var area = $("[name=popup_area]").val();

		if(lokasi != ''  && area != '' && pintu != ''){
			$.ajax({
				url : "{{ url('area/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal('hide');
		      	}
			})
		}else{

		}

	})

	function edit(this_){
		var id = $(this_).data("id");
		$.ajax({
			url : "{{ url('area/get_edit') }}",
			type : "POST",
			dataType : "json",
			data : { id : id },
			headers : {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
		    success : function(respon){
		    	if(respon.length > 0){
		    		show_edit(respon);
		    	}
		    }
		})
	}

	function show_edit(respon){
		clear_input();
		
		var id = respon[0].id;
		var lokasi = respon[0].id_lokasi;
		var nama_lokasi = respon[0].nama_lokasi;
		var id_pintu = respon[0].id_gate;
		var nama = respon[0].nama;
		var foto = respon[0].file_foto;

		if(foto != ''){
			$(".inline-block").attr("href", foto);
			$(".img-fluid").attr("src", foto);
			$("#div_foto").show();
		}else{
			$("#div_foto").hide();
		}
		
		$("[name=popup_id]").val(id);
		$("[name=popup_gereja]").val(lokasi);
		$("[name=gereja_text]").val(nama_lokasi);
		$("[name=popup_pintu]").val(id_pintu).trigger("change");
		$("[name=popup_area]").val(nama);

		$("#modal_title").text("Edit");
		$("#examplemodal").modal('show');
	}

	function hapus(this_){
		var id = $(this_).data("id");
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ini",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('area/hapus')}} ",
	          	type: 'post',
	          	dataType : 'json',
	          	data: {id : id},
			    headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	          	success: function(respon){
	          		if(respon.status == 1){
	          			swal("", respon.keterangan, "success");
	          		}else{
	          			swal("", respon.keterangan, "error");
	          		}
	          		table.ajax.reload();
	         	}
        	})
    	})
	}


	function trigger(value){
		$(value).html("<option value=''> -- Pilih -- </option>");
		$(value).val("").trigger("change");
	}
</script>
@endsection