@extends('templates.index')
@section('title', 'Range Tanggal Pemesanan')

@section('content')
<div class="page-header">
    <h1 class="page-title">Range Tanggal Pemesanan</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Master</a></li>
      <li class="breadcrumb-item active"><a href="#">Range Tanggal Pemesanan</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<div class="panel-heading">
	        <h4 class="panel-title">
	          <strong>Range Tanggal Pemesanan</strong>
	        </h4>
	    </div>
		<div class="panel-body">
			<form action="{{url('rangetanggal/simpan')}}" autocomplete="off" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id">
				<div class="form-group row">
					<label class="col-form-label col-md-3 col-lg-3">Range (/hari)</label>
	            	<div class="col-md-9 col-lg-9">
	            		<input type="number" name="range" class="form-control">
	            	</div>
				</div>
				<div class="footer">
					<button class="float-right btn btn-primary"><i class="icon fa-save"></i> Simpan</button>
				</div>
			</form>			
		</div>
	</div>
</div>


@endsection
@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		get_data();
	})
	function get_data(){
		$.ajax({
	          	url: "{{ url('rangetanggal/get_data')}} ",
	          	type: 'get',
	          	dataType : 'json',
	          	success: function(respon){
	          		$('[name=id]').val(respon.id);
	          		$('[name=range]').val(respon.range_tanggal);
	          	}
	    });
	}
</script>
@endsection