@extends('templates.index')
@section('title', 'Lihat Ruangan')

@section('css')
<style type="text/css">
	.modal-open .select2-container {
    z-index: 0;
	}
</style>
@endsection


@section('content')
<link rel="stylesheet" href="{{ asset('css/custom_seat.css') }}">
<div class="page-header">
    <h1 class="page-title">Jadwal Kegiatan</h1>
    <ol class="breadcrumb">
      
      
    </ol>
</div>

<div class="page-content container-fluid">
	
	@php
	$id_jadwal = isset($data['detail_ruang']->id_jadwal) ? $data['detail_ruang']->id_jadwal:"";
	$tgl_jadwal = isset($data['detail_ruang']->tgl_show) ? $data['detail_ruang']->tgl_show:"";
	@endphp
	<div class="panel">
		<header class="panel-heading">
        	<div class="panel-actions"></div>
        	<h3 class="panel-title"></h3>
        </header>
        <div class="panel-body">
        	<div class="col-sm-6">
        		<form id="form_tambah">
	        		<div class="form-group row">
	                	<label class="col-md-3 col-form-label">Area</label>
	                    <div class="col-md-9">
	                      	<select class="form-control" name="area" id="area">
	                      		{{-- <option value="">-- Pilih Area --</option> --}}
	                      		@foreach($data['area'] as $a)
	                      		<option value="{{ $a->id_area }}">{{ $a->nama_area." ( ".$a->sisa_kursi." )" }}</option>
	                      		@endforeach
	                      	</select>
	                    </div>
	                </div>
	        	</form>
        	</div>
        	<div class="col-sm-6">
        	</div>
        	<div class="front">Depan</div>
        	<div class="table-responsive">
	        	<div class="container">
	        		<div id="seat-map">
	                    
	                    <div id="seat-info"></div>
	                                
	                </div>
	        	</div>
        	</div>
        	<div class="col-sm-12">
        		<div id="legend"></div>
        	</div>
        </div>
     </div>
</div>

<input type="hidden" name="id_jadwal" value="{{ $id_jadwal }}">
<input type="hidden" name="tgl_jadwal" value="{{ $tgl_jadwal }}">
<div class="modal fade" id="examplePositionSidebar" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog">
	<div class="modal-dialog modal-simple modal-sidebar modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title">Detail</h4>
            </div>
            <form id="form_modal" enctype="multipart/form-data" autocomplete="on">
            <div class="modal-body" id="modal_body">
            	
            	
            </div>
            <div class="modal-footer" id="modal_footer">
            	
            </div>
            </form>
        </div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	var store_area = '', store_ismember = '', store_gereja = '', store_gerejatext = '', store_wil = '', store_ling = '';
	var interval_1;
	var max_pesan = parseInt(5);
	var arr_duduk = [];
	var html_member = '<div class="form-group">'+
	            		'<label class="form-control-label">Member</label>'+
	            		'<select class="form-control" name="member" id="member" style="width: 100%" required="">'+
	            			'<option value="">-- Pilih --</option>'+
	            		'</select>'+
	            	'</div>';
	// var html_non = '<div class="form-group">'+
	// 					'<label class="form-control-label">NIK</label>'+
	// 					'<input type="text" class="form-control" name="popup_nik">'+
	// 				'</div>'+
	var html_non = 	'<div class="form-group">'+
						'<label class="form-control-label">Nama</label>'+
						'<input type="text" class="form-control" name="popup_nama">'+
					'</div>'+
					'<div class="form-group">'+
						'<label class="form-control-label">HP</label>'+
						'<input type="number" class="form-control" name="popup_hp">'+
					'</div>'+
					'<div class="form-group">'+
						'<label class="form-control-label">Alamat</label>'+
						'<textarea class="form-control" name="popup_alamat" required=""></textarea>'+
					'</div>'+
					'<div class="form-group">'+
	            		'<label class="form-control-label">Gereja</label>'+
	            		'<select class="form-control" name="popup_gereja" id="popup_gereja" style="width: 100%">'+
	            			'<option value="">-- Pilih --</option>'+
	            			'@php foreach($data['gereja'] as $g){ @endphp'+
	            			'@php echo '<option value="'.$g->id.'">'.$g->nama.'</option>' @endphp'+
	            			'@php } @endphp'+
	            			'<option value="9999">Lainnya</option>'+
	            		'</select>'+
	            	'</div>'+
	            	'<div class="form-group" id="div_parokitext" style="display: none">'+
						'<label class="form-control-label">Nama Gereja</label>'+
						'<input type="text" class="form-control" name="popup_gerejatext">'+
					'</div>'+
	            	'<div class="form-group" id="div_wil">'+
	            		'<label class="form-control-label">Wilayah</label>'+
	            		'<select class="form-control" name="popup_wilayah" id="popup_wilayah" style="width: 100%">'+
	            			
	            		'</select>'+
	            	'</div>'+
	            	'<div class="form-group" id="div_ling">'+
	            		'<label class="form-control-label">Lingkungan</label>'+
	            		'<select class="form-control" name="popup_lingkungan" id="popup_lingkungan" style="width: 100%">'+
	            			
	            		'</select>'+
	            	'</div>'+
					'<div class="form-group">'+
						'<label class="form-control-label">Foto</label>'+
						'<input type="file" accept="image/*" id="popup_foto"  class="form-control" name="popup_foto" value="">'+
					'</div>';
	$(document).ready(function(){
		$("#area").select2();
		$('#area').val($('#area option:first-child').val()).trigger('change');

		$("#btn_coba").click(function(){
			$("#examplePositionSidebar").modal("show");
		})
	})

	$("#area").change(function(){
		var area = $(this).val();
		store_area = area;

		if(area != ""){
			start(area);
		}
	})

	function render(area){
		var area = area;
		var id_jadwal = $("[name=id_jadwal]").val();

		$.ajax({
			url : "{{ url('lihat_show/get_duduk') }}",
			type : "post",
			dataType : "json",
			data : { id : area, id_jadwal : id_jadwal, tgl_show : $("[name=tgl_jadwal]").val()},
			headers : {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      	},
			success : function(respon){
				seat(respon.map, respon.column, respon);
			}
		})
	}

	function seat(map, colom, respon){
		$('.container').css({
          "margin": "0 auto",
          "width": respon.jum_kolom+"px",
          "text-align": "left"
      	});
		$('.seatCharts-row').remove();
	    $('.seatCharts-legendItem').remove();
	    $('#seat-map,#seat-map *').unbind().removeData();
		var sc = $('#seat-map').seatCharts({
    	map: map,
    	seats: { //Definition seat property
      		  f: {
		        price   : 100,
		        classes : 'first-class', 
		        category: '1st'
		      },
		      e: {
		        price   : 40,
		        classes : 'economy-class', 
		        category: '2nd'
		      },
		      d: {
		        price   : 40,
		        classes : 'second-class', 
		        category: '2nd'
		      }     
    	},
    	naming : { //Define the ranks of other information
      		top : true,
      		columns: colom,
      		rows : respon.row,
      		getLabel : function (character, row, column) {
        		return row+column;
      		}
    	},
    	legend : { //Definition legend
      		node : $('#legend'),
      		items : [
        		[ 'd', 'available', 'Sudah Ditempati' ],
		        [ 'f', 'available', 'Sudah Dipesan'],
		        [ 'e', 'available', 'Kosong']
      		]         
    	},
    	click: function () {
      		if (this.status() == 'available') {
		        var selected = sc.find('selected').length+1;

		        
		        //console.log(selected);
		        //return 'selected';
		        // -------- Single
		        var id = this.settings.id;
		        get_detail_duduk(id);
		        $("#examplePositionSidebar").modal("show");
		        return 'available';
		        // -------- Single

		        // -------- Multi
		        // if(selected <= max_pesan){
		        // 	arr_duduk.push(this.settings.id);
		        // 	console.log(arr_duduk);
		        // 	return 'selected';
		        // }else{
		        // 	return 'available';
		        // }
		        // -------- Multi
		        
		      } else if (this.status() == 'selected') {//choosen
		        
		        //Delete reservation
		        $('#cart-item-'+this.settings.id).remove();
		        // arr_duduk.splice( $.inArray(this.settings.id, arr_duduk) , 1);
		        // console.log(arr_duduk);
		        return 'available';
		      } else if (this.status() == 'unavailable') {//sold
		      	
		        //sold
		        return 'unavailable';
		      } else {
		        return this.style();
		        
		      }
		    },
  		});
	}

	function get_detail_duduk(id){
		if(id != ''){
			$.ajax({
				url : "{{ url('lihat_show/get_detail_duduk') }}",
				type : "post",
				dataType : "json",
				data : {area : store_area, id_jadwal : $("[name=id_jadwal]").val(), id : id, tgl_show : $("[name=tgl_jadwal]").val()},
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
				success : function(respon){
					if(respon.length > 0){
						fill_modal(respon);
						// $("#examplePositionSidebar").modal('show');
					}else{
						$("#modal_title").text('');
						$("#modal_body").html('');
						$("#modal_footer").html('');
					}
				}
			})
		}
	}

	function on_modal(){
	$("[name=is_member]").change(function(){
		// var id = $(this).val();
		var id = '';
		if (this.checked && this.value == '0'){
			id = this.value;
		}else{
			id = this.value;
		}
		store_ismember = id;
		if(id == 0){
			$("#div_member").html("");
			$("#div_member").html(html_member);
			load_select2_modal();
			$.ajax({
				url : "{{ url("lihat_show/get_member") }}",
				type : "get",
				dataType : "json",
				success : function(respon){
					$("#member").html("");
					$("#member").append("<option value=''>-- Pilih --</option>");
					if(respon.length > 0){
						for(i in respon){
							$("#member").append("<option value='"+respon[i].id_member+"'>"+respon[i].no_telp+" - "+respon[i].nama+"</option>");
						}
					}else{

					}
				}
			})
		}else if(id == 1){
			$("#div_member").html("");
			$("#div_member").html(html_non);
			convert_image();
			load_select2_modal();
			on_add_member();
		}else{
			$("#div_member").html("");
			
		}
	})
	store_gereja = ''; 
	store_gerejatext = '';
	store_wil = '';
	store_ling = '';
	}

	function fill_modal(data){
		$("#modal_body").html('');
		$("#modal_footer").html('');
		if(data[0].is_booking == 0 && data[0].is_pesan == 1){
			var html_header = 'Tambah Pengunjung';
			var html = '<div class="form-group">'+
            		'<label class="form-control-label">Status</label>'+
            		// '<select class="form-control" name="is_member" id="is_member" style="width: 100%" required="">'+
            		// 	'<option value="">-- Pilih --</option>'+
            		// 	'<option value="0">Member</option>'+
            		// 	'<option value="1">Bukan Member</option>'+
            		// '</select>'+
            		'<div>'+
                        '<div class="radio-custom radio-default radio-inline">'+
                        	'<input type="radio" id="inputBasicMale" name="is_member" value="0"/>'+
                        	'<label for="inputBasicMale">Member</label>'+
                        '</div>'+
                        '<div class="radio-custom radio-default radio-inline">'+
                        	'<input type="radio" id="inputBasicFemale" name="is_member" value="1"/>'+
                        	'<label for="inputBasicFemale">Non Member</label>'+
                        '</div>'+
                    '</div>'+
            	'</div>'+
            	'<br>'+
            	'<div id="div_member">'+
            		
            	'</div>'+
            	'<input type="hidden" name="id_duduk" value="'+data[0].id_duduk+'">'+
            	'<input type="hidden" name="id_lokasi" value="'+data[0].id_locations+'">'+
            	'<input type="hidden" name="id_ruang" value="'+data[0].id_rooms+'">'+
            	'<input type="hidden" name="val_area" value="">'+
            	'<input type="hidden" name="jadwal" value="">'+
            	'<input type="hidden" name="text_jadwal" value="'+data[0].text_jadwal+'">'+
            	'<input type="hidden" name="jenis_jadwal" value="'+data[0].jenis_jadwal+'">'+
            	'<input type="hidden" name="tgl_show" value="'+data[0].tgl_show+'">'
            	;
            	
			var html_footer = '<button type="button" id="btn_simpan" class="btn btn-primary btn-block" onclick="aksi_simpan()">Simpan</button>'+
            		'<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>';
            	
		}else{
			var html_header = 'Lihat Detail';
			var html = '<div class="table-responsive">'+
            		'<table class="table table-striped">'+
            			'<tr>'+
            				'<td width="15%">Area</td>'+
            				'<td width="5%">:</td>'+
            				'<td width="30%">'+data[0].nama_area+'</td>'+
            			'</tr>'+
            			'<tr>'+
            				'<td width="15%">Nomor Kursi</td>'+
            				'<td width="5%">:</td>'+
            				'<td width="30%">'+data[0].row+" "+data[0].kolom+'</td>'+
            			'</tr>'+
            			'<tr>'+
            				'<td width="15%">Nama</td>'+
            				'<td width="5%">:</td>'+
            				'<td width="30%">'+data[0].nama_pemesan+'</td>'+
            			'</tr>'+
            			'<tr>'+
            				'<td width="15%">Status Pesanan</td>'+
            				'<td width="5%">:</td>'+
            				'<td width="30%">'+data[0].text_status+'</td>'+
            			'</tr>'+
            			'<tr>'+
            				'<td width="15%">Kedatangan</td>'+
            				'<td width="5%">:</td>'+
            				'<td width="30%">'+data[0].text_pesan+'</td>'+
            			'</tr>'+
            		'</table>'+
            	'</div>';
            var html_footer = '<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>';
		}
		var html_ = "<h4>Kursi : "+data[0].row+" "+data[0].kolom+"</h4><br>";
		$("#modal_title").text(html_header);
		$("#modal_body").append(html_+html);
		$("#modal_footer").append(html_footer);
		load_select2_modal();
		on_modal();
		$("[name=val_area]").val(store_area);
		$("[name=jadwal]").val($("[name=id_jadwal]").val());
		$("#examplePositionSidebar").modal('show');
	}

	function load_select2_modal(id, modal){
		$("#member, #popup_gereja, [name=popup_wilayah], [name=popup_lingkungan]").select2({
			dropdownParent: $("#examplePositionSidebar")
		});
	}

	function on_add_member(){
	$("[name=popup_gereja]").change(function(){
		var parent = $(this).val();
		store_gereja = parent;
		$("[name=popup_gerejatext]").val('');
		if(parent != '' && parent != 9999){
        	$("#div_parokitext").hide();
        	$("#div_wil").show();
        	$("#div_ling").show();        	
        	$.ajax({
	            url : "{{ url('lihat_show/get_lingkungan') }}",
	            type : "post",
	            dataType : "html",
	            data : { id : 1, parent : parent },
	            headers : {
	              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            success : function(respon){
	              $("[name=popup_wilayah]").html(respon);
	            },
	              complete : function(respon){
	                  
	            }
          	})
        }else if(parent > 9000){
	        $("#div_parokitext").show();
	        $("#div_wil").hide();
	        $("#div_ling").hide();
	        $("[name=popup_wilayah]").html("");
	        $("[name=popup_lingkungan]").html("");
        }else{
	        $("#div_parokitext").hide();
	        $("#div_wil").show();
	        $("#div_ling").show();
	        $("[name=popup_wilayah]").html("");
	        $("[name=popup_lingkungan]").html("");
        }
	})

	$("[name=popup_wilayah]").change(function(){
        var parent = $(this).val();
        if(parent != ''){
        	$.ajax({
	            url : "{{ url('lihat_show/get_lingkungan') }}",
	            type : "post",
	            dataType : "html",
	            data : { id : 2, parent : parent },
	            headers : {
	              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            success : function(respon){
	              $("[name=popup_lingkungan]").html(respon);
	            },
	              complete : function(respon){
	                  
	            }
	        })
        }else{
        	$("[name=popup_lingkungan]").html("");
        }
    })
	}

	function aksi_simpan(){
		var is_member = store_ismember; //$("#is_member").val();
		var member = '-';
		var nik = '-';
		var nama = '-';

		if(is_member == 0){
			member = $("#member").val();
			store_gereja = '0';
			store_gerejatext = '0';
			store_wil = '0';
			store_ling = '0';
		}else if(is_member == 1){
			nik = $("[name=popup_nik]").val();
			nama = $("[name=popup_nama]").val();
		}

		if(store_gereja != '' && store_gereja != 9999){
			store_gerejatext = '0';
			store_wil = $("[name=popup_wilayah]").val();
			store_ling = $("[name=popup_lingkungan]").val();
		}else if(store_gereja > 9000){
			store_gerejatext = $("[name=popup_gerejatext]").val();
			store_wil = '0';
			store_ling = '0';
		}else{
			store_gerejatext = '';
			store_wil = '';
			store_ling = '';
		}
		
		
		if(is_member != '' && member != '' && nama != '' && store_gereja != '' && store_gerejatext != '' && store_wil != '' && store_ling != ''){
			$.ajax({
				url : "{{ url('lihat_show/simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(), //fd
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
				success : function(respon){
					if(respon.status == 0){
						swal("", respon.keterangan, "error");
						start(store_area);
						$("#examplePositionSidebar").modal("hide");
					}else if(respon.status == 1){
						swal("", respon.keterangan, "success");
						start(store_area);
						get_area();
						$("#examplePositionSidebar").modal("hide");
					}else if(respon.status == 2){
						swal("", respon.keterangan, "error");
						start(store_area);
						
					}else{
						start(store_area);
						$("#examplePositionSidebar").modal("hide");
					}
				}
			})
		}else{
			swal("", "Silahkan isi inputan yang tersedia", "warning");
		}
	}

	function start(area){
        render(area);

        clearInterval(interval_1);
        interval_1 = setInterval(function () {
            render(area);
            // $("#examplePositionSidebar").modal('hide');
        }, 180 * 1000);
    }


    function convert_image(){
    	var foto = document.getElementById('popup_foto');
    	var max_width = 640;
	    var max_height = 480;
	    var form = document.getElementById('form_modal');

	    function processfile(file, txt_image) {
	      
	        if( !( /image/i ).test( file.type ) ){
	                alert( "File "+ file.name +" is not an image." );
	                // foto.value = null;
	                return false;
	        }
	            var reader = new FileReader();
	            reader.readAsArrayBuffer(file);
	            
	            reader.onload = function (event) {
	                var blob = new Blob([event.target.result]);
	                window.URL = window.URL || window.webkitURL;
	                var blobURL = window.URL.createObjectURL(blob);
	              
	                var image = new Image();
	                image.src = blobURL;
	                image.onload = function() {
	                    var resized = resizeMe(image); 
	                    var newinput = document.createElement("input");
	                    newinput.type = 'hidden';
	                    newinput.name = txt_image;
	                    newinput.value = resized;
	                    form.appendChild(newinput);
	                }
	            };
	        
	    }

	    function readfiles(files, txt_image) {
	      
	        var existinginputs = document.getElementsByName(txt_image);
	        while (existinginputs.length > 0) { 
	            form.removeChild(existinginputs[0]);
	        } 
	      
	        for (var i = 0; i < files.length; i++) {
	            processfile(files[i], txt_image);
	        }
	    }

	    foto.onchange = function(){
	        if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
	            alert('The File APIs are not fully supported in this browser.');
	            return false;
	        }else{
	             readfiles(foto.files, 'img_foto');
	        }
	    }

	    function resizeMe(img) {
	      
	        var canvas = document.createElement('canvas');
	        var width = img.width;
	        var height = img.height;

	        if (width > height) {
	            if (width > max_width) {
	                height = Math.round(height *= max_width / width);
	                width = max_width;
	            }
	        } else {
	            if (height > max_height) {
	                width = Math.round(width *= max_height / height);
	                height = max_height;
	            }
	        }
	        canvas.width = width;
	        canvas.height = height;
	        var ctx = canvas.getContext("2d");
	        ctx.drawImage(img, 0, 0, width, height);
	      
	        return canvas.toDataURL("image/jpeg",0.7);

	    }
    }

    function get_area(){
		var tgl = '{{ $tgl_jadwal }}';
		var id_jadwal = '{{ $id_jadwal }}';

		if(tgl != '' && id_jadwal != ''){
			$.ajax({
				url : "{{ url('lihat_show/get_area') }}",
				type : "POST",
				dataType : "html",
				data : { tgl : tgl, id_jadwal : id_jadwal},
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		$("#area").html("");
		      		$("#area").html(respon);
		      	},
		      	complete : function(respon){
		      		$('#area').val(store_area).trigger('change');
		      	}
			})
		}
	}

</script>
@endsection