@extends('templates.index')
@section('title', 'Verifikasi Member')

@section('content')
<div class="page-header">
    <h1 class="page-title">Member</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('verifikasi_daftar') }}">Verifikasi Member</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama</th>
	                 	<th>HP</th>
	                 	<th>Tanggal Registrasi</th>
	                 	<th>Status</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            <h4 class="modal-title" id="modal_title">Detail Pendaftaran</h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_iduser">
            <input type="hidden" name="popup_idmember" value="">
            <div class="modal-body" id="modal_body">
            	<table class="table table-hover table-striped w-full display nowrap" style="width:100%">
            		<tr>
            			<td>Tanggal Daftar</td>
            			<td>:</td>
            			<td id="td_daftar"></td>
            		</tr>
            		<tr>
            			<td width="30%">Nama</td>
            			<td width="5%">:</td>
            			<td id="td_nama"></td>
            		</tr>
            		<tr>
            			<td>Jenis Kelamin</td>
            			<td>:</td>
            			<td id="td_jk"></td>
            		</tr>
            		<tr>
            			<td>HP</td>
            			<td>:</td>
            			<td id="td_hp"></td>
            		</tr>
            		<tr>
            			<td>Alamat</td>
            			<td>:</td>
            			<td id="td_alamat"></td>
            		</tr>
            		<tr>
            			<td>Gereja</td>
            			<td>:</td>
            			<td id="td_gereja"></td>
            		</tr>
            		<tr>
            			<td>Wilayah</td>
            			<td>:</td>
            			<td id="td_wilayah"></td>
            		</tr>
            		<tr>
            			<td>Lingkungan</td>
            			<td>:</td>
            			<td id="td_ling"></td>
            		</tr>            		
            		<tr>
            			<td>Foto</td>
            			<td>:</td>
            			<td>
            				<div class="example">
	            				<a class="inline-block" href="" data-plugin="magnificPopup" data-main-class="mfp-img-mobile">
                      			<img class="img-fluid" src="" alt="..." width="220"/>
                    			</a>
	                    	</div>
            			</td>
            		</tr>
            	</table>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            	<button type="button" class="btn btn-primary" id="btn_simpan">Verifikasi</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		

		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: '{{ url('verifikasi_daftar/get_data') }}',
	        columns: [
	            {data: 'nama', name: 'nama', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'nama', name: 'nama'},	            
	            {data: 'no_telp', name: 'no_telp'},
	            {data: 'tanggal_reg', name: 'tanggal_reg'},
	            {data: 'status', name: 'status'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	function submit_check(id, user){
		
		swal({
      		title: "Verifikasi Akun?",
	        text: "Anda akan memverifikasi data ini",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('verifikasi_daftar/check')}} ",
	          	type: 'post',
	          	dataType : 'json',
	          	data : { id : id, user : user},
			    headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	          	success: function(respon){
	          		$("#examplemodal").modal("hide");
	          		table.ajax.reload();
	         	}
        	})
    	})
	}

	$("#btn_simpan").click(function(){
		var id_user = $("[name=popup_iduser]").val();
		var id_member = $("[name=popup_idmember]").val();

		if(id_user != '' && id_member != ''){
			submit_check(id_member, id_user);
		}
	})

	function check(this_){
		var id = $(this_).attr("data-id");
		var user = $(this_).attr("data-user");

		if(id != '' && user != ''){
			$.ajax({
				url : "{{ url('verifikasi_daftar/get_detail') }}",
				data : { id_member : id, id_user : user },
				dataType : "json",
				type : "post",
				headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		if(respon.status.status == '1'){
		      			show_data(respon.data);
		      		}else{
		      			swal("", "Gagal mengambil data", "error");
		      		}
		      	}
			})
		}
	}

	function show_data(respon){
		var id_member = respon.id;
		var id_user = respon.id_user;

		$("[name=popup_iduser]").val(id_user);
		$("[name=popup_idmember]").val(id_member);

		$("#td_daftar").text(respon.tgl_registrasi_);
		$("#td_nama").text(respon.nama);
		$("#td_jk").text(respon.jenkel);
		$("#td_hp").text(respon.no_telp);
		$("#td_alamat").text(respon.alamat);
		$("#td_gereja").text(respon.nama_lokasi);
		$("#td_wilayah").text(respon.nama_wilayah);
		$("#td_ling").text(respon.nama_lingkungan);
		if(respon.foto_member != ''){
			$(".example").show();
			$(".inline-block").attr("href", respon.foto_member);
			$(".img-fluid").attr("src", respon.foto_member);
		}else{
			$(".example").hide();
		}
		$("#examplemodal").modal("show");
	}

</script>
@endsection