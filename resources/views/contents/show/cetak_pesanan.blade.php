 <?php

class Pdf extends PDF_MC_Table{
	//Page header
	//public $kas_masuk,$tgl1,$tgl2;
	public $def_width = 58, $def_height = 170;
	function __construct() {
		//set Page 
		parent::__construct('P','mm',array($this->def_width , $this->def_height));
        
    }
	function Header(){
        
		if($this->PageNo() != 0){
			$spacing = 5;
            $kolom_1 = 20;
            $kolom_2 = 5;
            $kolom_3 = 30;
            $kolom_4 = 10;
            $ln = 3;
            $indent_1 = $kolom_1+$kolom_2+$kolom_3+20;

            $x = $this->GetX();
            $y = $this->GetY();

            $this->setFont('Arial','B',14);
            $this->setFillColor(255,255,255);
            $this->setTextColor(0,0,0);
            // $this->Ln();
        }
				
	}
	
	function Content(){


        $this->cell(0, 5, "Ticket QRCode", 0, 1, "L");
        $this->Image($this->url,12.5,12,33,0,'PNG');
        $this->SetY($this->GetY()+40);
        $this->setFont('Arial','B',14);
        $this->setFillColor(255,255,255);
        $this->setTextColor(0,0,0);
        $this->cell(0, 5, $this->gate, 0, 1, "L");
        
        $this->setFont('arial','B',8);
        $this->SetY($this->GetY()+5);
        $this->SetWidths(array(15,2,35));
        $this->SetAligns(array('L','C','L'));
        // $this->Row_noborder(array('No.HP',':',$this->data->no_hp),3);
        $this->Row_noborder(array('Nama',':',$this->data->nama_ktp),3);
        $this->Row_noborder(array('Gereja',':',$this->data->nama_lokasi),3);
        $this->Row_noborder(array('Gate',':',$this->gate),3);
        $this->Row_noborder(array('Jadwal',':',$this->waktu),3);
        $this->Ln();
		
        $this->setFont('arial','B',8);
        $this->SetWidths(array(18, 28));
        $this->SetAligns(array('C', 'C'));
        $this->Row(array("KURSI", "PENGUNJUNG"));

        $this->setFont('arial','',8);
        $this->SetWidths(array(18, 28));
        $this->SetAligns(array('C', 'L'));
        foreach($this->detail as $d){
            $this->Row(array($d->nama_area.' - '.trim(strtolower($d->nama_area),'area ').$d->kolom, $d->nama_ktp));
        }
        
                

	}
	
	function Footer()
	{
		$this->Ln();
		
		$this->SetY(-15);
		//buat garis horizontal
		
		//Arial italic 9
		$this->SetFont('Arial','B',9);
        $this->Cell(0,10,'',0,0,'L');
		//nomor halaman
	}
    function namalabel($nama,$x,$y,$key,$k,$xx,$yy){
        $this->setXY($x,$y+32);
        $this->cell(33,'5',$nama."/".$y."/key=".$key,1,0,'C');       
        $r_X = $this->GetX()-$k*33;
        $r_Y = $yy;
        if($k%6==0){
            $r_X = $this->GetX()-198;
        }
         
        $this->setXY($this->GetX(),$this->GetY());
        //$this->setXY($r_X,$y);
        // $this->setXY($x,$y);
        //$this->setX($this->GetX()-198);
    }
}

$Pdf = new Pdf();

$Pdf->data      = $data['data'];
$Pdf->detail    = $data['detail'];
$Pdf->url       = $data['url'];
$Pdf->gate      = $data['gate'];
$Pdf->waktu     = $data['waktu'];

$Pdf->SetAutoPageBreak(true ,15);
$Pdf->SetMargins(5,5,5);
$Pdf->AliasNbPages();
$Pdf->AddPage();
$Pdf->SetFont('Arial','',11);
$Pdf->Content();
$Pdf->SetTitle("Cetak Pesan");
$Pdf->Output("Cetak Pesan.pdf", "I");

?>