@extends('templates.index')
@section('title', 'Jadwal Petugas')
@section('css')
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/select2/select2.css') }}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.css') }}">

<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/timepicker/jquery-timepicker.css')}}">
<link rel="stylesheet" href="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
<style type="text/css">
	.bootstrap-timepicker-widget.dropdown-menu {
    z-index: 99999!important;
}
</style>
@endsection
@section('content')
<div class="page-header">
    <h1 class="page-title">Jadwal Petugas</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Pilih Petugas</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<header class="panel-heading">
            <h3 class="panel-title">
              	<button type="button" class="btn btn-primary" id="btn_tambah"><i class='fa fa-plus'></i> Tambah</button>
            </h3>
          </header>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Nama Jadwal</th>
	                    <th>Tanggal</th>
	                    <th>Petugas</th>
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="popup_id">
            <div class="modal-body" id="modal_body">            	
                <div class="form-group row">
	            	<label class="col-form-control col-lg-2">Tanggal</label>
	            	<div class="col-lg-4">
	            		<input type="text" name="popup_tanggal" class="form-control" value="{{ date("d-m-Y") }}" readonly>
	            	</div>

	            	<label class="col-form-label col-lg-2">Jadwal</label>
	            	<div class="col-lg-4">
	            		<select class="form-control" name="popup_jadwal" id="popup_jadwal">
	                        @foreach($data['jadwal'] as $d)
		            		    <option value="{{$d->id_jadwal}}">{{$d->nama}} ( {{($d->status==1)?tgl_full($d->hari,'10'):tgl_full($d->tanggal,'0')}} {{$d->jam}})</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>
            	
	            <div class="form-group row">
	            	<label class="col-form-label col-lg-2">Petugas</label>
	            	<div class="col-lg-8">
	            		<select class="form-control select2 col-lg-10" name="popup_petugas[]" multiple="multiple" id="popup_petugas">
		            		<!-- <option value=""> -- Pilih -- </option> -->
		            		@foreach($data['member'] as $d)
		            		<option value="{{$d->id_member}}">{{$d->nama}}</option>
		            		@endforeach
		            	</select>
	            	</div>
	            </div>

                

            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-default btn-batal-modul">Batal</button>
            	<button type="button" class="btn btn-primary" id="btn_simpan">Simpan</button>
            </div>
            </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="{{ asset('themeforest/global/vendor/select2/select2.full.min.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>

<script src="{{ asset('themeforest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('themeforest/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script type="text/javascript">
	var table;
	var id_table = '';
	$(document).ready(function(){
		var start = new Date();
		$('.select2').select2();

        $('.datepicker').datepicker({
	      format : 'dd-mm-yyyy',
	      setDate: new Date(),
	      startDate: new Date(),
	      autoclose: true,
	      clearBtn: true,
	    });

    

	   
		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: "{{ url('jadwal_petugas_show') }}",
	        columns: [
	            {data: 'id', name: 'id', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'jadwal', name: 'jadwal'},
	            {data: 'tgl_jadwal', name: 'tgl_jadwal'},
	            {data: 'petugas', name: 'petugas'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});

		$("#div_gereja").hide();
	})

	$("#btn_tambah").click(function(){
		clear_input();
		get_jadwal($("[name=popup_tanggal]").val());
		$("#modal_title").text("Pilih Petugas");
		$("#examplemodal").modal('show');
	})

	$(".btn-batal-modul").on("click", function(e){
	      $("#modal_title").text("Pilih Petugas");
	      $("#examplemodal").modal('hide');
	})

	$("#btn_simpan").click(function(){
		var id = $("[name=popup_id]").val();
		var tanggal = $("[name=popup_tanggal]").val();
		var petugas = $("#popup_petugas").val();
		var jadwal = $("[name=popup_jadwal]").val();

		if(tanggal != '' && petugas.length > 0 && jadwal != ''){
			$.ajax({
				url : "{{ url('jadwal_petugas_simpan') }}",
				type : "POST",
				dataType : "json",
				data : $("#form_modal").serialize(),
				headers : {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
		      	success : function(respon){
		      		table.ajax.reload();
		      		$("#examplemodal").modal("hide");
		      	}
			})
		}else{
			swal("", "Silahkan lengkapi inputan yang ada", "warning");
		}
	})

	function edit(data_id){
		var id = $("#table_id"+data_id).val();
		var idjadwal = $("#table_idjadwal"+data_id).val();
        
		var idpetugas = $("#table_idpetugas"+data_id).val().split(',');
		var tanggal = $("#table_tanggal"+data_id).val();
		get_jadwal(tanggal);
		id_table = data_id;

		console.log(data_id);

		$("[name=popup_id]").val(data_id);
		$("[name=popup_jadwal]").val(idjadwal).trigger('change');		
		$("#popup_petugas").val(idpetugas).trigger('change');
		$("[name=popup_tanggal]").val(tanggal);

		$("#examplemodal").modal("show");
	}

	function hapus(id){
		swal({
      		title: "Hapus data?",
	        text: "Anda akan menghapus data ",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonClass: "btn-warning",
	        confirmButtonText: 'Ya',
	        cancelButtonText: "Tidak",
    	},function(){      
        	$.ajax({
	          	url: "{{ url('jadwal_petugas_hapus')}} ",
	          	type: 'post',
	          	data: {id : id},
			       headers : {
			       	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			       },
	          	success: function(respon){ 
	          		table.ajax.reload();
	          		// swal("Deleted!", "Your imaginary file has been deleted!", "success");
	         	}
        	})
    	})
	}

	function clear_input(){
		$("[name=popup_id]").val('');
		$("[name=popup_jadwal]").val('').trigger('change');
		$("#popup_petugas").val(null).trigger('change');
		$("[name=popup_tanggal]").val('{{ date('d-m-Y') }}');
		id_table = '';
	}

	function get_jadwal(var_){
		var tgl = var_;

		if(tgl != ''){
			$.ajax({
				url : "{{ url('jadwal_petugas/get_jadwal') }}",
				type : "post",
				dataType : "html",
				data : { tanggal : tgl},
				headers : {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			    success : function(respon){
			    	if(respon.length > 0){
			    		$("[name=popup_jadwal]").html("");
			    		$("[name=popup_jadwal]").append(respon);
			    	}else{
			    		$("[name=popup_jadwal]").html("");
			    	}
			    },
			    complete : function(respon){
			    	if(id_table != ''){
			    		var id_jadwal = $("#table_idjadwal"+id_table).val();
			    		$("[name=popup_jadwal]").val(id_jadwal).trigger("change");
			    	}
			    }
			})
		}
	}


</script>
@endsection