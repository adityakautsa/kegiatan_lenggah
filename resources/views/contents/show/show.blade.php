@extends('templates.index')
@section('title', 'Show Hari Ini')

@section('content')
<div class="page-header">
    <h1 class="page-title">Kegiatan</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('lihat_show') }}">Lihat Jadwal</a></li>
      
    </ol>
</div>

<div class="page-content container-fluid">
	<div class="panel">
		<div class="page-header">
			<button class="btn btn-warning btn-icon" type="button" id="btn_search"><i class='fa fa-filter'></i></button>
		</div>
		<div class="panel-body">
			<table class="table table-hover dtTable table-striped w-full display nowrap" style="width:100%">
				<thead>
	                <tr>
	                 	<th></th>
	                 	<th>Tanggal</th>
	                 	<th>Jam Mulai</th>
	                 	<!-- <th>Jam Selesai</th>
	                 	<th>Lokasi</th>
	                 	<th>Ruangan</th> -->
	                 	<th></th>
	                </tr>
	              </thead>
			</table>			
		</div>
	</div>
</div>
<div class="modal fade" id="examplemodal" aria-hidden="true" aria-labelledby="examplemodal" role="dialog">
	<div class="modal-dialog modal-simple modal-top modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
                	<span aria-hidden="true">×</span>
                </button>
            <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <form id="form_modal">
            <input type="hidden" name="id">
            <div class="modal-body" id="modal_body">
            	<div class="form-group row">
	            	<label class="col-form-label col-lg-3">Tanggal</label>
	            	<div class="col-lg-9">
	            		<input type="text" name="popup_tanggal" class="form-control date_fromtoday" value="{{ date('d-m-Y') }}" readonly>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer" id="modal_footer">
            	<button type="button" class="btn btn-primary" id="btn_filter">Filter</button>
            </div>
            </form>
        </div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		table = $(".dtTable").DataTable({
			processing: true,
	        serverSide: true,
	        responsive: true,
	        searchDelay: 2000,
	        ajax: {
	        	url : '{{ url('lihat_show/get_data') }}',
	        	type : "post",
	        	headers : {
	        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},
	        	data : function(data){
	        		data['tanggal'] = $("[name=popup_tanggal]").val();
	        	}
	        },
	        columns: [
	            {data: 'tanggal', name: 'tanggal', orderable: false, searchable: false, render : function(data, type, row, meta){
			  		return meta.row+1;
			  	}},
	            {data: 'tanggal', name: 'tanggal'},
	            {data: 'jam_mulai', name: 'jam_mulai'},
	            // {data: 'jam_selesai', name: 'jam_selesai'},
	            // {data: 'nama_lokasi', name: 'nama_lokasi'},
	            // {data: 'nama_ruang', name: 'nama_ruang'},
	            {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
	        ],
		});
	})

	$("#btn_search").click(function(){
		$("#examplemodal").modal("show");
	})

	$("#btn_filter").click(function(){
		var tgl = $("[name=popup_tanggal]").val();

		if(tgl != ''){
			$("#examplemodal").modal("hide");
			table.ajax.reload();
		}
	})
</script>
@endsection