-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.6.37 - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table ticket_laravel.user_menu
CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `id_group` int(250) DEFAULT NULL,
  `parent_id` smallint(3) DEFAULT NULL,
  `child_id` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Membuang data untuk tabel ticket_laravel.user_menu: ~19 rows (lebih kurang)
DELETE FROM `user_menu`;
/*!40000 ALTER TABLE `user_menu` DISABLE KEYS */;
INSERT INTO `user_menu` (`id`, `id_group`, `parent_id`, `child_id`) VALUES
	(1, 1, 0, 1),
	(2, 1, 0, 2),
	(3, 1, 0, 3),
	(4, 1, 2, 4),
	(5, 1, 2, 5),
	(6, 1, 2, 6),
	(7, 1, 2, 7),
	(9, 1, 0, 9),
	(10, 1, 9, 10),
	(11, 1, 0, 14),
	(12, 1, 14, 15),
	(13, 1, 14, 16),
	(14, 1, 14, 17),
	(15, 1, 0, 18),
	(16, 1, 18, 19),
	(17, 1, 0, 20),
	(18, 1, 20, 21),
	(19, 1, 20, 22),
	(20, 1, 22, 23),
	(21, 1, 22, 24),
	(22, 1, 22, 25),
	(23, 1, 22, 26),
	(24, 1, 22, 27),
	(25, 1, 22, 28),
	(26, 1, 18, 29),
	(30, 3, 0, 18),
	(31, 3, 18, 19),
	(32, 3, 18, 29),
	(33, 4, 0, 22),
	(34, 4, 22, 27),
	(35, 4, 22, 28),
	(36, 4, 0, 14),
	(37, 4, 14, 16),
	(38, 4, 14, 15);
/*!40000 ALTER TABLE `user_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
