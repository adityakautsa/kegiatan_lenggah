ALTER TABLE `member` ADD COLUMN `is_blacklist` INT(2) NOT NULL DEFAULT '0' AFTER `is_petugas`;
ALTER TABLE `member` CHANGE COLUMN `id_lokasi` `id_lokasi` TEXT NOT NULL DEFAULT '' AFTER `tgl_aktivasi`;
ALTER TABLE `user_accounts` CHANGE COLUMN `id_lokasi` `id_lokasi` VARCHAR(255) NULL DEFAULT '1' AFTER `active`;