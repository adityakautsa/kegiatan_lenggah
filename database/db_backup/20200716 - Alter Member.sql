ALTER TABLE `member`
	ADD COLUMN `id_lokasi` INT NOT NULL AFTER `tgl_aktivasi`,
	ADD COLUMN `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() AFTER `is_petugas`,
	ADD COLUMN `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() AFTER `created_at`;

ALTER TABLE `member`
	CHANGE COLUMN `nik` `nik` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci' AFTER `id`,
	CHANGE COLUMN `tempat_lahir` `tempat_lahir` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci' AFTER `nama`,
	CHANGE COLUMN `tgl_lahir` `tgl_lahir` DATE NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci' AFTER `tempat_lahir`,
	CHANGE COLUMN `jenis_kelamin` `jenis_kelamin` INT NOT NULL DEFAULT 0 AFTER `tgl_lahir`,
	CHANGE COLUMN `provinsi` `provinsi` INT NOT NULL DEFAULT 0 COLLATE 'utf8mb4_general_ci' AFTER `jenis_kelamin`,
	CHANGE COLUMN `kabupaten` `kabupaten` INT NOT NULL DEFAULT 0 COLLATE 'utf8mb4_general_ci' AFTER `provinsi`,
	CHANGE COLUMN `kecamatan` `kecamatan` INT NOT NULL DEFAULT 0 COLLATE 'utf8mb4_general_ci' AFTER `kabupaten`,
	CHANGE COLUMN `desa` `desa` INT NOT NULL DEFAULT 0 COLLATE 'utf8mb4_general_ci' AFTER `kecamatan`,
	CHANGE COLUMN `alamat` `alamat` TEXT NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci' AFTER `desa`,
	CHANGE COLUMN `tgl_registrasi` `tgl_registrasi` DATE NOT NULL COLLATE 'utf8mb4_general_ci' AFTER `no_telp`,
	CHANGE COLUMN `tgl_aktivasi` `tgl_aktivasi` DATE NOT NULL COLLATE 'utf8mb4_general_ci' AFTER `tgl_registrasi`,
	CHANGE COLUMN `id_lokasi` `id_lokasi` INT NOT NULL DEFAULT 0 AFTER `tgl_aktivasi`,
	CHANGE COLUMN `is_aktif` `is_aktif` INT(2) NOT NULL DEFAULT 1 COMMENT '0 -> non, 1 -> aktif' COLLATE 'utf8mb4_general_ci' AFTER `id_lokasi`,
	CHANGE COLUMN `is_petugas` `is_petugas` INT(2) NOT NULL DEFAULT 0 COLLATE 'utf8mb4_general_ci' AFTER `is_aktif`,
	CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `is_petugas`,
	CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`;

ALTER TABLE `member`
	CHANGE COLUMN `provinsi` `provinsi` BIGINT NOT NULL DEFAULT 0 AFTER `jenis_kelamin`,
	CHANGE COLUMN `kabupaten` `kabupaten` BIGINT NOT NULL DEFAULT 0 AFTER `provinsi`,
	CHANGE COLUMN `kecamatan` `kecamatan` BIGINT NOT NULL DEFAULT 0 AFTER `kabupaten`,
	CHANGE COLUMN `desa` `desa` BIGINT NOT NULL DEFAULT 0 AFTER `kecamatan`;

