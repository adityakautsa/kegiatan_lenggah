-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.37 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ticket_laravel
CREATE DATABASE IF NOT EXISTS `ticket_laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ticket_laravel`;

-- Dumping structure for table ticket_laravel.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `id_booking` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `id_seat` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `kategori_jadwal` varchar(255) DEFAULT NULL,
  `tgl_booking` date DEFAULT NULL,
  `tgl_show` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `is_show` char(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_booking`),
  KEY `fk_booking_member` (`id_member`),
  KEY `fk_booking_lokasi` (`id_location`),
  KEY `fk_booking_room` (`id_room`),
  KEY `fk_booking_jadwal` (`id_jadwal`),
  KEY `fk_booking_seat` (`id_seat`),
  CONSTRAINT `fk_booking_jadwal` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_lokasi` FOREIGN KEY (`id_location`) REFERENCES `locations` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_room` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_seat` FOREIGN KEY (`id_seat`) REFERENCES `seats` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ticket_laravel.booking: ~1 rows (approximately)
DELETE FROM `booking`;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` (`id_booking`, `id_member`, `id_location`, `id_room`, `id_seat`, `id_jadwal`, `kategori_jadwal`, `tgl_booking`, `tgl_show`, `status`, `is_show`, `created_at`, `updated_at`) VALUES
	(2, 1, 1, 1, 1, 2, '0', '2020-07-09', '2020-07-09', 0, '1', '2020-07-09 15:30:22', '2020-07-09 15:30:22');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
