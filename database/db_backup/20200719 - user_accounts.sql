-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.6.37 - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table ticket_laravel.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `retype_password` varchar(150) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `active` tinyint(255) DEFAULT NULL,
  `id_lokasi` int(255) DEFAULT NULL,
  `id_group` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(250) DEFAULT NULL,
  `id_member` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Membuang data untuk tabel ticket_laravel.user_accounts: ~1 rows (lebih kurang)
DELETE FROM `user_accounts`;
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `nik`, `nama`, `username`, `password`, `retype_password`, `no_telepon`, `email`, `ip_address`, `active`, `id_lokasi`, `id_group`, `created_at`, `created_by`, `updated_at`, `updated_by`, `id_member`) VALUES
	(1, '34343', '434343', 'admin', 'admin', 'admin', NULL, 'admin', NULL, 1, 1, '1', '2020-06-12 19:54:46', NULL, '2020-07-18 10:46:50', NULL, 10),
	(3, '1234567890123456', 'Danur Windo', 'danur', 'danur', NULL, NULL, NULL, NULL, 1, 7, '3', '2020-07-18 11:02:53', NULL, '2020-07-19 12:41:55', NULL, 9),
	(5, '3506010987123456', 'Mahmudi Eka Sari', 'eksa_sari', 'eka_sari', 'eka_sari', NULL, NULL, NULL, 0, 7, '3', '2020-07-18 11:17:39', NULL, '2020-07-18 11:17:39', NULL, 8);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
