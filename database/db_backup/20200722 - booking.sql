/*
 Navicat Premium Data Transfer

 Source Server         : locahost
 Source Server Type    : MySQL
 Source Server Version : 100412
 Source Host           : localhost:3306
 Source Schema         : ticket

 Target Server Type    : MySQL
 Target Server Version : 100412
 File Encoding         : 65001

 Date: 22/07/2020 14:55:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking`  (
  `id_booking` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_seat` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `kategori_jadwal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_booking` date NULL DEFAULT NULL,
  `tgl_show` date NULL DEFAULT NULL,
  `status` int(2) NULL DEFAULT 0,
  `is_show` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_covid` int(2) NULL DEFAULT 0,
  `tgl_sembuhcovid` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_booking`) USING BTREE,
  INDEX `fk_booking_member`(`id_member`) USING BTREE,
  INDEX `fk_booking_lokasi`(`id_location`) USING BTREE,
  INDEX `fk_booking_jadwal`(`id_jadwal`) USING BTREE,
  INDEX `fk_booking_seat`(`id_seat`) USING BTREE,
  CONSTRAINT `fk_booking_jadwal` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_lokasi` FOREIGN KEY (`id_location`) REFERENCES `locations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of booking
-- ----------------------------
INSERT INTO `booking` VALUES (16, 1, 1, 0, 1, '0', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-16 15:44:56', '2020-07-16 15:44:56');
INSERT INTO `booking` VALUES (17, 1, 1, 0, 1, '0', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-16 16:17:32', '2020-07-16 16:17:32');
INSERT INTO `booking` VALUES (18, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 10:52:26', '2020-07-17 10:52:26');
INSERT INTO `booking` VALUES (19, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 11:32:26', '2020-07-17 11:32:26');
INSERT INTO `booking` VALUES (20, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:32:26', '2020-07-17 15:32:26');
INSERT INTO `booking` VALUES (21, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:32:42', '2020-07-17 15:32:42');
INSERT INTO `booking` VALUES (22, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:33:16', '2020-07-17 15:33:16');
INSERT INTO `booking` VALUES (23, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:33:38', '2020-07-17 15:33:38');
INSERT INTO `booking` VALUES (24, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:34:56', '2020-07-17 15:34:56');
INSERT INTO `booking` VALUES (25, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:35:57', '2020-07-17 15:35:57');
INSERT INTO `booking` VALUES (26, 1, 1, 0, 1, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-17 15:36:11', '2020-07-17 15:36:11');
INSERT INTO `booking` VALUES (27, 1, 1, 0, 5, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-21 15:27:30', '2020-07-21 15:27:30');
INSERT INTO `booking` VALUES (28, 1, 1, 0, 5, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-21 15:27:30', '2020-07-21 15:27:30');
INSERT INTO `booking` VALUES (29, 1, 1, 0, 5, '1', '2020-07-17', '2020-07-17', 0, '1', 0, NULL, '2020-07-21 15:27:30', '2020-07-21 15:27:30');
INSERT INTO `booking` VALUES (30, 1, 1, 0, 5, '1', '2020-07-20', '2020-07-20', 0, '1', 0, NULL, '2020-07-21 15:27:30', '2020-07-21 15:27:30');
INSERT INTO `booking` VALUES (32, 1, 1, 0, 5, '1', '2020-07-27', '2020-07-27', 0, '1', 0, '0000-00-00', '2020-07-22 10:27:06', '2020-07-22 10:27:06');

-- ----------------------------
-- Table structure for booking_detail
-- ----------------------------
DROP TABLE IF EXISTS `booking_detail`;
CREATE TABLE `booking_detail`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_booking` int(255) NULL DEFAULT NULL,
  `id_seat` int(255) NULL DEFAULT NULL,
  `id_member` int(255) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of booking_detail
-- ----------------------------
INSERT INTO `booking_detail` VALUES (2, 15, 44, NULL, '0');
INSERT INTO `booking_detail` VALUES (3, 15, 45, NULL, '0');
INSERT INTO `booking_detail` VALUES (4, 16, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (5, 17, 144, NULL, '0');
INSERT INTO `booking_detail` VALUES (6, 18, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (7, 19, 45, NULL, '0');
INSERT INTO `booking_detail` VALUES (8, 20, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (9, 20, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (10, 21, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (11, 21, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (12, 22, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (13, 22, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (14, 23, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (15, 23, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (16, 24, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (17, 24, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (18, 25, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (19, 25, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (20, 26, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (21, 26, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (22, 27, 46, NULL, '0');
INSERT INTO `booking_detail` VALUES (23, 27, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (24, 28, 106, NULL, '0');
INSERT INTO `booking_detail` VALUES (25, 28, 107, NULL, '0');
INSERT INTO `booking_detail` VALUES (26, 29, 47, NULL, '0');
INSERT INTO `booking_detail` VALUES (27, 29, 48, NULL, '0');
INSERT INTO `booking_detail` VALUES (28, 30, 44, NULL, '0');
INSERT INTO `booking_detail` VALUES (29, 30, 45, NULL, '0');
INSERT INTO `booking_detail` VALUES (30, 32, 83, 1, '0');

SET FOREIGN_KEY_CHECKS = 1;
