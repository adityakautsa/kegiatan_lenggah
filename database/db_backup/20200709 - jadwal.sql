-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.37 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ticket_laravel
CREATE DATABASE IF NOT EXISTS `ticket_laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ticket_laravel`;

-- Dumping structure for table ticket_laravel.jadwal
CREATE TABLE IF NOT EXISTS `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_location` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jam_mulai` varchar(255) DEFAULT NULL,
  `jam_selesai` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `is_aktif` int(2) DEFAULT '1' COMMENT '0 -> tdk aktif\r\n1 -> aktif\r\n',
  `jenis_jadwal` int(2) DEFAULT NULL COMMENT '0 -> rutin 1 -> khusus',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jadwal`),
  KEY `fk_jadwal_lokasi` (`id_location`),
  KEY `fk_jadwal_ruangan` (`id_room`),
  CONSTRAINT `fk_jadwal_lokasi` FOREIGN KEY (`id_location`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_jadwal_ruangan` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table ticket_laravel.jadwal: ~2 rows (approximately)
DELETE FROM `jadwal`;
/*!40000 ALTER TABLE `jadwal` DISABLE KEYS */;
INSERT INTO `jadwal` (`id_jadwal`, `id_location`, `id_room`, `tanggal`, `jam_mulai`, `jam_selesai`, `keterangan`, `is_aktif`, `jenis_jadwal`, `created_at`, `updated_at`) VALUES
	(2, 1, 1, '2020-07-09', '08:00', '10:00', '-', 1, 0, '2020-07-08 15:38:06', '2020-07-08 15:38:06'),
	(3, 2, 2, '2020-07-09', '10:00', '12:00', '-', 1, 0, '2020-07-08 15:38:49', '2020-07-08 15:38:49');
/*!40000 ALTER TABLE `jadwal` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
