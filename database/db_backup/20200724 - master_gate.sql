-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.6.37 - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table ticket_laravel.master_gate
DROP TABLE IF EXISTS `master_gate`;
CREATE TABLE IF NOT EXISTS `master_gate` (
  `id_gate` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_gate`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel ticket_laravel.master_gate: ~3 rows (lebih kurang)
DELETE FROM `master_gate`;
/*!40000 ALTER TABLE `master_gate` DISABLE KEYS */;
INSERT INTO `master_gate` (`id_gate`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'Dalam Gereja', '2020-07-24 14:41:10', '2020-07-24 14:41:10'),
	(2, 'Balai Bawah', '2020-07-24 14:41:20', '2020-07-24 14:41:20'),
	(3, 'Balai Atas', '2020-07-24 14:41:26', '2020-07-24 14:41:26');
/*!40000 ALTER TABLE `master_gate` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
