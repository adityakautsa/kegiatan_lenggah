-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.6.37 - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               11.0.0.6044
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table ticket_laravel.master_menu
DROP TABLE IF EXISTS `master_menu`;
CREATE TABLE IF NOT EXISTS `master_menu` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent` int(3) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `tipe_site` int(10) DEFAULT NULL,
  `urutan` smallint(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Membuang data untuk tabel ticket_laravel.master_menu: ~17 rows (lebih kurang)
DELETE FROM `master_menu`;
/*!40000 ALTER TABLE `master_menu` DISABLE KEYS */;
INSERT INTO `master_menu` (`id`, `url`, `name`, `parent`, `icon`, `tipe_site`, `urutan`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 'home', 'Dashboard', 0, 'ion-ios-home', 1, 1, '2020-06-12 20:19:50', NULL, '2020-07-19 10:39:54', NULL),
	(2, '', 'User & Device Management', 0, 'ion-ios-people', 1, 5, '2020-06-12 20:20:00', NULL, '2020-07-19 10:38:25', NULL),
	(4, 'menu', 'Menu', 2, '', 1, 3, '2020-06-12 20:21:48', NULL, '2020-07-19 08:22:40', NULL),
	(5, 'user', 'User', 2, '', 1, 1, '2020-06-12 20:21:48', NULL, '2020-07-19 08:22:17', NULL),
	(6, 'grup', 'Group', 2, '', 1, 4, '2020-06-12 20:21:48', NULL, '2020-07-19 08:22:42', NULL),
	(8, '#', 'Mapping user - locations', 2, '', 1, 2, '2020-07-01 21:12:45', NULL, '2020-07-19 08:22:19', NULL),
	(14, '#', 'Kegiatan', 0, 'icon fa-map-marker', 1, 4, '2020-07-02 09:43:19', NULL, '2020-07-19 13:53:58', NULL),
	(15, 'booking_scan', 'Scan QR', 14, '', 1, 1, '2020-07-02 09:44:33', NULL, '2020-07-19 10:42:05', NULL),
	(16, 'lihat_show', 'Jadwal', 14, '', 1, 2, '2020-07-02 09:49:29', NULL, '2020-07-19 13:54:06', NULL),
	(18, '#', 'Pesan', 0, 'icon fa-cubes', 1, 3, '2020-07-02 12:47:01', NULL, '2020-07-19 13:53:29', NULL),
	(19, 'booking', 'Pesan', 18, '', 1, 1, '2020-07-02 12:47:44', NULL, '2020-07-19 13:53:19', NULL),
	(22, '#', 'Master', 0, 'fa-database', 1, 2, '2020-07-19 08:14:58', NULL, '2020-07-19 08:15:21', NULL),
	(23, 'lokasi', 'Gereja', 22, NULL, 1, 1, '2020-07-19 08:18:53', NULL, '2020-07-19 08:19:03', NULL),
	(24, 'ruang', 'Ruang', 22, NULL, 1, 2, '2020-07-19 08:19:22', NULL, '2020-07-19 08:19:22', NULL),
	(25, 'area', 'Area', 22, NULL, 1, 3, '2020-07-19 08:19:39', NULL, '2020-07-19 08:19:39', NULL),
	(26, 'seat', 'Seats', 22, NULL, 1, 4, '2020-07-19 08:19:55', NULL, '2020-07-19 08:19:55', NULL),
	(27, 'jadwal', 'Jadwal', 22, NULL, 1, 5, '2020-07-19 08:20:14', NULL, '2020-07-19 08:20:14', NULL),
	(28, 'member', 'Member', 22, NULL, 1, 6, '2020-07-19 08:20:29', NULL, '2020-07-19 08:20:29', NULL),
	(29, 'riwayat', 'Riwayat Pesan', 18, NULL, 1, 2, '2020-07-19 08:21:18', NULL, '2020-07-19 08:21:18', NULL);
/*!40000 ALTER TABLE `master_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
