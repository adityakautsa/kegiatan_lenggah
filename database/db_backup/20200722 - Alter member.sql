ALTER TABLE `member`
	ADD COLUMN `foto` VARCHAR(50) NOT NULL DEFAULT '' AFTER `id_lokasi`;

ALTER TABLE `member`
	ADD COLUMN `jenis_daftar` INT NOT NULL DEFAULT 1 COMMENT '0 -> daftar sendiri' AFTER `foto`;