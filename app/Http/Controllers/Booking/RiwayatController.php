<?php

namespace App\Http\Controllers\Booking;
// require __DIR__ . '/vendor/autoload.php';
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


class RiwayatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('contents.booking.riwayat');
    }

    public function get_data(){
    	$id_member = Auth::user()->id_member;
    	$where[] = "m.id = '".$id_member."' ";

    	$arr['where'] = " WHERE ".implode(" AND ", $where);
    	// $arr['order_by'] = "ORDER BY b.tgl_booking DESC";
        $arr['order_by'] = "ORDER BY b.tgl_show DESC";
    	$arr['group_by'] = "GROUP BY b.id_booking, b.id_member";
    	$d_data = get_riwayat_pesanan($arr);
        
    	$arr = array();
    	foreach ($d_data as $d) {
    		// $d->tgl_show_format = tgl_full($d->tgl_show, '98');
            $d->tgl_show_format = tgl_full($d->tgl_show.' '.$d->jam_mulai, '9');
    		$d->tgl_booking_format = tgl_full($d->created_at, '9');
            $d->jumlah = format_angka($d->jumlah_kursi);
            $d->kursi = $this->get_data_kursi($d->id_booking);
    		$d->aksi = "<div class='btn-group' role='group'><a href='#' onclick='show(".$d->id_booking.")' class='btn btn-icon btn-info' type='button'><i class='fa fa-eye'></i></a></div>";
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi'])
        ->make(true);
    }

     public function get_data_kursi($id){
        $where = '';
        if(isset($id)){
            $where = "WHERE bd.id_booking='".$id."'";
        }
        $d_data = DB::select("SELECT bd.*, s.kolom as kolom, a.nama as nama_area, mg.nama as nama_gate FROM booking_detail AS bd JOIN seats AS s ON bd.id_seat=s.id JOIN area AS a ON s.area=a.id JOIN master_gate AS mg ON a.id_gate=mg.id_gate $where");

        $d_kursi = array();
        $d_gate = '';
        $d_area = '';
        foreach($d_data as $d){
            $d_kursi[] = trim(strtolower($d->nama_area),'area ').$d->kolom;
            $d_gate = $d->nama_gate;
            $d_area = $d->nama_area;
        }
        $kursi = '';
        if(count($d_kursi)>0){
        $kursi = $d_gate.' - '.$d_area.' ('.implode(',',$d_kursi).')';
        }

        $arr = $kursi;
        return $arr;
     }

    public function get_ticket(Request $request){
        $id = $request->get('id');

        $d_data = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('locations as l','j.id_location','l.id')->where(['id_booking'=>$id])->select(DB::raw('b.*,m.nama as nama_ktp, m.nik as no_ktp, m.no_telp as no_hp, j.nama as nama_jadwal, b.tgl_show AS waktu, j.jam_mulai as jam, l.nama as nama_lokasi, j.jenis_jadwal'))->first();
        $data['data'] = $d_data;

        $d_detail = DB::table('booking_detail as b')->join('seats as s','b.id_seat','s.id')->leftjoin('member as m','b.id_member','m.id')->join('area as a','s.area','a.id')->join('master_gate as mg','a.id_gate','mg.id_gate')->where(['b.id_booking'=>$d_data->id_booking])->select(DB::raw('b.*,s.row as baris,s.kolom as kolom,m.nama as nama_ktp, m.nik as no_ktp, a.nama as nama_area, mg.nama as nama_gate'));
        $arr = array();
        if($d_detail->count() > 0){
            foreach ($d_detail->get() as $key => $value) {
                # code...
                $arr[] = '<tr><td width="20%">'.$value->nama_area.' - '.trim(strtolower($value->nama_area),'area ').$value->kolom.'</td><td width="80%" class="text-left" style="padding-left: 5px;">'.$value->nama_ktp.'</td></tr>';
            }
        }

        $data['detail'] = $arr;
        $data['gate'] = $d_detail->first()->nama_gate;
        $data['waktu'] = tgl_full($d_data->waktu,'1').' '.tgl_full($d_data->jam,'100');

        return response()->json($data);
    }

    public function download2($id){
        $url = $_SERVER['HTTP_HOST'];        
        require(public_path((($url=="localhost" || $url=="127.0.0.1") ? 'fpdf1813\Mc_table.php' : 'fpdf1813/Mc_table.php')));
        
        // require(public_path('fpdf1813\Mc_table.php'));
        $d_data = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('locations as l','j.id_location','l.id')->where(['id_booking'=>$id])->select(DB::raw('b.*,m.nama as nama_ktp, m.nik as no_ktp, m.no_telp as no_hp, j.nama as nama_jadwal, b.tgl_show AS waktu, j.jam_mulai as jam, l.nama as nama_lokasi, j.jenis_jadwal'))->first();        
        $d_detail = DB::table('booking_detail as b')->join('seats as s','b.id_seat','s.id')->leftjoin('member as m','b.id_member','m.id')->join('area as a','s.area','a.id')->join('master_gate as mg','a.id_gate','mg.id_gate')->where(['b.id_booking'=>$d_data->id_booking])->select(DB::raw('b.*,s.row as baris,s.kolom as kolom,m.nama as nama_ktp, m.nik as no_ktp,  a.nama as nama_area, mg.nama as nama_gate'));
        $data['data']   = $d_data;
        $data['detail'] = $d_detail->get();
        $data['id']     = $id;
        $data['url']    = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$id.'&choe=UTF-8';
        $data['gate']   = $d_detail->first()->nama_gate;
        $data['waktu']  = tgl_full($d_data->waktu,'1').' '.tgl_full($d_data->jam,'100');

        $barcode = '<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=http%3A%2F%2Fwww.google.com%2F&choe=UTF-8" title="Link to Google.com" />';
        // print_r($barcode);exit();

        trigger_log($id, 'booking', 'Riwayat', 'Download Tiket waktu acara '.tgl_full($d_data->waktu,'0')." ".tgl_full($d_data->jam,'100'),5,1);    

        $html = view('contents.booking.cetak_pdf')->with('data',$data);
        return response($html)->header('Content-Type', 'application/pdf');
    }

    public function cetak($id){
        // $connector = new FilePrintConnector("php://stdout");
        $connector = new WindowsPrintConnector("RPP02N");
        $printer = new Printer($connector);
        // dd($printer);
        $printer -> text("Hello World!\n");
        $printer -> cut();
        $printer -> close();

    }

    public function historylog(Request $request){
        $id = $request->id;
        $d_data = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('locations as l','j.id_location','l.id')->where(['id_booking'=>$id])->select(DB::raw('b.*,m.nama as nama_ktp, m.nik as no_ktp, m.no_telp as no_hp, j.nama as nama_jadwal, b.tgl_show AS waktu, j.jam_mulai as jam, l.nama as nama_lokasi, j.jenis_jadwal'))->first();  
        trigger_log($id, 'booking', 'Cetak', 'Cetak Tiket waktu acara '.tgl_full($d_data->waktu,'0')." ".tgl_full($d_data->jam,'100'),7,1); 
    }
    
}