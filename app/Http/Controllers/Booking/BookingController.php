<?php

namespace App\Http\Controllers\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;

class BookingController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $auth = Auth::user();
    	$data['auth'] =  $auth;
        $member = DB::table('member')->where('id','=',$auth->id_member)->first();
        $data['member'] = DB::table('member as m')->leftjoin('locations as l','m.id_lokasi','l.id')->leftjoin('locations as l2','m.id_wilayah','l2.id')->leftjoin('locations as l3','m.id_lingkungan','l3.id')->where(['m.id'=>$auth->id_member])->select('m.*','l.nama as nama_lokasi','l2.nama as nama_wilayah','l3.nama as nama_lingkungan')->first();
    	$data['lokasi'] = DB::select("select * from locations where is_aktif='1'");                
        // $data['area'] = DB::table('area as a')->leftjoin('locations as l','a.id_lokasi','l.id')->join('master_gate as mg','a.id_gate','mg.id_gate')->where(['l.id'=>$auth->id_lokasi])->select(DB::raw('a.*, mg.nama as nama_gate'))->get();
        /*$data['area'] = DB::table('area as a')->leftjoin('locations as l','a.id_lokasi','l.id')
        ->join('master_gate as mg','a.id_gate','mg.id_gate')
        ->join('seats as s', function ($join) {
            $join->on('a.id', '=', 's.area')
                 ->where('s.is_aktif', '=', 1);
        })
        ->where(['l.id'=>$auth->id_lokasi])        
        ->groupby('a.id')
        ->select(DB::raw('a.*, mg.nama as nama_gate, (SUM(s.area)) AS jumlah_kursi'))->get();*/
        $datenow = date('Y-m-d');
        $data['area'] = DB::SELECT("
            SELECT a.*, mg.nama as nama_gate,
            CASE WHEN t.jum IS NULL THEN 0 ELSE t.jum END AS jumlah_booking,
            CASE WHEN k.jum IS NULL THEN 0 ELSE k.jum END AS jumlah_kursi
            FROM area as a 
            LEFT JOIN (
            SELECT s.area, count(*) as jum FROM seats as s where s.is_aktif='1' GROUP BY s.area
            ) as k ON a.id=k.area
            LEFT JOIN (
            SELECT s.area, count(*) as jum FROM booking AS b JOIN booking_detail AS bd ON b.id_booking=bd.id_booking JOIN seats AS s ON bd.id_seat=s.id
            where b.tgl_show='$datenow'
            GROUP BY s.area) as t ON a.id=t.area
            LEFT JOIN locations as l ON a.id_lokasi=l.id
            LEFT JOIN master_gate as mg ON a.id_gate=mg.id_gate
            WHERE a.id_lokasi='$auth->id_lokasi'
            ");
        $data['rangetanggal'] = DB::table('master_rangetanggal')->offset(0)->limit(1)->first()->range_tanggal;
        
    	return view('contents.booking.index')->with('data',$data);
    }

    public function get_data_lokasi($id_lokasi){
        $cek_paroki = DB::SELECT("SELECT C.* FROM (
                            SELECT A.id, A.nama as paroki, '' as wilayah, '' as lingkungan FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            UNION ALL
                            SELECT l.id, A.nama as paroki, l.nama as wilayah, '' as lingkungan FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            LEFT JOIN locations AS l ON A.id=l.parents
                            UNION ALL
                            SELECT l.id, B.paroki as paroki, B.nama as wilayah, l.nama as lingkungan FROM (
                            SELECT A.nama as paroki, l.* FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            LEFT JOIN locations AS l ON A.id=l.parents
                            ) AS B
                            JOIN locations AS l ON B.id=l.parents
                            ) AS C WHERE C.id='".$id_lokasi."' ORDER BY paroki, wilayah, lingkungan");
        
        if(count($cek_paroki) > 0){
            foreach ($cek_paroki as $key => $value) {
                # code...
                $arr = array('id'=>$value->id,
                             'paroki'=>$value->paroki,
                             'wilayah'=>$value->wilayah,
                             'lingkungan'=>$value->lingkungan);
            }
        }else{
            $arr = array('id'=>'',
                         'paroki'=>'',
                         'wilayah'=>'',
                         'lingkungan'=>'');
        }

        return $arr;
    }

    public function index2(){
        $auth = Auth::user();
        $data['auth'] =  $auth;
        $data['gereja'] = DB::table('locations')->where(['id'=>$auth->id_lokasi])->first();
        $data['member'] = DB::table('member')->where(['id'=>$auth->id_member])->first();
        $data['lokasi'] = DB::select("select * from locations where is_aktif='1'");
        $data['area'] = DB::table('area as a')->join('rooms as r','a.id_rooms','r.id')->join('locations as l','r.id_locations','l.id')->where(['l.id'=>$auth->id_lokasi])->select(DB::raw('a.*'))->get(); 
        return view('contents.booking.index')->with('data',$data);
    }

    public function seats(Request $request){
    	$id = $request['id'];
        $jadwal = $request['jadwal'];
        $tanggal = $request['tanggal'];
    	$where = "where area = '".$id."'";
    	$where2 = "where area = '".$id."' AND row IS NOT NULL AND kolom IS NOT NULL";
    	$d_column = DB::SELECT("SELECT deret, kolom FROM seats $where GROUP BY deret, kolom ORDER BY kolom asc");
        $d_row = DB::SELECT("SELECT row, baris  FROM seats $where GROUP BY row ORDER BY kolom asc");
    	$column = array();
    	foreach ($d_column as $key => $value) {
    		# code...
    		$column[$value->deret] = $value->kolom;
    	}
    	$col = implode(',',$column);
    	$y['column'] =explode(",", $col) ; 

        $row = array();
        foreach ($d_row as $key => $value) {
            # code...
            $row[$value->baris] = $value->row;
        }
        $ro = implode(',',$row);
        $y['rows'] =explode(",", $ro);

    	$d_map = DB::SELECT("SELECT * FROM seats $where ORDER BY deret asc");
    	$map = array();
    	foreach ($d_map as $key => $value) {
    		# code...
    		$map[$value->baris][] = ($value->kolom==null)?'_':'e';
    	}
    	$aa = array();
    	for($i=1; $i<=count($map); $i++){
    		$aa[] = implode('',$map[$i]);
    	}
    	$ma = implode(',',$aa);
    	$y['map'] = explode(",", $ma);

    	$y['seats'] = DB::SELECT("SELECT *, CONCAT(row,'_',kolom) as seats FROM seats $where2");

        $d_jum = DB::SELECT("SELECT COUNT('*') as jum FROM `seats` $where  GROUP BY baris ORDER BY COUNT(*) DESC limit 1");
        if(count($d_jum) > 0){
            $jum_kolom = ((int)$d_jum[0]->jum>10)?(int)((($d_jum[0]->jum/10)*400)-(($d_jum[0]->jum/10)*25)):(int)(($d_jum[0]->jum/10)*400);
        }else{
            $jum_kolom = 0;
        }
        $y['jum_kolom'] = $jum_kolom;

        $d_booking = DB::SELECT("SELECT bd.*, CONCAT(s.row,'_',s.kolom) AS kursi FROM booking AS b JOIN booking_detail AS bd ON b.id_booking=bd.id_booking JOIN seats AS s ON bd.id_seat=s.id where b.id_jadwal='".$jadwal."' AND b.tgl_show='".tgl_full($tanggal,'99')."' AND s.area='".$id."' GROUP BY s.row, s.kolom");
        $booking = array();
        foreach ($d_booking as $key => $value) {
            $booking[] = $value->kursi;
        }
        $boo = implode(',', $booking);
        $y['booking'] = explode(",", $boo);

        $d_area = DB::table('area')->where('id','=',$id)->first()->nama;
        $y['area'] = trim(strtolower($d_area),'area ');

    	return response()->json($y);
    }

    public function get_jadwal(Request $request){
        $id_member = $request['id_member'];
        $id_lokasi = $request['id_lokasi'];
        $tanggal = tgl_full($request['tanggal'],'99');
        $waktu = $request['waktu'];
        $data = DB::table('jadwal')->where(['id_location'=>$id_lokasi,'tanggal'=>$tanggal,'jam_mulai'=>$waktu,'is_aktif'=>'1'])->Select(DB::raw('*'))->get();
        $arr = array();
        foreach ($data as $key => $value) {
            # code...
            $arr[] = array('id_jadwal'=>$value->id_jadwal,
                            'tanggal'=>tgl_full($value->tanggal,''),
                            'jam_mulai'=>$value->jam_mulai,
                            'jam_selesai'=>$value->jam_selesai);
        }
        echo json_encode($arr);
    }

    public function get_member(){
        $d_data = DB::table('member')->where(['is_aktif'=>1])->SELECT(DB::raw('*'))->get();
        $arr = array();
        foreach ($d_data as $key => $value) {
            # code...
            $arr[] = array('id_member'=>$value->id,
                            'nik_member'=>$value->nik,
                            'nama_member'=>$value->nama);
        }

        echo json_encode($arr);
    }

    public function get_jadwal_tanggal(Request $request){
        date_default_timezone_set("Asia/Bangkok");
        $id_lokasi = $request['id_lokasi'];
        $tanggal = tgl_full($request['tanggal'],'99');
        $hari = $request['hari'];

        $date = date('Y-m-d');
        $hours = date('H:i');
        if($tanggal == $date){
        $data = DB::SELECT("SELECT id_jadwal, tanggal, jam_mulai as jam FROM (
                    select * from jadwal where tanggal = '".$tanggal."' AND jam_mulai > '".$hours."' AND jenis_jadwal='0'
                    UNION ALL
                    select * from jadwal where hari = '".$hari."' AND jam_mulai > '".$hours."' AND jenis_jadwal='1'
                    ) as a ORDER BY jam_mulai ");
        }else{
        $data = DB::SELECT("SELECT id_jadwal, tanggal, jam_mulai as jam FROM (
                    select * from jadwal where tanggal = '".$tanggal."' and jenis_jadwal='0'
                    UNION ALL
                    select * from jadwal where hari = '".$hari."' and jenis_jadwal='1'
                    ) as a ORDER BY jam_mulai ");    
        }

        $arr = array();
        foreach ($data as $key => $value) {
            # code...
            $arr[] = array('id_jadwal'=>$value->id_jadwal,
                           'tanggal'=>tgl_full($request['tanggal'],''),
                           'jam'=>$value->jam);
            
        }

        echo json_encode($arr);
    }

    public function get_jadwal_jam(Request $request){
        $id = $request['id'];
        $data = DB::table('jadwal')->where(['id_jadwal'=>$id,'is_aktif'=>'1'])->Select(DB::raw('*'))->get();
        $arr = array();
        foreach ($data as $key => $value) {
            # code...
            $arr[] = array('id_jadwal'=>$value->id_jadwal,
                           'tanggal'=>tgl_full($request['tanggal'],''),
                           'jam'=>$value->jam_mulai,
                           'jenis'=>$value->jenis_jadwal,
                           'area'=>$value->id_area,
                           'id_lokasi'=>$value->id_location);
        }
        echo json_encode($arr);
    }

    public function get_area(Request $request){
        // $area = explode(',',$request->get('area'));
        $area = $request->get('area');
        $date = tgl_full($request->get('tanggal'),'99');

        // $d_data = DB::table('area')->whereIn('id',$area)->get();
        $d_data = DB::SELECT("
            SELECT a.*, mg.nama as nama_gate,
            CASE WHEN t.jum IS NULL THEN 0 ELSE t.jum END AS jumlah_booking,
            CASE WHEN k.jum IS NULL THEN 0 ELSE k.jum END AS jumlah_kursi
            FROM area as a 
            LEFT JOIN (
            SELECT s.area, count(*) as jum FROM seats as s where s.is_aktif='1' GROUP BY s.area
            ) as k ON a.id=k.area
            LEFT JOIN (
            SELECT s.area, count(*) as jum FROM booking AS b JOIN booking_detail AS bd ON b.id_booking=bd.id_booking JOIN seats AS s ON bd.id_seat=s.id
            where b.tgl_show='$date'
            GROUP BY s.area) as t ON a.id=t.area
            LEFT JOIN locations as l ON a.id_lokasi=l.id
            LEFT JOIN master_gate as mg ON a.id_gate=mg.id_gate
            WHERE a.id IN ($area)
            ");
        $arr = array();
        foreach ($d_data as $key => $value) {
            # code...
            $arr[] = array('id_area'=>$value->id,
                          'nama_area'=>$value->nama,
                          'foto'=>$value->foto,
                          'jumlah_kursi'=>($value->jumlah_kursi-$value->jumlah_booking));
        }

        echo json_encode($arr);
    }

    public function simpan(Request $request){
        $input = $request->all();
        
        DB::beginTransaction();
        try {
        $table_kursi = $input['kursi'];

        $data['id_member'] = $input['txt_idmember'];
        $data['id_location'] = $input['txt_idlocation'];
        $data['id_jadwal'] = $input['txt_jadwal'];
        $data['tgl_booking'] = date('Y-m-d');
        $data['tgl_show'] = tgl_full($input['txt_tgljadwal'],'99');
        $data['status'] = 0;
        $data['is_show'] = 1;
        $data['id_seat'] = 0;
        $data['kategori_jadwal'] = $input['txt_jenisjadwal'];
        $data['status_covid'] = $input['covid'];
        $data['tgl_sembuhcovid'] = tgl_full($input['tanggal_covid'],'99');

        $c_kursi = implode(',',$table_kursi);
        // $cek = DB::table('booking as b')->join('booking_detail as bd','b.id_booking','b.id_booking')->where(['b.id_jadwal'=>$input['txt_jadwal'],'b.id_location'=>$input['txt_idlocation']])->whereIn('bd.id_seat',[$c_kursi]);
        $cek =  DB::SELECT("SELECT * FROM booking as b
                join booking_detail as bd ON b.id_booking=bd.id_booking 
                where (b.id_jadwal = '".$input['txt_jadwal']."' AND b.tgl_show='".tgl_full($input['txt_tgljadwal'],'99')."' AND b.id_location = '".$input['txt_idlocation']."') And bd.id_seat IN ($c_kursi)");
        // $cek_max = DB::table('booking as b')->join('booking_detail as bd','b.id_booking','bd.id_booking')
        //         ->where(['b.id_jadwal'=>$input['txt_jadwal'],'tgl_show'=>tgl_full($input['txt_tgljadwal'],'99'),'b.id_location'=>$input['txt_idlocation'],'b.id_member'=>$input['txt_idmember']])->groupby('bd.id_seat');
        $cek_max = DB::SELECT("SELECT COUNT(*) as jum FROM (SELECT bd.* FROM booking as b
                join booking_detail as bd ON b.id_booking=bd.id_booking 
                where (b.id_jadwal = '".$input['txt_jadwal']."' AND b.tgl_show='".tgl_full($input['txt_tgljadwal'],'99')."' AND b.id_location = '".$input['txt_idlocation']."' And bd.id_member='".$input['txt_idmember']."') group by bd.id_seat ) as c");
        
        if(($cek_max[0]->jum+count($input['kursi'])) > 5){
            $status = '3';
            $id="0"; 
            $tgl_booking = '';
            $tgl_show = '';
            
        }else{
        if(count($cek) > 0){
            DB::rollBack();
            $status = '2';
            $id="0"; 
            $tgl_booking = '';
            $tgl_show = '';
        }else{
            $id = DB::table('booking')->insertGetId($data);
            $tr_member = DB::table('member')->where('id','=',Auth::user()->id_member)->first()->nama;
            foreach ($table_kursi as $key => $value) {
                # code...
                $detail['id_booking'] = $id;
                $detail['id_seat'] = $input['kursi'][$key];
                $detail['id_member'] = $input['member'][$key];
                $detail['status'] = 0;
                DB::table('booking_detail')->insert($detail);
            }
                DB::commit(); 
                $status = '1';
            $tgl_booking = tgl_full(date('d-M-Y'),'0');
            $tgl_show = tgl_full($input['txt_tgljadwal'],'0');
        }
        }
        }catch (Exception $e) {
            DB::rollBack();
            $status = '0'; 
            $tgl_booking = '';
            $tgl_show = '';
        }
        return response()->json(array('status'=>$status,'id'=>$id, 'tgl_booking'=>$tgl_booking,'tgl_show'=>$tgl_show));

    }
}
