<?php

namespace App\Http\Controllers\booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;

class ScanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($selected=''){
        date_default_timezone_set("Asia/Bangkok");
    	$data['member'] = DB::table('member')->SELECT(DB::raw('*'))->get();
        $data['gate'] = DB::table('master_gate')->select(DB::raw('*'))->get();
        $data['selected'] = $selected;

        $datenow = date('Y-m-d');
        $member = Auth::user()->id_member;

        $d_jadwal = get_show()->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('".$datenow."', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$datenow."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$datenow."', '%Y-%m-%d')) - 1) END")->orderby("j.jam_mulai", "asc");
        $c_jadwal = array();
        foreach ($d_jadwal->get() as $key => $value) {
            # code...
            $c_jadwal[] = $value->id_jadwal;
        }
        $jadwal = implode(',',$c_jadwal);
        $cek_jadwal = DB::table('jadwal_petugas as jm')->join('jadwal as j','jm.id_jadwal','j.id_jadwal')->join('member as m','jm.id_petugas','m.id')->where('jm.tanggal',tgl_full($datenow,'99'))->whereIn('jm.id_petugas',[$member])->whereIn('jm.id_jadwal',$c_jadwal)->get();
        
        $data['tombol'] = ($cek_jadwal->count() == 0)?'0':'1';

    	return view('contents.booking.show')->with('data',$data);
    }

    public function show($id_booking,$id_gate){
        date_default_timezone_set("Asia/Bangkok");
        $data['data'] = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->leftjoin('locations as l','j.id_location','l.id')->leftjoin('locations as l2','m.id_wilayah','l2.id')->leftjoin('locations as l3','m.id_lingkungan','l3.id')->where(['b.id_booking'=>$id_booking])->select(DB::raw('b.*,m.nama as nama_ktp, m.alamat as alamat, m.nik as no_ktp, m.no_telp as no_hp, j.nama as nama_jadwal, b.tgl_show AS waktu, j.jam_mulai as jam, CASE WHEN l.nama IS NULL THEN m.id_lokasi ELSE l.nama END as nama_lokasi, l2.nama as nama_wilayah, l3.nama as nama_lingkungan, j.jenis_jadwal as status_jadwal'))->first();
        $d_detail = DB::table('booking_detail as b')->join('seats as s','b.id_seat','s.id')->join('area as a','s.area','a.id')->leftjoin('member as m','b.id_member','m.id')->where(['b.id_booking'=>$data['data']->id_booking])->select(DB::raw('b.*,s.row as baris,s.kolom as kolom,m.nama as nama_ktp, m.nik as no_ktp, a.nama as nama_area, a.id_gate as id_gate'));
        $data['detail'] = $d_detail->get();
        $data['gate'] = DB::table('master_gate')->where('id_gate','=',$d_detail->first()->id_gate)->first();
        $data['id'] = $id_booking;

        $datenow = date('d-m-Y');
        $timenow = date('H:i');
        $timenow_after = date('H:i', strtotime("+15 minutes",strtotime($timenow)));

        $member = Auth::user()->id_member;
        $cek_jadwal = DB::table('jadwal_petugas as jm')->join('jadwal as j','jm.id_jadwal','j.id_jadwal')->join('member as m','jm.id_petugas','m.id')->where(['jm.id_jadwal'=>$data['data']->id_jadwal,'jm.tanggal'=>tgl_full($datenow,'99')])->whereIn('jm.id_petugas',[$member]);

        //print_r(tgl_full($data['data']->jam,'100').'/'.$timenow_after);exit();
        if($cek_jadwal->count() > 0){            
        if($id_gate==$d_detail->first()->id_gate){
            if($datenow == tgl_full($data['data']->waktu,'')){
                if($timenow_after >= tgl_full($data['data']->jam,'100')){
                    $data['status']['title'] = "Tiket Kadaluarsa!";
                    $data['status']['text']  = "Tiket sudah tidak bisa dipakai";
                    $data['status']['type']  = "error";
                    $data['status']['id']    = "0";
                }else{
                    $data['status']['title'] = "";
                    $data['status']['text']  = "";
                    $data['status']['type']  = "";
                    $data['status']['id']    = "1";
                }
            }else{
                $data['status']['title'] = "Tanggal Tidak Sesuai!";
                $data['status']['text']  = "Tanggal Tiket tidak sesuai dengan tanggal sekarang";
                $data['status']['type']  = "error";
                $data['status']['id']    = "0";
            }
        }else{
            $data['status']['title'] = "Data Error!";
            $data['status']['text']  = "Tidak ada di Gate ini, Silahkan Pilih Gate yang benar";
            $data['status']['type']  = "error";
            $data['status']['id']    = "0";
        }
        }else{
            $data['status']['title'] = "Petugas Salah!";
            $data['status']['text']  = "Anda Bukan Petugas Scan untuk jadwal hari ini";
            $data['status']['type']  = "error";
            $data['status']['id']    = "0";
        }
        return view('contents.booking.scan_show')->with('data',$data);
    }


    public function get_data(Request $request){
    	$id = $request->get('id');
        $gate = $request->get("gate"); 
        // $id = 37;
        // $gate = 44;
    	$d_data = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('locations as l','m.id_lokasi','l.id')->leftjoin('locations as l2','m.id_wilayah','l2.id')->leftjoin('locations as l3','m.id_lingkungan','l3.id')->where(['id_booking'=>$id])->select(DB::raw('b.*,m.nama as nama_ktp, m.alamat as alamat, m.nik as no_ktp, m.no_telp as no_telp, j.nama as nama_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN j.hari ELSE j.tanggal END AS waktu, j.jam_mulai as jam, l.nama as nama_lokasi, l2.nama as nama_wilayah, l3.nama as nama_lingkungan, j.jenis_jadwal as status_jadwal'));
        
    	if($d_data->count() > 0){
    		foreach ($d_data->get() as $key => $d) {
    			# code...
                $date = date('Y-m-d');
                $lokasi = $this->get_data_lokasi($d->id_location);
    			$arr['data'][] = array('id_booking'=>$d->id_booking,
    									'no_ktp'=>$d->no_ktp,
    									'nama_ktp'=>$d->nama_ktp,
    									'alamat'=>$d->alamat,
    									'status_covid'=>($d->status_covid==0)?'Tidak':'Ya',
    									'tgl_sembuh'=>tgl_full($d->tgl_sembuhcovid,''),
                                        'paroki'=>$d->nama_lokasi,
                                        'wilayah'=>$d->nama_wilayah,
                                        'lingkungan'=>$d->nama_lingkungan,
                                        'tgl_pesan' =>tgl_full($d->created_at,'7'),
                                        'tgl_show' => ($d->status_jadwal == 0)?tgl_full($d->waktu.' '.$d->jam,'7'):tgl_full($d->waktu." ".$d->jam,'8'),
                                        'no_telp'=>$d->no_telp);
    		}
    		
    	}else{
    		$arr['data'] = array();
    	}

    	$d_detail = DB::table('booking_detail as b')->join('seats as s','b.id_seat','s.id')->join('area as a','s.area','a.id')->leftjoin('member as m','b.id_member','m.id')->where(['b.id_booking'=>$arr['data'][0]['id_booking']])->select(DB::raw('b.*,s.row as baris,s.kolom as kolom,m.nama as nama_ktp, m.nik as no_ktp, a.nama as nama_area'));

    	if($d_detail->count() > 0){
    		foreach ($d_detail->get() as $key => $d) {
    			# code...
    			$arr['detail'][] = array('id'=>$d->id,
    									'id_booking'=>$d->id_booking,
    									'id_seat'=>$d->id_seat,
    									'nama_seat'=>$d->nama_area.' - '.$d->baris.$d->kolom,
    									'id_member'=>$d->id_member,
    									'nama_member'=>($d->id_member==null||$d->id_member=='')?'':$d->nama_ktp.' ('.$d->no_ktp.')',
    									'status'=>$d->status);
    		}
    	}else{
    		$arr['detail'] = array();
    	}

    	$arr['member'] = DB::table('member')->select(DB::raw('*'))->get();

    	return response()->json($arr);

    }

    public function simpan(Request $request){
    	$id = $request->get('id_member');

    	$tabel_id = ($request->get('tabel_idmember')) ? $request->get('tabel_idmember'):[];
    	$tabel_hadir = $request->get('tabel_hadir');
    	$tabel_member = $request->get('tabel_member');
    	// dd($tabel_hadir);        
    	for($i=0;$i<count($tabel_id);$i++){
    		$data['id_member'] = $tabel_member[$i];
    		$data['status'] = (isset($tabel_hadir[$i]) > 0) ? $tabel_hadir[$i]:"0";
    		DB::table('booking_detail')->where(['id'=>$tabel_id[$i]])->update($data);
    	}

    	return response()->json(['status'=>'1']);
    }

    public function simpan2(Request $request){
        $id = $request->get('id_member');

        $tabel_id = ($request->get('tabel_id')) ? $request->get('tabel_id'):[];
        $tabel_hadir = $request->get('tabel_hadir');
        $tabel_member = $request->get('tabel_idmember');
        
        for($i=0;$i<count($tabel_id);$i++){
                $data['id_member'] = $tabel_member[$i];
                $data['status'] = $tabel_hadir[$i];
                DB::table('booking_detail')->where(['id'=>$tabel_id[$i]])->update($data);

            $log = DB::table('booking_detail as bd')->join('booking as b','bd.id_booking','b.id_booking')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('seats as s','bd.id_seat','s.id')->where(['bd.id'=>$tabel_id[$i]])->select(DB::raw('bd.*,concat(b.tgl_show," ",j.jam_mulai) as waktu, concat(s.row,s.kolom) as kursi'))->first();
            $status = ($tabel_hadir[$i] == 0)?'Tidak Hadir':'Hadir';
            trigger_log($id, 'booking_detail', 'Scan', 'Scan Tiket waktu acara '.tgl_full($log->waktu,'9').' dan kursi '.$log->kursi.' ('.$status.')',4,1);          
        }        

        return redirect('booking_scan');
    }

    public function get_data_lokasi($id_lokasi){
        $cek_paroki = DB::SELECT("SELECT C.* FROM (
                            SELECT A.id, A.nama as paroki, '' as wilayah, '' as lingkungan FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            UNION ALL
                            SELECT l.id, A.nama as paroki, l.nama as wilayah, '' as lingkungan FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            LEFT JOIN locations AS l ON A.id=l.parents
                            UNION ALL
                            SELECT l.id, B.paroki as paroki, B.nama as wilayah, l.nama as lingkungan FROM (
                            SELECT A.nama as paroki, l.* FROM (
                            SELECT l.* FROM locations AS l WHERE l.parents=0 
                            ) AS A
                            LEFT JOIN locations AS l ON A.id=l.parents
                            ) AS B
                            JOIN locations AS l ON B.id=l.parents
                            ) AS C WHERE C.id='".$id_lokasi."' ORDER BY paroki, wilayah, lingkungan");
        
        if(count($cek_paroki) > 0){
            foreach ($cek_paroki as $key => $value) {
                # code...
                $arr = array('id'=>$value->id,
                             'paroki'=>$value->paroki,
                             'wilayah'=>$value->wilayah,
                             'lingkungan'=>$value->lingkungan);
            }
        }else{
            $arr = array('id'=>'',
                         'paroki'=>'',
                         'wilayah'=>'',
                         'lingkungan'=>'');
        }

        return $arr;
    }
}
