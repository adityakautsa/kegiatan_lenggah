<?php

namespace App\Http\Controllers\Show;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class VerifikasiController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['prov'] = DB::table("master_prov")->orderBy("name", "asc")->get();
    	return view('contents.show.verifikasi')->with('data', $data);
    }

    function get_data(){
    	$d_data = get_member()->join("user_accounts AS ua", "m.id", "ua.id_member")->where(['is_aktif' => '0'])->select(DB::raw("m.*, ua.id id_user, ua.username, ua.active, CASE WHEN prop.name IS NULL THEN '' ELSE prop.name END AS nama_prov, CASE WHEN kab.name IS NULL THEN '' ELSE kab.name END AS nama_kab, CASE WHEN kec.name IS NULL THEN '' ELSE kec.name END AS nama_kec, CASE WHEN kel.name IS NULL THEN '' ELSE kel.name END AS nama_kel"))->orderby("ua.active", "ASC")->orderby("m.nama", "ASC");
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		if($d->active == 0){
    			$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-success' type='button' data-id='".$d->id."' data-user='".$d->id_user."' onclick='check($(this))'><i class='fa fa-check'></i></button></div>";
    		}else{
    			$d->aksi = '';
    		}
    		$d->tanggal_reg = tgl_full($d->tgl_registrasi, 98);
    		$d->tanggal_aktivasi = ($d->tgl_aktivasi == '0000-00-00') ? '':tgl_full($d->tgl_aktivasi, 98);
    		$d->status = is_aktif($d->active);
    		$d->petugas = is_petugas($d->is_petugas);
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status', 'petugas'])
        ->make(true);
    }

    function get_detail(Request $request){
        $id_member = $request->get('id_member');
        $id_user = $request->get('id_user');

        $d_data = _memberuser()->where(["m.id" => $id_member, "ua.id" => $id_user])->get();
        $count = $d_data->count();

        if($count > 0){
            $d_member = $d_data->first();

            $image = '';
            if($d_member->foto != ''){
                $image = asset('images/member')."/".$d_member->foto;
            }

            $arr['status'] = ["status" => "1"];
            $arr['data'] = $d_member;
            $arr['data']->tgl_registrasi_ = tgl_full($d_member->tgl_registrasi, '1');
            $arr['data']->jenkel = ($d_member->jenis_kelamin == '0') ? "Pria":"Wanita";
            $arr['data']->foto_member = $image;
        }else{
            $arr['status'] = ["status" => "0"];
        }

        return response()->json($arr);
    }

    function check(Request $request){
    	$id = $request->get('id');
    	$user = $request->get('user');

    	DB::table('member')->where('id', $id)->update(["is_aktif" => "1", "tgl_aktivasi" => date("Y-m-d")]);
    	DB::table('user_accounts')->where(['id_member' => $id, "id" => $user])->update(["active" => "1"]);

        /* -- Log -- */
        $get_member = _memberuser()->where("m.id", $id)->get()->first();
        trigger_log($id, "member dan user_accounts", "Verifikasi member ", "Verifikasi Pendaftar untuk member ".$get_member->nama." (".$get_member->id.") dan nomor hp ".$get_member->no_telp.". Dengan user_account id ".$get_member->id_user.", username ".$get_member->username." ", 2, 1);
        /* -- Log -- */
    	echo json_encode(['status' => '1']);
    }
}