<?php

namespace App\Http\Controllers\Show;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class JadwalpetugasController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $tglnow = date("Y-m-d");
        $data['member'] = DB::table('member as m')->join('user_accounts as u','u.id_member','m.id')->where(['u.id_group'=>'4'])->select(DB::raw('*'))->get();

        // $data['jadwal'] = DB::table('jadwal')->where(['tanggal'=>$tglnow])->select(DB::raw('*'))->get();

        $arr = array("Minggu"=>0,"Senin"=>1,"Selasa"=>2,"Rabu"=>3,"Kamis"=>4,"Jumat"=>5,"Sabtu"=>6);
        $tanggal = tgl_full($tglnow,'99');
        $hari = $arr[tgl_full($tglnow,'6')];
        $data['jadwal'] = DB::SELECT("SELECT id_jadwal, tanggal, hari, jam_mulai as jam, jenis_jadwal as status, nama FROM (
                    select * from jadwal where tanggal = '".$tanggal."' and jenis_jadwal='0'
                    UNION ALL
                    select * from jadwal where hari = '".$hari."' and jenis_jadwal='1'
                    ) as a ORDER BY jam_mulai "); 
      
    	return view('contents.show.jadwal_petugas')->with('data',$data);
    }


    public function show(){
    	$d_data = DB::table('jadwal_petugas as p')->join('jadwal as j','p.id_jadwal','j.id_jadwal')->select(DB::raw("p.id, p.tanggal, p.id_petugas, j.nama as jadwal, j.id_jadwal, j.jam_mulai"));
    	$arr = array();
    	foreach ($d_data->get() as $d) {
            $tgl = tgl_full($d->tanggal, 98);

            $id_petugas = '';
            $arr_petugas = array();
            if($d->id_petugas != ''){
                $id_petugas = explode(",", $d->id_petugas);
                $d_petugas = get_member()->whereIn("m.id", $id_petugas)->select("m.nama")->get();
                $c_petugas = $d_petugas->count();

                if($c_petugas > 0){
                    foreach($d_petugas as $p){
                        $arr_petugas[] = $p->nama;
                    }

                    $id_petugas = implode("<br>", $arr_petugas);
                }
            }

    		$arr[] = ["id" => $d->id,
                    "tanggal" => tgl_full($d->tanggal,''),
                    "jam" => $d->jam_mulai,
                    "tgl_jadwal" => tgl_full($d->tanggal,'')." ( ".$d->jam_mulai." )",
                    "jadwal" => $d->jadwal,
                    "petugas" => $id_petugas,
    				"aksi" => "<div class='btn-group' role='group'><button type='button' class='btn btn-icon btn-warning' onclick='edit(".$d->id.")'><i class='fa fa-pencil-square-o'></i></button><button type='button' class='btn btn-icon btn-danger' onclick='hapus(".$d->id.")'><i class='fa fa-trash-o'></i></button></div>
                        <input type='hidden' value='".$d->id."' name='table_id' id='table_id".$d->id."'>
                        <input type='hidden' value='".$tgl."' name='table_tanggal' id='table_tanggal".$d->id."'>
                        <input type='hidden' value='".$d->id_petugas."' name='table_idpetugas' id='table_idpetugas".$d->id."'>
                        <input type='hidden' value='".$d->id_jadwal."' name='table_idjadwal' id='table_idjadwal".$d->id."'>"];
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'petugas'])
        ->make(true);
    }

    function simpan(Request $request){
        $tanggal = tgl_full($request->get('popup_tanggal'),'99');

        $id = $request->get('popup_id');

	    $data['id_jadwal'] = $request->get('popup_jadwal');
        $data['id_petugas'] = implode(',',$request->get('popup_petugas'));
        $data['tanggal'] = $tanggal;

        DB::beginTransaction();
        try {
            if($id == ''){
                DB::table('jadwal_petugas')->insert($data);
            }else{
                DB::table('jadwal_petugas')->where(array('id' => $id))->update($data);
            }
            DB::commit();   
        } catch (Exception $e) {
            DB::rollBack();
        }
        return $data;
    }

    public function hapus(Request $request){
        $id = $request->get('id');

        $query = DB::table('jadwal_petugas')->where(['id'=>$id])->delete();

        if($query){
            $result = [
                "type"=>"success",
                "text"=>"Data berhasil dihapus"
            ];
        }else{
            $result = [
                "type"=>"error",
                "text"=>"Terjadi kesalahan!"
            ];
        }

        echo json_encode($result);
    }

    function get_jadwal(Request $request){
        $tglnow = $request->get("tanggal");

        $arr = array("Minggu"=>0,"Senin"=>1,"Selasa"=>2,"Rabu"=>3,"Kamis"=>4,"Jumat"=>5,"Sabtu"=>6);
        $tanggal = tgl_full($tglnow,'99');
        $hari = $arr[tgl_full($tglnow,'6')];
        $d_jadwal = DB::SELECT("SELECT id_jadwal, tanggal, hari, jam_mulai as jam, jenis_jadwal as status, nama FROM (
                    select * from jadwal where tanggal = '".$tanggal."' and jenis_jadwal='0'
                    UNION ALL
                    select * from jadwal where hari = '".$hari."' and jenis_jadwal='1'
                    ) as a ORDER BY jam_mulai ");
        
        $return = array();
        
        if(count($d_jadwal) > 0){
            foreach($d_jadwal as $d){
                $hari = ($d->status==1)?tgl_full($d->hari,'10'):tgl_full($d->tanggal,'0');

                $return[] = "<option value='".$d->id_jadwal."'>".$d->nama." ( ".$hari." ".$d->jam." )"."</option>";
            }
        }else{
            $return[] = "<option value=''>-- Pilih --</option>";
        }

        print_r($return);
    }

}