<?php

namespace App\Http\Controllers\Show;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class ShowController extends Controller
{
    public $path;
    public $dimensions;

    public function __construct()
    {   
        $this->path = 'images/user';
        $this->dimensions = ['500'];
        $this->middleware('auth');
    }

    public function index(){
    	return view('contents.show.show');
    }

    public function detail($id, $tgl){
    	$d_area = get_show()->where("j.id_jadwal", $id);
    	$d_data = get_show()->where("j.id_jadwal", $id);

    	// ->leftjoin("seats AS s", function($leftjoin){
    	// 	$leftjoin->on("r.id", "s.id_rooms")->on("s.id_locations", "loc.id");
    	// })
        $id_area = array();
        $id_area = $d_area->get()->first()->id_area;
        // $id_area = explode(",", $id_area);

    	// $area = $d_area->leftjoin("area AS a", "a.id_lokasi", "loc.id")
     //    ->whereIn("a.id", $id_area)
    	// ->groupBy("a.id", "a.nama")
    	// ->orderBy("a.nama")
    	// ->select("a.id","a.nama AS area");

        $tgl_show = $tgl;
        $where[] = "s.area IN (".$id_area.")";
        $where[] = "j.id_jadwal = '".$id."'";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
        $where = "WHERE ".implode(" AND ", $where);
        
        $condition['where'] = $where;
        $condition['order'] = " GROUP BY s.area ORDER BY s.nama ASC";
        $condition['select'] = ", SUM(CASE WHEN bo.id_booking IS NULL THEN 1 ELSE 0 END) AS sisa_kursi, a.id id_area";
        $d_map = get_seat_detail($condition, $tgl_show);
    	
    	
    	$detail_ruang = $d_data->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('".$tgl."', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show, loc.alamat alamat_lokasi"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl."', '%Y-%m-%d')) - 1) END");

    	$data['area'] = $d_map;
    	$data['detail_ruang'] = $detail_ruang->first();
        $data['gereja'] = DB::table("locations")->where(["tipe" => "0", "is_aktif" => '1'])->get();
    	return view('contents.show.detail')->with("data", $data);
    	
    }

    function simpan(Request $request){
    	$is_member = $request->get('is_member');
    	$id_duduk = $request->get('id_duduk');
    	$id_lokasi = $request->get('id_lokasi');
    	$id_ruang = $request->get('id_ruang');
    	$area = $request->get("val_area");
    	$id_jadwal = $request->get("jadwal");
        $text_jadwal = $request->get("text_jadwal");
        $jenis_jadwal = $request->get("jenis_jadwal");
        $tgl_show = $request->get("tgl_show");

        $lokasi_gereja = $request->get("popup_gereja");
        $wilayah = ($request->get("popup_wilayah") != '') ? $request->get("popup_wilayah"):"0";
        $lingkungan = ($request->get("popup_lingkungan") != '') ? $request->get("popup_lingkungan"):"0";
        $hp = $request->get("popup_hp");        
        if($lokasi_gereja > 9000){
            $lokasi_gereja = $request->get("popup_gerejatext");
            $wilayah = '0';
            $lingkungan = '0';
        }
    	
    	$where[] = "s.area = '".$area."'";
    	$where[] = "j.id_jadwal = '".$id_jadwal."'";
    	$where[] = "s.id = '".$id_duduk."'";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
    	$where = "WHERE ".implode(" AND ", $where);
    	
    	$condition['where'] = $where;
    	$d_map = get_seat_detail($condition, $tgl_show);
    	$count = count($d_map);

        
        $count_file = ($request->get('img_foto') != '') ? "1":"";
        $nama_file = '';
        if($count_file > 0){
            $path = "images/member/";
            if (!is_dir($path )) {
                mkdir($path, 0777, true);
            }
            $data_foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->get('img_foto')));
            $nama_file = date("YmdHis").".png";
            file_put_contents($path.$nama_file, $data_foto);
        }
        
        

    	if($count > 0){
    		if($d_map[0]->is_booking == 0){
    			if($is_member == 0){
                    $jum = jum_pesanmember($id_jadwal, $tgl_show, $request->get('member'));

                    if($jum < max_pesan()){
    				$insert['id_member'] = $request->get('member');
    				$insert['id_location'] = $id_lokasi;
    				$insert['id_seat'] = $id_duduk;
    				$insert['id_jadwal'] = $id_jadwal;
    				$insert['tgl_booking'] = date("Y-m-d");
    				$insert['tgl_show'] = $tgl_show;
    				$insert['status'] = '1';
    				$insert['is_show'] = '1';
                    $insert['kategori_jadwal'] = $jenis_jadwal;
    				$id = DB::table("booking")->insertGetId($insert);

                    $insert_detail['id_booking'] = $id;
                    $insert_detail['id_seat'] = $id_duduk;
                    $insert_detail['id_member'] = $request->get('member');
                    $insert_detail['status'] = '1';
                    
                    DB::table('booking_detail')->insert($insert_detail);

                    /* -- Log -- */
                    $get_member = get_member()->where("m.id", $insert['id_member'])->get()->first();
                    $get_kursi = DB::table("seats")->where("id", $id_duduk)->get()->first();
                    $get_area = get_area()->where("a.id", $get_kursi->area)->get();
                    $c_area = $get_area->count();
                    $area = ($c_area > 0) ? $get_area->first()->nama:"";
                    $j = get_show()->where("j.id_jadwal", $id_jadwal)->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, loc.alamat alamat_lokasi"))->get()->first();
                    trigger_log($id, "booking", "Jadwal", "Tambah pesanan untuk member ".$get_member->nama." (".$get_member->id."), dengan nomor kursi ".$get_kursi->row." ".$get_kursi->kolom." di area ".$area.". Untuk jadwal ".$j->text_jadwal.", tanggal ".tgl_full($tgl_show." ".$j->jam_mulai, 78)." di Gereja ".$j->nama_lokasi.". ", 1, 1);
                    /* -- Log -- */

    				$response = ['status' => '1', "keterangan" => "Data tersimpan"];
                    }else{
                        $response = ['status' => '0', "keterangan" => "Member yang dipilih sudah melebihi batas pemesanan untuk jadwal ini"];
                    }
    			}else{
                    $d_member = DB::table("member")->where("no_telp", $hp)->get()->count();

                    if($d_member <= 0){
        				$member['nik'] = ($request->get("popup_nik") != '') ? $request->get("popup_nik"):"";
        				$member['nama'] = $request->get("popup_nama");
        				$member['alamat'] = !empty($request->get("popup_alamat")) ? $request->get("popup_alamat"):"";
        				$member['tgl_registrasi'] = date('Y-m-d');
                        $member['tgl_aktivasi'] = date('Y-m-d');
        				$member['is_aktif'] = '1';
        				$member['is_petugas'] = '0';
                        $member['id_lokasi'] = $lokasi_gereja;
                        $member['foto'] = $nama_file;
                        $member['id_wilayah'] = $wilayah;
                        $member['id_lingkungan'] = $lingkungan;
                        $member['no_telp'] = $hp;

        				$id_mem = DB::table("member")->insertGetId($member);

        				$insert['id_member'] = $id_mem;
        				$insert['id_location'] = $id_lokasi;
        				// $insert['id_room'] = $id_ruang;
        				$insert['id_seat'] = $id_duduk;
        				$insert['id_jadwal'] = $id_jadwal;
        				$insert['tgl_booking'] = date("Y-m-d");
        				$insert['tgl_show'] = $tgl_show;
        				$insert['status'] = '1';
        				$insert['is_show'] = '1';
                        $insert['kategori_jadwal'] = $jenis_jadwal;

        				$id = DB::table("booking")->insertGetId($insert);

                        $insert_detail['id_booking'] = $id;
                        $insert_detail['id_seat'] = $id_duduk;
                        $insert_detail['id_member'] = $id_mem;
                        $insert_detail['status'] = '1';
                        
                        DB::table('booking_detail')->insert($insert_detail);

                        /* -- Log -- */
                        
                        $get_kursi = DB::table("seats")->where("id", $id_duduk)->get()->first();
                        $get_area = get_area()->where("a.id", $get_kursi->area)->get();
                        $c_area = $get_area->count();
                        $area = ($c_area > 0) ? $get_area->first()->nama:"";
                        $j = get_show()->where("j.id_jadwal", $id_jadwal)->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, loc.alamat alamat_lokasi"))->get()->first();

                        trigger_log($id, "booking", "Jadwal", "Tambah pesanan untuk member ".$member['nama']." (".$id_mem."), dengan nomor kursi ".$get_kursi->row." ".$get_kursi->kolom." di area ".$area.". Untuk jadwal ".$j->text_jadwal.", tanggal ".tgl_full($tgl_show." ".$j->jam_mulai, 78)." di Gereja ".$j->nama_lokasi.". ", 1, 1);
                        /* -- Log -- */

        				$response = ['status' => '1', "keterangan" => "Data tersimpan"];
                    }else{
                        $response = ['status' => '2', "keterangan" => "No. Hp sudah didaftarkan, silahkan coba lagi"];   
                    }
    			}
    		}else{
    			$response = ['status' => '0', "keterangan" => "Kursi yang dipilih sudah dipesan, silahkan pilih kursi lain"];	
    		}
    	}else{
    		$response = ['status' => '0', "keterangan" => "Gagal menyimpan data"];
    	}


    	if($is_member == 0){
    		$member = $request->get('member');

    	}
    	echo json_encode($response);
    }

    public function get_data(Request $request){
        $tgl = date("Y-m-d");
        if(!empty($request->get('tanggal'))){
            $tgl = tgl_full($request->get('tanggal'), 99);
        }

        $d_data = get_show()->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('".$tgl."', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl."', '%Y-%m-%d')) - 1) END")->orderby("j.jam_mulai", "asc");
        
        $arr = array();
        foreach ($d_data->get() as $d) {
            $arr[] = ["tanggal" => tgl_full($d->tgl_show, ''),
                    "jam_mulai" => $d->jam_mulai,
                    "jam_selesai" => $d->jam_selesai,
                    "keterangan" => $d->nama,
                    "nama_lokasi" => $d->nama_lokasi,
                    "jenis_jadwal" => $d->text_jadwal,
                    "aksi" => "<div class='btn-group' role='group'><a class='btn btn-icon btn-info' type='button' href='".url('lihat_show/detail').'/'.$d->id_jadwal.'/'.$d->tgl_show."'><i class='fa fa-eye'></i></a></div>"];
        }

        return Datatables::of($arr)
        ->rawColumns(['aksi'])
        ->make(true);
    }

    function get_duduk(Request $request){
    	$area = $request['id'];
    	$id_jadwal = $request['id_jadwal'];
        $tgl_show = $request->get("tgl_show");

        $column = array();
        $row = array();
    	$d_data = DB::table("seats")->groupBy("deret", "kolom")->select("deret", "kolom")->orderby("kolom", "asc")->where("area", $area)->get();
        $wheres = "where area = '".$area."'";
        $d_data = DB::SELECT("SELECT deret, kolom FROM seats $wheres GROUP BY deret, kolom ORDER BY kolom asc");
    	foreach ($d_data as $key => $value) {
    		$column[$value->deret] = $value->kolom;
    	}
    	$col = implode(',',$column);
    	$y['column'] =explode(",", $col);

        $d_data = DB::SELECT("SELECT row FROM seats $wheres GROUP BY row ORDER BY LENGTH(row), row asc");
        foreach ($d_data as $key => $value) {
            $row[$value->row] = $value->row;
        }
        $r = implode(',',$row);
        $y['row'] =explode(",", $r);

    	$where[] = "s.area = '".$area."'";
    	$where[] = "j.id_jadwal = '".$id_jadwal."'";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
    	$where = "WHERE ".implode(" AND ", $where);

        $where_area = "where area = '".$area."'";
    	
    	$condition['where'] = $where;

    	$d_map = get_seat_detail($condition, $tgl_show);

    	// $map = $d_map;
    	$map = array();
    	foreach ($d_map as $key => $value) {
    		$kolom = "";
    		if($value->kolom == null){
    			$kolom = "_";
    		}else{
    			if($value->is_booking == 1){
    				if($value->status_pesan == 0){
    					$kolom = "f";
    				}else{
    					$kolom = "d";
    				}
    			}else if($value->is_booking == 0){
    				$kolom = "e";
    			}
    		}
    		$map[$value->baris][] = $kolom;
    	}

        $map = array_values($map);

    	$aa = array();
    	for($i=0; $i<count($map); $i++){
    		$aa[] = implode('',$map[$i]);
    	}
    	$ma = implode(',',$aa);
    	$y['map'] = explode(",", $ma);

        $d_jum = DB::SELECT("SELECT COUNT('*') as jum FROM seats $where_area  GROUP BY baris ORDER BY COUNT(*) DESC limit 1");
        if(count($d_jum) > 0){
            $jum_kolom = ((int)$d_jum[0]->jum>10)?(int)((($d_jum[0]->jum/10)*400)-(($d_jum[0]->jum/10)*25)):(int)(($d_jum[0]->jum*40)+80);
        }else{
            $jum_kolom = 0;
        }
        $y['jum_kolom'] = $jum_kolom;

    	return response()->json($y);
    }

    function get_detail_duduk(Request $request){
    	$id = $request->get("id");
    	$area = $request->get("area");
    	$id_jadwal = $request->get("id_jadwal");
        $tgl_show = $request->get("tgl_show");

    	$ex = explode("_", $id);

    	$where[] = "s.area = '".$area."'";
    	$where[] = "j.id_jadwal = '".$id_jadwal."'";
    	$where[] = "s.row = '".$ex[0]."' ";
    	$where[] = "s.kolom = '".$ex[1]."' ";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";

    	$where = "WHERE ".implode(" AND ", $where);
    	$condition['where'] = $where;
    	$d_map = get_seat_detail($condition, $tgl_show);

    	$arr = array();
        $tgl_show = '';
        $tgl_full = '';
        $tgl_ini = date("Y-m-d H:i");
    	foreach ($d_map as $k => $val) {
    		$waktu_daftar = "1";
            $tgl_full = $val->tgl_show." ".$val->jam_mulai;
            $waktu_jadwal = strtotime($val->tgl_show." ".$val->jam_mulai);
            if(strtotime($tgl_ini) > $waktu_jadwal){
                $waktu_daftar = "0";
            }

            $val->text_status = ($val->is_booking == '0') ? "Belum Dipesan":"Dipesan";

    		$val->text_pesan = text_pesan($val->status_pesan);
    		$val->nomor_kursi = $val->row."_".$val->kolom;
            $val->is_pesan = $waktu_daftar;
            $val->tgl_ini = $tgl_ini;
    		$arr[] = $val;

    	}

    	echo json_encode($arr);
    }

    function get_member(){
    	$d_data = DB::table("member")->where(['is_aktif' => 1, "is_petugas" => '0'])->get();
    	$arr = array();
    	foreach($d_data as $d){
            $nik = ''; $no_telp = '';
            if($d->nik != null){
                $nik = $d->nik;
            }
            if($d->no_telp != null){
                $no_telp = $d->no_telp;
            }
    		$arr[] = ["id_member" => $d->id,
    					"nik" => $nik,
    					"nama" => $d->nama,
                        "no_telp" => $no_telp];
    	}

    	echo json_encode($arr);
    }

    function get_jadwal(){
    	return DB::table("jadwal AS j")->join("locations AS loc", "j.id_location", "loc.id")->join("rooms AS r", "j.id_room", "r.id")->where(["loc.is_aktif" => "1", "r.is_aktif" => "1"]);
    }

    function get_lingkungan(Request $request){
        $id = $request->get("id");
        $parent = $request->get("parent");
        print_r(_lokasiselect($id, $parent));
    }

    function get_area(Request $request){
        $tgl = $request->get('tgl');
        $id = $request->get('id_jadwal');


        $d_area = get_show()->where("j.id_jadwal", $id);
        $id_area = array();
        $id_area = $d_area->get()->first()->id_area;

        $tgl_show = $tgl;
        $where[] = "s.area IN (".$id_area.")";
        $where[] = "j.id_jadwal = '".$id."'";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
        $where = "WHERE ".implode(" AND ", $where);
        
        $condition['where'] = $where;
        $condition['order'] = " GROUP BY s.area ORDER BY s.nama DESC";
        $condition['select'] = ", SUM(CASE WHEN bo.id_booking IS NULL THEN 1 ELSE 0 END) AS sisa_kursi, a.id id_area";
        $d_map = get_seat_detail($condition, $tgl_show);

        $arr = array();
        foreach($d_map as $d){
            $arr[] = '<option value="'.$d->id_area.'" nama="'.$d->nama_area.'">'.$d->nama_area.' ( '.$d->sisa_kursi.' )'.'</option>';
        }
        print_r($arr);
    }
}
