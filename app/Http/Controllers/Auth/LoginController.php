<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $timeout    = 120;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(){
        return 'username';
    }

    public function showLoginForm(){
        return view('login.index');
    }

    protected function credentials(Request $request){
        return $request->only('username', 'password');
    }

    protected function attemptLogin(Request $request)
    {
        $validation = $this->validate_input($request);

        if ($validation->fails()) {
            return redirect('/home')->withErrors($validation)->withInput();
        }

        $username = $request->username;
        $password = $request->password;
        $active   = '1';
        
        $cek = DB::table('user_accounts')->where(['username'=>$username,'password'=>$password,'active'=>'1']);
        if($cek->count() > 0){
        $auth_guard = Auth::guard('web'); 
        $auth_attempt =Auth::attempt(['active' => '1', 'username' => $request->username, 'password' => $request->password ], $request->remember);
        }
    }

    private function validate_input($request, $id = null)
    {

        return Validator::make(
            $request->all(),
            [
                'username' => 'required',
                'password' => 'required',
            ]
        );
    }

    public function logout(Request $request)
    {
        $this->guard('web')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/home')->withSuccess('Terimakasih, selamat datang kembali!');
    }

}
