<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['grup'] = DB::table("user_groups")->orderBy("nama_group", "asc")->get();
    	$data['member'] = DB::table("member")->orderBy('nama', 'asc')->where('is_aktif', '1')->get();
    	return view('contents.superadmin.user')->with('data', $data);
    }

    function get_data(){
    	$d_data = get_useracc();
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id_user."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id_user."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
    		$d->status = is_aktif($d->active);
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status'])
        ->make(true);
    }

    function get_edit(Request $request){
		$id = $request->get('id');

		$d_data = get_useracc()->where('ua.id', $id)->get();

		echo json_encode($d_data);
    }

    function simpan(Request $request){
    	$id = $request->get('popup_id');
    	$nik = ''; $nama = ''; $id_lokasi = '';
    	$data['id_member'] = $request->get('popup_member');
    	$data['username'] = $request->get('popup_user');
    	
    	$data['retype_password'] = $request->get('popup_pass');
    	$data['id_group'] = $request->get('popup_grup');
    	$data['active'] = $request->get('popup_aktif');
    	
    	$d_user = DB::table("member")->where("id", $request->get('popup_member'));
    	$count = $d_user->get()->count();
    	$dt = $d_user->first();

    	if($count > 0){
    		$nik = $dt->nik;
    		$nama = $dt->nama;
    		$id_lokasi = $dt->id_lokasi;
    	}

    	$data['nik'] = $nik;
    	$data['nama'] = $nama;
    	$data['id_lokasi'] = $id_lokasi;

    	if($id == ''){
    		$data['password'] = $request->get('popup_pass');
    		$id = DB::table('user_accounts')->insertGetId($data);
            /* -- Log -- */
            trigger_log($id, "user_accounts", "Tambah Akun", "Tambah Akun dengan username ".$data['username']." (".$id.")", 1, 1);
            /* -- Log -- */
    	}else{
    		if($request->get('popup_pass') != ''){
    			$data['password'] = $request->get('popup_pass');
    		}
            trigger_log($id, "user_accounts", "Edit Akun", "Rubah Akun dari username ".$data['username']." (".$id.")", 2, 1);
    		DB::table("user_accounts")->where("id", $id)->update($data);
            trigger_log($id, 'booking_detail', 'User', 'Edit User dengan username = '.$request->get('popup_user'),2,1);  
    	}

    	echo json_encode(["status" => "1"]);
    }

    function hapus(Request $request){
    	$id = $request->get("id");

        $d_data = DB::table('user_accounts')->where("id", $id)->get();
    	DB::table('user_accounts')->where("id", $id)->delete();
    	$c_data = DB::table('user_accounts')->where("id", $id)->get();
        $count = $c_data->count();
    	if($count == 0){
            trigger_log($id, "user_accounts", "Hapus Akun", "Hapus Akun dengan username ".$d_data->first()->username." (".$d_data->first()->id.")", 3, 1);
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }
}