<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class GrupController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('contents.master_grup.index');
    }

    public function get_data(){
    	$d_data = DB::table("user_groups")->orderby("nama_group", "asc");
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$arr[] = ["id" => $d->id,
    				"nama" => $d->nama_group,
    				"aksi" => "<div class='btn-group' role='group'><a class='btn btn-icon btn-info' type='button' data-id='".$d->id."' href='".url('grup/detail').'/'.$d->id."'><i class='fa fa-eye'></i></a> <button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>".
    					'<input type="hidden" id="table_id'.$d->id.'" value="'.$d->id.'">'.
    					'<input type="hidden" id="table_nama'.$d->id.'" value="'.$d->nama_group.'">'];
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi'])
        ->make(true);
    }

    function simpan(Request $request){
    	$id = $request->get("popup_id");
    	$insert['nama_group'] = $request->get("popup_grup");

    	if($id == ''){
    		DB::table('user_groups')->insert($insert);
    	}else{
    		DB::table("user_groups")->where("id", $id)->update($insert);
    	}

    	echo json_encode(["status" => '1']);
    }

    function hapus(Request $request){
    	$id = $request->get("id");

    	DB::table('user_groups')->where("id", $id)->delete();
    	$d_data = DB::table('user_groups')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }

    function detail($id){
        $data['id'] = $id;
        $d_data = DB::table("user_groups")->where("id", $id)->get();
        $count = $d_data->count();

        $data['nama_grup'] = "";
        if($count > 0){
            $data['nama_group'] = $d_data->first()->nama_group;
        }

        return view('contents.master_grup.detail')->with("data", $data);
        // print_r(menu_item());
        // print_r(get_role('1'));

    }

    function get_data_detail(Request $request){
        $id = $request->get('id');

        $d_data = DB::table("user_menu AS um")->join("master_menu AS m1", function($join){
            $join->on("m1.id", "um.child_id")
            ->on("m1.parent", "um.parent_id");
        })->leftjoin("master_menu AS m2", "m2.id", "m1.parent")
        ->where("id_group", $id)
        ->orderby("m1.id", "asc")
        ->select(DB::raw("um.*, m1.name, m1.icon, m2.name AS parent_menu"));
        
        $arr = array();
        foreach ($d_data->get() as $d) {
            $icon = "";
            if($d->icon != '' || $d->icon != null){
                $icon = "<i class='".$d->icon."'></i> ".$d->icon;
            }
            $d->icon_html = $icon;
            $d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
            $arr[] = $d;
        }

        return Datatables::of($arr)
        ->rawColumns(['aksi', 'icon_html'])
        ->make(true);
    }

    function hapus_detail(Request $request){
        $id = $request->get("id");

        DB::table('user_menu')->where("id", $id)->delete();
        $d_data = DB::table('user_menu')->where("id", $id)->get()->count();

        if($d_data == 0){
            $arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
        }else{
            $arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
        }

        echo json_encode($arr);

    }

    function get_menu(Request $request){
        $id = $request->get('id');

        print_r(_menuselect($id));
    }

    function simpan_menu(Request $request){
        $id = $request->get("popup_id");
        $insert['id_group'] = $request->get("popup_idgrup");
        $insert['child_id'] = $request->get("popup_grup");

        $d_menu = DB::table("master_menu")->where("id", $insert['child_id'])->get();
        $count = $d_menu->count();

        if($count > 0){
            $d_first = $d_menu->first();
            $insert['parent_id'] = $d_first->parent;

            DB::table('user_menu')->insert($insert);
        }

        echo json_encode(["status" => '1']);
    }
}