<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class MenuController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data['menu'] = get_menu()->orderby("menu.urutan", "asc")->orderby("menu.parent", "asc")->orderby("menu.id", "asc")->get();
    	return view('contents.superadmin.menu')->with('data',$data);
    }

    function get_data(){
        $d_data = get_menu()->orderby("menu.urutan", "asc")->orderby("menu.parent", "asc")->orderby("menu.id", "asc"); 
        $arr = array();
        foreach ($d_data->get() as $d) {
            $d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
            $arr[] = $d;
        }

        echo json_encode($arr);
    }

    function get_data2(){
        $d_data = get_menu()->orderby("menu.urutan","asc")->orderby("menu.parent","asc")->orderby("menu.id","asc");
        $arr = array();
        if($d_data->count() > 0){
            foreach ($d_data->get() as $key => $d) {
                # code...
                $arr[] = array("id"=>$d->id,
                               "menu"=>"<i class='".$d->icon."'></i> ".$d->name,
                               "link"=>$d->url,
                               "icon"=>$d->icon,
                               "parent"=>$d->parent_menu,
                               "tipe"=>$d->tipe_site,
                               "urutan"=>$d->urutan,
                               "aksi"=>"<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit(".$d->id.")'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus(".$d->id.")'><i class='fa fa-trash-o'></i></button></div>
                               <input type='hidden' value='".$d->id."' name='table_id[]' id='table_id".$d->id."'>
                               <input type='hidden' value='".$d->name."' name='table_nama[]' id='table_nama".$d->id."'>
                               <input type='hidden' value='".$d->url."' name='table_url[]' id='table_url".$d->id."'>
                               <input type='hidden' value='".$d->icon."' name='table_icon[]' id='table_icon".$d->id."'>
                               <input type='hidden' value='".$d->parent."' name='table_parent[]' id='table_parent".$d->id."'>
                               <input type='hidden' value='".$d->tipe_site."' name='table_tipe[]' id='table_tipe".$d->id."'>
                               <input type='hidden' value='".$d->urutan."' name='table_urutan[]' id='table_urutan".$d->id."'>
                               ");
            }
            
        }

        return Datatables::of($arr)
        ->rawColumns(['aksi','menu'])
        ->make(true);
    }

    function simpan(Request $request){
        $id = $request->get('popup_id');
        $data['name']   = $request->get('popup_name');
        $data['url']    = $request->get('popup_url');
        $data['icon']   = $request->get('popup_icon');
        $data['parent'] = $request->get('popup_parent');
        $data['tipe_site'] = $request->get('popup_aktif');
        $data['urutan'] = $request->get('popup_urutan');

        DB::beginTransaction();
        try {
            if($id == ''){
                DB::table('master_menu')->insert($data);
            }else{
                DB::table('master_menu')->where(array('id' => $id))->update($data);
            }
            DB::commit();   
        } catch (Exception $e) {
            DB::rollBack();
        }
        return $data;
    }

    public function hapus(Request $request){
        $id = $request->get('id');

        $query = DB::table('master_menu')->where(['id'=>$id])->delete();

        if($query){
            $result = [
                "type"=>"success",
                "text"=>"Data berhasil dihapus"
            ];
        }else{
            $result = [
                "type"=>"error",
                "text"=>"Terjadi kesalahan!"
            ];
        }

        echo json_encode($result);
    }
}