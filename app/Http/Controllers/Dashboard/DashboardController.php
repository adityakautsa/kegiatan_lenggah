<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $id_member  = Auth::user()->id_member;
        $id_group   = Auth::user()->id_group;
        $where[] = "m.id = '".$id_member."' ";

        $arr['where'] = " WHERE ".implode(" AND ", $where);
        // $arr['order_by'] = "ORDER BY b.tgl_booking DESC";
        $arr['order_by'] = "ORDER BY b.tgl_show DESC";
        $arr['group_by'] = "GROUP BY b.id_booking, b.id_member";
        $d_data = get_riwayat_pesanan($arr);

        $d_arr = array();
        foreach ($d_data as $key => $d) {
            # code...
            $d_arr[] = array('tgl_show'     =>tgl_full($d->tgl_show.' '.$d->jam_mulai, '9'),
                           'tgl_booking'    =>tgl_full($d->created_at,'9'),
                           'jumlah_kursi'   =>format_angka($d->jumlah_kursi),
                           'kursi'          =>$this->get_data_kursi($d->id_booking),
                           'aksi'           =>"<div class='btn-group' role='group'><a href='#' onclick='show(".$d->id_booking.")' class='btn btn-icon btn-info' type='button'><i class='fa fa-eye'></i></a></div>");
        }
        $data['data'] = $d_arr;

        $d_data_petugas = DB::SELECT("SELECT b.id_booking, j.id_area, b.id_jadwal, b.tgl_booking, b.tgl_show, j.tanggal, j.jam_mulai as jam, CASE WHEN sum(bd.hadir) IS NULL THEN '0' ELSE sum(bd.hadir) END as hadir, 
            CASE WHEN sum(bd2.tidak_hadir) IS NULL THEN '0' ELSE sum(bd2.tidak_hadir) END as tidak_hadir,
            CASE WHEN sum(bd3.pesan) IS NULL THEN '0' ELSE sum(bd3.pesan) END as pesan  
            FROM booking as b
            LEFT JOIN jadwal AS j ON j.id_jadwal = b.id_jadwal
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as hadir FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking WHERE bd.status='1' GROUP BY b.id_jadwal
            ) as bd ON b.id_booking=bd.id_booking
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as tidak_hadir FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking WHERE bd.status='0' GROUP BY b.id_jadwal
            ) as bd2 ON b.id_booking=bd2.id_booking
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as pesan FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking GROUP BY b.id_jadwal
            ) as bd3 ON b.id_booking=bd3.id_booking
            GROUP BY b.id_jadwal, b.tgl_show
            ORDER BY b.id_booking DESC
            limit 1");
        
        $p_arr = array();
        foreach($d_data_petugas as $d){
            $p_arr[] = array('jadwal'=>tgl_full($d->tgl_show,'1').' '.$d->jam,
                             'kapasitas'=>format_angka($this->get_kapasitas($d->id_area)),
                             'pesan'=>format_angka($d->pesan),
                             'hadir'=>format_angka($d->hadir),
                             'tidak_hadir'=>format_angka($d->tidak_hadir),
                             'kosong'=>format_angka(($this->get_kapasitas($d->id_area)-$d->pesan)));
        }
        $data['data_p'] = $p_arr;
        
        //Misa Yang Akan Datang
        $hari_ini= strval(date("Y-m-d"));
        $besok = strval(date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d")))));  

        // --------------------- Coba2 --------------------------------------------------------------
        $ye = date("Y-m-d");      
        $ye2 = date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d"))));
        $hari= array("Minggu"=>0, "Senin"=>1, "Selasa"=>2, "Rabu"=>3, "Kamis"=>4, "Jumat"=>5, "Sabtu"=>6);
        $day = $hari[tgl_full($ye,6)];
        $day_tomoro = $hari[tgl_full($ye2,6)];
        

        $d_jadwal = get_show()->select(DB::raw("j.id_jadwal, j.jenis_jadwal, j.id_area, loc.nama nama_lokasi, 
            CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal,
            CASE WHEN j.jenis_jadwal = 1 THEN j.hari ELSE j.tanggal END AS waktu,
            j.jam_mulai AS jam"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal >= '".$hari_ini."' AND j.tanggal <= '".$besok."'  ELSE j.hari >= (DAYOFWEEK(DATE_FORMAT('".$hari_ini."', '%Y-%m-%d')) - 1) AND j.hari <= (DAYOFWEEK(DATE_FORMAT('".$besok."', '%Y-%m-%d')) - 1)  END")->orderby("j.jam_mulai", "asc");
        
        $c_jadwal = array();
        foreach($d_jadwal->get() as $d){
            $dwaktu = tgl_full($d->waktu,'1');
            if($d->jenis_jadwal==1 && $d->waktu==$day){
                $dwaktu = tgl_full($ye,'1');
            }elseif($d->jenis_jadwal==1 && $d->waktu==$day_tomoro){
                $dwaktu = tgl_full($ye2,'1');
            }
            $c_jadwal[] = array('jadwal'=>$dwaktu,
                                'id_jadwal'=>$d->id_jadwal,
                                'area'=>$d->id_area,
                                'jam'=>$d->jam);
        }
        
        $f_arr = array();
        foreach($c_jadwal as $key => $d){
            $cek = $this->get_tomorow($d['id_jadwal'],$d['area'],$ye,$ye2);
            $f_arr[] = array("jadwal"=>$d['jadwal'],
                                "kapasitas"=>$cek[0]->kapasitas,
                                "pesan"=>$cek[0]->pesan,
                                "hadir"=>$cek[0]->hadir,
                                "tidak_hadir"=>$cek[0]->tidak_hadir,
                                "kosong"=>$cek[0]->kosong,
                                "jam"=>$d['jam']);
        }        
        

        // ---------------------- END COBA2 ----------------------------------------------------------------

        // $e_data_petugas = DB::SELECT("SELECT id_booking, id_area, id_jadwal, jenis_jadwal, tgl_booking, tgl_show, tanggal, jam, hadir, tidak_hadir,created_at,tgl_urut,pesan FROM (
        //     SELECT b.id_booking, j.id_area, j.id_jadwal, j.jenis_jadwal,b.tgl_booking, b.tgl_show, j.tanggal,j.created_at,j.created_at AS tgl_urut, j.jam_mulai AS jam, CASE WHEN SUM(bd.hadir) IS NULL THEN '0' ELSE SUM(bd.hadir) END AS hadir, 
        //                 CASE WHEN SUM(bd2.tidak_hadir) IS NULL THEN '0' ELSE SUM(bd2.tidak_hadir) END AS tidak_hadir,
        //                 CASE WHEN SUM(bd3.pesan) IS NULL THEN '0' ELSE SUM(bd3.pesan) END AS pesan  
        //                 FROM booking AS b
        //                 RIGHT JOIN (SELECT id_jadwal, jenis_jadwal, tanggal, jam_mulai, id_area, created_at FROM jadwal WHERE DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '2020-08-19' AND '2020-08-20'
        //                 ) AS j ON j.id_jadwal = b.id_jadwal
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS hadir FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking WHERE bd.status='1' GROUP BY b.id_jadwal
        //                 ) AS bd ON b.id_booking=bd.id_booking
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS tidak_hadir FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking WHERE bd.status='0' GROUP BY b.id_jadwal
        //                 ) AS bd2 ON b.id_booking=bd2.id_booking
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS pesan FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking GROUP BY b.id_jadwal
        //                 ) AS bd3 ON b.id_booking=bd3.id_booking
        //                 WHERE j.jenis_jadwal IN(1)
        //                 GROUP BY j.id_jadwal
        //     UNION
        //     SELECT b.id_booking, j.id_area, j.id_jadwal, j.jenis_jadwal, b.tgl_booking, b.tgl_show, j.tanggal, j.created_at,j.tanggal AS tgl_urut, j.jam_mulai AS jam, CASE WHEN SUM(bd.hadir) IS NULL THEN '0' ELSE SUM(bd.hadir) END AS hadir, 
        //                 CASE WHEN SUM(bd2.tidak_hadir) IS NULL THEN '0' ELSE SUM(bd2.tidak_hadir) END AS tidak_hadir,
        //                 CASE WHEN SUM(bd3.pesan) IS NULL THEN '0' ELSE SUM(bd3.pesan) END AS pesan  
        //                 FROM booking AS b
        //                 RIGHT JOIN (SELECT id_jadwal, jenis_jadwal,tanggal, jam_mulai, id_area, created_at FROM jadwal WHERE DATE_FORMAT(tanggal,'%Y-%m-%d') BETWEEN '2020-08-19' AND '2020-08-20'
        //                 ) AS j ON j.id_jadwal = b.id_jadwal
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS hadir FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking WHERE bd.status='1' GROUP BY b.id_jadwal
        //                 ) AS bd ON b.id_booking=bd.id_booking
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS tidak_hadir FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking WHERE bd.status='0' GROUP BY b.id_jadwal
        //                 ) AS bd2 ON b.id_booking=bd2.id_booking
        //                 LEFT JOIN ( SELECT b.id_booking, COUNT(*) AS pesan FROM booking AS b LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking GROUP BY b.id_jadwal
        //                 ) AS bd3 ON b.id_booking=bd3.id_booking
        //                 WHERE j.jenis_jadwal IN(0)
        //                 GROUP BY j.id_jadwal
        //     )ss ORDER BY jam,DATE_FORMAT(tgl_urut,'%Y-%m-%d') DESC");

        
        // $f_arr = array();
        // foreach($e_data_petugas as $e){
        //     $cek_tanggal = $e->jenis_jadwal;
        //     if ($cek_tanggal=='1') {
        //         $tanggal =  $e->created_at;
        //     }else{
        //         $tanggal =  $e->tanggal;
        //     }
        //     $f_arr[] = array('jadwal'=>tgl_full($tanggal,'1'),
        //                      'jam'=>$e->jam,
        //                      'kapasitas'=>format_angka($this->get_kapasitas($e->id_area)),
        //                      'pesan'=>format_angka($e->pesan),
        //                      'hadir'=>format_angka($e->hadir),
        //                      'tidak_hadir'=>format_angka($e->tidak_hadir),
        //                      'kosong'=>format_angka(($this->get_kapasitas($e->id_area)-$e->pesan)));
        // }
        $data['data_f'] = $f_arr;

        if($id_group == 3){
            return view('contents.dashboard.index')->with('data',$data);
        }else{            
            return view('contents.dashboard.index_petugas')->with('data',$data);    
        }

    }

    public function get_kapasitas($area){
        $d_data = DB::SELECT("SELECT count(*) as kapasitas FROM seats as s
                where s.area IN ($area)");
        return $d_data[0]->kapasitas;
    }

    public function get_tomorow($jadwal,$area,$hari_ini,$besok){
        $d_data = DB::SELECT("SELECT a.id_jadwal, SUM(a.pesan) pesan, SUM(a.hadir) hadir, SUM(a.tidak_hadir) tidak_hadir, SUM(a.kapasitas) kapasitas, SUM(a.kapasitas) kapasitas, SUM(a.kapasitas-a.pesan) kosong FROM (
            SELECT '$jadwal' as id_jadwal,COUNT(*) as pesan,'0' as hadir,'0' AS tidak_hadir, '0' as kapasitas FROM booking AS b 
            LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking 
            WHERE bd.status='0' AND b.tgl_show >= '$hari_ini' AND b.tgl_show <= '$besok' AND b.id_jadwal = '$jadwal'
            UNION ALL
            SELECT '$jadwal' as id_jadwal,'0' as pesan, COUNT(*) as hadir, '0' AS tidak_hadir, '0' as kapasitas FROM booking AS b 
            LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking 
            WHERE bd.status='0' AND b.tgl_show >= '$hari_ini' AND b.tgl_show <= '$besok' AND b.id_jadwal = '$jadwal'
            UNION ALL
            SELECT '$jadwal' as id_jadwal,'0' as pesan,'0' as hadir, COUNT(*) AS tidak_hadir, '0' as kapasitas FROM booking AS b 
            LEFT JOIN booking_detail AS bd ON b.id_booking=bd.id_booking 
            WHERE bd.status='0' AND b.tgl_show >= '$hari_ini' AND b.tgl_show <= '$besok' AND b.id_jadwal = '$jadwal'
            UNION ALL
            SELECT '$jadwal' as id_jadwal, '0' as pesan,'0' as hadir, COUNT(*) AS tidak_hadir, count(*) as kapasitas FROM seats as s
            where s.area IN ($area)
            ) as a GROUP BY a.id_jadwal");
        return $d_data;
    }

    public function get_grafik(Request $request){
        $d_data = DB::SELECT("SELECT b.id_booking, j.id_area, b.id_jadwal, b.tgl_booking, b.tgl_show, j.tanggal, j.jam_mulai as jam, CASE WHEN sum(bd.hadir) IS NULL THEN '0' ELSE sum(bd.hadir) END as hadir, 
            CASE WHEN sum(bd2.tidak_hadir) IS NULL THEN '0' ELSE sum(bd2.tidak_hadir) END as tidak_hadir,
            CASE WHEN sum(bd3.pesan) IS NULL THEN '0' ELSE sum(bd3.pesan) END as pesan  
            FROM booking as b
            LEFT JOIN jadwal AS j ON j.id_jadwal = b.id_jadwal
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as hadir FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking WHERE bd.status='1' GROUP BY b.id_jadwal
            ) as bd ON b.id_booking=bd.id_booking
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as tidak_hadir FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking WHERE bd.status='0' GROUP BY b.id_jadwal
            ) as bd2 ON b.id_booking=bd2.id_booking
            LEFT JOIN ( SELECT b.id_booking, COUNT(*) as pesan FROM booking as b LEFT JOIN booking_detail as bd ON b.id_booking=bd.id_booking GROUP BY b.id_jadwal
            ) as bd3 ON b.id_booking=bd3.id_booking
            GROUP BY b.id_jadwal, b.tgl_show
            ORDER BY b.id_booking DESC");

        $tanggal = array();
        $hadir   = array();
        foreach($d_data as $d){
            $tanggal[]  = tgl_full($d->tgl_show,'1').' '.$d->jam;
            $pesan[]    = format_angka($d->pesan);
            $hadir[]    = format_angka($d->hadir);
            $tidak_hadir[] = format_angka($d->tidak_hadir);
        }
        // print_r($arr);exit();
            $s['labels']=$tanggal;

            $singkatan = ["Pesan","Hadir","Tidak Hadir"];

            //OTG
            $data["Pesan"]=$pesan;

            //ODP
            $data["Hadir"]=$hadir;

            //PDP
            $data["Tidak Hadir"]=$tidak_hadir;

            //CONFIRMED
            // $data ["CONFIRMED"]=[1,10,32,50];

        $d_warna = [
            '#253138c7', //OTG
            '#2ab2bd', //ODP
            '#f9a81e', //PDP
            // 'rgba(255,0,0, 1)', //Confirm
        ];

        foreach ($d_warna as $key => $value) {
            $s['dataset'][] = array(
                "label" => $singkatan[$key],
                "data"  => $data[$singkatan[$key]],
                "fill" => false,
                "backgroundColor" => $d_warna[$key],
                "borderColor" => $d_warna[$key],
            );
        }

        //echo json_encode($s);
        return response()->json($s);
    }

    public function get_data_kursi($id){
        $where = '';
        if(isset($id)){
            $where = "WHERE bd.id_booking='".$id."'";
        }
        $d_data = DB::select("SELECT bd.*, s.kolom as kolom, a.nama as nama_area, mg.nama as nama_gate FROM booking_detail AS bd JOIN seats AS s ON bd.id_seat=s.id JOIN area AS a ON s.area=a.id JOIN master_gate AS mg ON a.id_gate=mg.id_gate $where");

        $d_kursi = array();
        $d_gate = '';
        $d_area = '';
        foreach($d_data as $d){
            $d_kursi[] = trim(strtolower($d->nama_area),'area ').$d->kolom;
            $d_gate = $d->nama_gate;
            $d_area = $d->nama_area;
        }
        $kursi = '';
        if(count($d_kursi)>0){
        $kursi = $d_gate.' - '.$d_area.' ('.implode(',',$d_kursi).')';
        }

        $arr = $kursi;
        return $arr;
    }

    public function show(){
    	// return view('contents.dashboard.index');
    	return view('templates.index');
    }

}
