<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Redirect;


class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
 

    public function api_ticket($id){
        // $id = "39";

        $d_data = DB::table('booking as b')->join('member as m','b.id_member','m.id')->join('jadwal as j','b.id_jadwal','j.id_jadwal')->join('locations as l','j.id_location','l.id')->where(['id_booking'=>$id])->select(DB::raw('b.*,m.nama as nama_ktp, m.nik as no_ktp, j.nama as nama_jadwal,j.keterangan, b.tgl_show AS waktu, j.jam_mulai as jam, l.nama as nama_lokasi'))->first();
        
       
        foreach ($d_data as $key => $value) {
            // $d->hari="aaa";
            $data['data']['hari'] = tgl_full($d_data->waktu, '1');
            $data['data'][$key] = $value;
        }


        $d_detail = DB::table('booking_detail as b')->join('seats as s','b.id_seat','s.id')->join('area as a','a.id','s.area')->join('master_gate as mg','mg.id_gate','a.id_gate')->leftjoin('member as m','b.id_member','m.id')->where(['b.id_booking'=>$d_data->id_booking])->select(DB::raw('b.*,s.row as baris,s.kolom as kolom,m.nama as nama_ktp, m.nik as no_ktp, a.nama, mg.nama as gate'));
        // dd($d_detail);
        $arr = array();
        if($d_detail->count() > 0){
            foreach ($d_detail->get() as $key => $value) {
               
                   
                    $arr[$key]['kursi']=trim(strtolower($value->nama),'area ').$value->kolom;
                    $arr[$key]['nama']=$value->nama_ktp;
                    $arr[$key]['ktp']=$value->no_ktp;
                    $arr[$key]['area']=$value->nama;
                    $arr[$key]['gate']=$value->gate;
              
            }
        }

        $data['detail'] = $arr;


        return response()->json($data);
    }
}
