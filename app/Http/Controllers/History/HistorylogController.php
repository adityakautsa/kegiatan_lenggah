<?php

namespace App\Http\Controllers\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class HistorylogController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('contents.history.index');
    }

    function get_data(){
        $id_group  = Auth::user()->id_group;
        $id_member = Auth::user()->id_member;
        if($id_group==3){
    	$d_data = DB::table('log_history')->where('id_user','=',$id_member)->orderby('tgl_log','DESC')->select(DB::raw('*'));
        }else{
        $d_data = DB::table('log_history')->orderby('tgl_log','DESC')->select(DB::raw('*'));    
        }
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$arr[] = array('tanggal'=>tgl_full($d->tgl_log,'9'),
    					   'nama_user'=>$d->nama_user,
    					   'status'=>$d->status,
    					   'keterangan'=>$d->keterangan);
    	}

    	return Datatables::of($arr)
        ->make(true);
    }
}
