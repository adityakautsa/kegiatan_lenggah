<?php

namespace App\Http\Controllers\Akun;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Session;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index(){
        $id_user = Auth::user()['id'];
        $id_member = Auth::user()['id_member'];

        $data['member'] = _memberuser()->where(['m.id' => $id_member, "ua.id" => $id_user])->get()->first();

    	return view('contents.akun.profil')->with("data", $data);
    }

    function edit(){
        $id_user = Auth::user()['id'];
        $id_member = Auth::user()['id_member'];
        $data['lokasi'] = get_lokasi()->where("l.tipe", '0')->get();
        $data['prov'] = DB::table("master_prov")->orderBy("name", "asc")->get();
        $data['member'] = _memberuser()->where(['m.id' => $id_member, "ua.id" => $id_user])->get()->first();

        return view('contents.akun.editprofil')->with("data", $data);
    }

    function get_lingkungan(Request $request){
        $id = $request->get("id");
        $parent = $request->get("parent");
        print_r(_lokasiselect($id, $parent));
    }

    function simpan(Request $request){
        // print_r($request->all());

        $id = $request->get('id');

        $d_member = DB::table("member")->where("id", $id)->get();
        $count = $d_member->count();
        $file_sebelum = '';
        if($count > 0){
            $file_sebelum = $d_member->first()->foto;
        }

        $input['nik'] = $request->get('nik');
        $input['nama'] = $request->get('nama');
        $input['tempat_lahir'] = $request->get('tmp_lahir');
        $input['tgl_lahir'] = tgl_full($request->get('tgl_lahir'), '2');
        $input['jenis_kelamin'] = $request->get('jk');
        $input['provinsi'] = $request->get('provinsi');
        $input['kabupaten'] = $request->get('kabupaten');
        $input['kecamatan'] = $request->get('kecamatan');
        $input['desa'] = $request->get('kelurahan');
        $input['alamat'] = $request->get('alamat');
        $input['no_telp'] = $request->get('telepon');
        // $input['id_wilayah'] = $request->get('wilayah');
        // $input['id_lingkungan'] = $request->get('lingkungan');
        $paroki = $request->get('paroki');

        if($paroki != ''){
            if($paroki > 9000){
                $input['id_lokasi'] = $request->get("gereja");
            }else{
                $input['id_lokasi'] = $paroki;
            }
        }

        $count_file = ($request->get('img_foto') != '') ? "1":"";
        $nama_file = '';
        if($count_file > 0){
            $path = "images/member/";
            if (!is_dir($path )) {
                mkdir($path, 0777, true);
            }
            $data_foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->get('img_foto')));
            $nama_file = date("YmdHis").".png";
            
            if($file_sebelum != ''){
                if(file_exists($path.$file_sebelum)){
                    unlink($path.$file_sebelum);
                }
            }

            file_put_contents($path.$nama_file, $data_foto);
            $input['foto'] = $nama_file;
        }

        $d_id = DB::table("member")->where("id", $id)->update($input);
        DB::table("user_accounts")->where("id_member", $id)->update(["id_lokasi" => $input['id_lokasi']]);
        return redirect("profil");
    }

    function rubah_password(){
        return view('contents.akun.editpassword');
    }

    function simpan_password(Request $request){
        $id_user = Auth::user()['id'];
        $id_member = Auth::user()['id_member'];

        $member = _memberuser()->where(['m.id' => $id_member, "ua.id" => $id_user])->select("ua.password")->get()->first();

        $pass_lama = $request->get("password_lama");
        $update['password'] = $request->get("password");
        $update['retype_password'] = $request->get("password");

        $data = array();
        if($member->password == $pass_lama){
            DB::table("user_accounts")->where(["id_member" => $id_member, "id" => $id_user])->update($update);
            return redirect()->route("rubah_password")->with("success", "Password berhasil diganti");
        }else{
            return redirect()->route("rubah_password")->with("error", "Gagal merubah password, password lama tidak sesuai");
        }
    }
}