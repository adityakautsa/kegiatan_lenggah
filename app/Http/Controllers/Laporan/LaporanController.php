<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Laporan2ViewExport;

class LaporanController extends Controller
{
    public $path;
    public $dimensions;

    public function __construct()
    {  
        $this->middleware('auth');
    }

    public function index(){
        $max = DB::table("master_rangetanggal")->where("id", '1')->get()->first()->range_tanggal;
        $data['tgl'] = date('d-m-Y', strtotime(date('Y-m-d'). '+'.$max.' day'));
    	return view('contents.laporan.index')->with("data", $data);
    }

    function laporan_pengunjung(Request $request){
        $tgl_show = date("Y-m-d");
        if(!empty($request->get('tanggal'))){
            $tgl_show = tgl_full($request->get('tanggal'), 99);
        }
        $id_jadwal = $request->get("jadwal");
        $id_gate = $request->get("gate");
        $id_area = $request->get("area");

        $where[] = "bo.id_booking IS NOT NULL";
        $where[] = "j.id_jadwal = '".$id_jadwal."'";
        $where[] = "g.id_gate = '".$id_gate."' ";
        $where_jumlah = "";
        $select = " 'Semua Area' nama_area ";
        if($id_area != 0){
            $where[] = "s.area = '".$id_area."' ";
            $where_jumlah = " AND s.area IN ($id_area)";
            $select = "d.nama_area";
        }
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
        $where = "WHERE ".implode(" AND ", $where);
        $condition['where'] = $where;
        $condition['order']  = "ORDER BY g.nama ASC, a.nama ASC, LENGTH(s.baris) ASC, s.baris ASC, LENGTH(s.deret) ASC, s.deret ASC";
        $condition['select'] = ", g.id_gate, g.nama nama_gate";
        $d_map = get_seat_detail($condition, $tgl_show);

        $d_judul = get_show()->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('".$tgl_show."', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END AND j.id_jadwal = '".$id_jadwal."'")->orderby("j.jam_mulai", "asc")->get();
        $c_judul = $d_judul->count();

        $judul = "";
        if($c_judul > 0){
            $dj = $d_judul->first();
            $judul = tgl_full($dj->tgl_show." ".$dj->jam_mulai, 78);
        }

        $d_jumlah = DB::SELECT("
                    SELECT d.area, $select , sum(d.hadir) as hadir, sum(d.tidak_hadir) as tidak_hadir FROM (
                        SELECT s.area, a.nama as nama_area, CASE WHEN count(*) > 0 THEN '1' ELSE '0' END as hadir, '0' as tidak_hadir  FROM booking_detail as bd join booking as b ON bd.id_booking=b.id_booking join seats as s ON bd.id_seat=s.id join area as a ON s.area=a.id
                        where b.id_jadwal='$id_jadwal' AND b.tgl_show='$tgl_show' AND bd.status = '1' AND a.id_gate = '$id_gate' $where_jumlah GROUP BY b.id_booking,bd.id_seat
                        UNION ALL 
                        SELECT s.area, a.nama as nama_area, '0' as hadir, CASE WHEN count(*) > 0 THEN '1' ELSE '0' END as tidak_hadir FROM booking_detail as bd join booking as b ON bd.id_booking=b.id_booking join seats as s ON bd.id_seat=s.id join area as a ON s.area=a.id
                        where b.id_jadwal='$id_jadwal' AND b.tgl_show='$tgl_show' AND bd.status = '0' AND a.id_gate = '$id_gate' $where_jumlah GROUP BY b.id_booking,bd.id_seat
                    ) as d 
                    
                    ");
        if($d_jumlah[0]->area == ''){
            $d_jumlah = DB::table("area AS a")->where("a.id_gate", $id_gate);
            if($id_area != 0){
                $d_jumlah = $d_jumlah->where("a.id", $id_area)->select(DB::raw("a.id area, a.nama nama_area, '0' hadir, '0' tidak_hadir"));
            }else{
                $d_jumlah = $d_jumlah->select(DB::raw("a.id area, 'Semua Area' nama_area, '0' hadir, '0' tidak_hadir"));
            }

            $dt = $d_jumlah->groupBy("a.id_gate")->get();
            $d_jumlah = $dt;
        }


        $d_data['judul'] = $judul;
        $d_data['data'] = $d_map;
        $d_data['pengunjung'] = $d_jumlah;

        // print_r($d_data);
        return Excel::download(new Laporan2ViewExport($d_data), "Laporan.xls");
    }

    function get_gate(Request $request){
        $id_jadwal = $request->get("id");
        $jenis = $request->get("jenis");

        $d_jadwal = DB::table("jadwal")->where("id_jadwal", $id_jadwal)->get()->first()->id_area;
        $id_area = explode(",", $d_jadwal);
        $d_data = get_area()->whereIn("a.id", $id_area)->orderby("a.nama", "asc");

        if($jenis == 1){
            $d_data = $d_data->groupBy("mg.id_gate");
        }

        $d_data = $d_data->get();

        return response()->json($d_data);
    }

    function get_area(Request $request){
        $id = $request->get("id");
        $id_jadwal = $request->get("jadwal");

        $d_jadwal = DB::table("jadwal")->where("id_jadwal", $id_jadwal)->get();
        $c_jadwal = $d_jadwal->count();
        $d_data = get_area()->where("mg.id_gate", $id)->orderby("a.nama", "asc");
        if($c_jadwal > 0){
            $d_area = $d_jadwal->first()->id_area;
            $d_area = explode(",", $d_area);

            $d_data = $d_data->whereIn("a.id", $d_area);
        }

        $d_data = $d_data->get();

        return response()->json($d_data);
    }

    function get_jam(Request $request){
        $tgl = tgl_full($request->get("tgl"), 2);

        if($tgl == ''){
            $tgl = date("Y-m-d");
        }

        $d_data = get_show()->select(DB::raw("j.*, loc.nama nama_lokasi, CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal, CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('".$tgl."', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show"))->whereraw("CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl."', '%Y-%m-%d')) - 1) END")->orderby("j.jam_mulai", "asc")->get();
        $count = $d_data->count();
        $jadwal = array();
        if($count > 0){
            foreach($d_data as $d){
                $jadwal[] = "<option value='".$d->id_jadwal."'>".$d->jam_mulai."</option>";
            }
        }else{
            $jadwal[] = "<option value=''> -- Pilih Jadwal --</option>";
        }

        print_r($jadwal);
    }
}