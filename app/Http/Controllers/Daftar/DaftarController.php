<?php

namespace App\Http\Controllers\Daftar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Validator;
use Jenssegers\Agent\Agent;

class DaftarController extends Controller
{

    public function __construct()
    {

    }

    public function index(){
        $data['gereja'] = DB::table('locations')->where("tipe", '0')->get()->first();
        $data['paroki'] = DB::table('locations')->where("tipe", '0')->get();
        $data['wilayah'] = DB::table('locations')->where(["tipe" => '1', "parents" => '1'])->get();
    	return view('contents.daftar.index')->with("data", $data);
    }

    function simpan(Request $request){
    	$validator = Validator::make($request->all(), [
            'captcha' => 'required|captcha',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => '0', 'ket'=>$validator->errors()->all()]);
        } else 

    	$hp = $request->get('hp');
    	$nama = $request->get('nama');
    	$jenkel = $request->get('jenkel');
    	$username = $request->get('username');
    	$pass_ = $request->get('pass_');
        $alamat = $request->get('alamat');
        $wilayah = $request->get('wilayah');
        $lingkungan = $request->get('lingkungan');
        $lokasi = $request->get("paroki");

        if($lokasi > 9000){
            $lokasi = $request->get("paroki_text");
            $wilayah = '0';
            $lingkungan = '0';
        }

    	$d_member = DB::table("member")->where("no_telp", $hp)->get()->count();
    	$arr = array();
    	if($d_member == 0){

            $count_file = ($request->get('img_foto') != '') ? "1":"";
            $nama_file = '';
            if($count_file > 0){
                $path = "images/member/";
                if (!is_dir($path )) {
                    mkdir($path, 0777, true);
                }
                $data_foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->get('img_foto')));
                $nama_file = date("YmdHis").".png";
                
                file_put_contents($path.$nama_file, $data_foto);
                $member['foto'] = $nama_file;
            }
    		$member['nik'] = '';
    		$member['nama'] = $nama;
            $member['no_telp'] = $hp;
    		$member['jenis_kelamin'] = $jenkel;
            $member['alamat'] = ($alamat == '') ? "":$alamat;
            $member['id_wilayah'] = $wilayah;
            $member['id_lingkungan'] = $lingkungan;
    		$member['tgl_registrasi'] = date('Y-m-d');
    		$member['jenis_daftar'] = '0';
    		$member['is_aktif'] = '0';
    		$member['is_petugas'] = '0';
            $member['id_lokasi'] = $lokasi;

    		$id_mem = DB::table("member")->insertGetId($member);

            if($id_mem != ''){
    		$akun['nik'] = '';
    		$akun['nama'] = $nama;
    		$akun['username'] = $username;
    		$akun['password'] = $pass_;
            $akun['no_telepon'] = $hp;
    		$akun['retype_password'] = $pass_;
    		$akun['active'] = '0';
    		$akun['id_lokasi'] = $lokasi;
    		$akun['id_member'] = $id_mem;
    		$akun['id_group'] = '3';

    		$id_akun = DB::table("user_accounts")->insertGetId($akun);
            }else{
                $id_akun = '';
            }

    		if($id_mem != '' && $id_akun != ''){
    			$arr = ['status' => '1', 'ket' => "Pendaftaran sukses"];
    		}else{
                DB::table("member")->delete("id", $id_member);
                $arr = ['status' => '0', 'ket' => "Pendaftaran gagal dilakukan"];
            }
    	}else{
    		$arr = ['status' => '0', 'ket' => "Nomer HP sudah pernah didaftarkan"];
    	}
        
    	return json_encode($arr);
    }

    public function refreshCaptcha()
    {
        return captcha_img('flat');
    }

    function get_lingkungan(Request $request){
        $id = $request->get("id");
        $parent = $request->get("parent");
        print_r(_lokasiselect($id, $parent));
    }
}