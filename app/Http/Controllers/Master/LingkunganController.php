<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class LingkunganController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['lokasi'] = DB::table("locations")->where("tipe", '0')->orderBy("nama", "asc")->get()->first();
        $data['wilayah'] = DB::table("locations")->where("tipe", '1')->orderBy("nama", "asc")->get();
    	$data['prov'] = DB::table("master_prov")->orderBy("name", "asc")->get();
    	return view('contents.master_lokasi.lingkungan')->with('data', $data);
    }

    function simpan(Request $request){
    	$id = $request->get('popup_id');
    	$data['nama'] = $request->get('popup_lokasi');
    	$data['provinsi'] = $request->get('popup_prov');
    	$data['kabupaten'] = $request->get('popup_kab');
    	$data['kecamatan'] = $request->get('popup_kec');
    	$data['desa'] = $request->get('popup_kel');
    	$data['alamat'] = $request->get('popup_alamat');
    	$data['no_telp'] = $request->get('popup_telepon');
    	$data['is_aktif'] = $request->get('popup_aktif');
        $data['parents'] = $request->get('popup_parent');
        $data['tipe'] = '2';
    	
    	if($id == ''){
    		$id = DB::table('locations')->insertGetId($data);
    	}else{
    		DB::table("locations")->where("id", $id)->update($data);
    	}

    	echo json_encode(["status" => "1"]);
    }

    function get_kabupaten(Request $request){
    	$id_prop = $request->get("id");
    	print_r(_kabupatenselect($id_prop));
    }

    function get_kecamatan(Request $request){
    	$id_kab = $request->get("id");
    	print_r(_kecamatanselect($id_kab));
    }

    function get_kelurahan(Request $request){
    	$id_kab = $request->get("id");
    	print_r(_kelurahanselect($id_kab));
    }

    function get_data(){
    	$d_data = get_lokasi()->join("locations AS l2", "l.parents", "l2.id")->join("locations AS l3", "l2.parents", "l3.id")->where("l.tipe", '2')->select(DB::raw("l.*, prop.name AS nama_prov, kab.name AS nama_kab, kec.name AS nama_kec, kel.name AS nama_kel, l2.nama nama_parent, l2.nama nama_wilayah, l3.nama nama_parent"))->orderby("l3.nama", "asc")->orderby("l2.nama", "asc")->orderby("l.nama", "asc");
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
    		$d->status = is_aktif($d->is_aktif);
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status'])
        ->make(true);
    }

    function get_edit(Request $request){
		$id = $request->get('id');

		$d_data = get_lokasi()->where('l.id', $id)->get();

		echo json_encode($d_data);
    }

    function hapus(Request $request){
    	$id = $request->get("id");

    	DB::table('locations')->where("id", $id)->delete();
    	$d_data = DB::table('locations')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }
}