<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class ProvinsiController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('contents.master_provinsi.index');
    }

    public function get_data(){
    	$d_data = DB::table("master_prov")->orderby("name", "asc");
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$arr[] = ["id" => $d->id,
    				"nama" => $d->name,
    				"aksi" => "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>".
    					'<input type="hidden" id="table_id'.$d->id.'" value="'.$d->id.'">'.
    					'<input type="hidden" id="table_nama'.$d->id.'" value="'.$d->name.'">'];
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi'])
        ->make(true);
    }

    function simpan(Request $request){
    	$id = $request->get("popup_id");
    	$insert['name'] = $request->get("popup_nama");

    	if($id == ''){
    		DB::table('master_prov')->insert($insert);
    	}else{
    		DB::table("master_prov")->where("id", $id)->update($insert);
    	}

    	echo json_encode(["status" => '1']);
    }

    function hapus(Request $request){
    	$id = $request->get("id");

    	DB::table('master_prov')->where("id", $id)->delete();
    	$d_data = DB::table('master_prov')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }
}