<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class SeatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data['lokasi'] = DB::table('locations')->Select(DB::raw('*'))->where("tipe", '0')->get()->first();
    	return view('contents.master_seat.index')->with('data',$data);
    }

    public function get_ruang($id){
        $data = DB::table('rooms')->where(['id_locations'=>$id])->Select(DB::raw('*'))->get();
        echo json_encode($data);
    }

    public function get_area($id){
        $data = DB::table('area')->where(['id_lokasi'=>$id])->Select(DB::raw('*'))->get();
        $arr = array();
        foreach($data as $d){
            $d->digit = filter_number($d->nama);
            $arr[] = $d;
        }
        echo json_encode($arr);
    }

    public function show(){
    	$tgl = date("Y-m-d");
    	$d_data = DB::table('seats as s')->leftjoin('locations as l','s.id_locations','l.id')->leftjoin("area as a", function($join){
            $join->on("a.id_lokasi", "l.id")
            ->on("s.area", "a.id");
        })->select(DB::raw("s.*,l.nama as nama_lokasi, a.nama AS nama_area"))->orderBy("l.nama", "ASC")->orderBy("a.nama", "ASC");
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$arr[] = ["id_seat" => $d->id,
                    "nama_lokasi" => $d->nama_lokasi,
    				"nama_kursi" => $d->row.$d->kolom,
    				"area" => $d->area,
                    "nama_area" => $d->nama_area,
    				"baris" => $d->row,
    				"kolom" => $d->kolom,
    				"is_aktif" => $d->is_aktif,
                    "text_aktif" => is_aktif($d->is_aktif),
    				"aksi" => "<div class='btn-group'><button type='button' class='btn btn-icon btn-danger' onclick='btnHapusData(".$d->id.")'><i class='icon ion-ios-trash'></i></button></div>
                        <input type='hidden' value='".$d->id."' name='table_id' id='table_id".$d->id."'>
                        <input type='hidden' value='".$d->id_locations."' name='table_lokasi' id='table_lokasi".$d->id."'>
                        <input type='hidden' value='".$d->nama."' name='table_nama' id='table_nama".$d->id."'>
                        <input type='hidden' value='".$d->area."' name='table_area' id='table_area".$d->id."'>
                        <input type='hidden' value='".$d->baris."' name='table_baris' id='table_baris".$d->id."'>
                        <input type='hidden' value='".$d->deret."' name='table_deret' id='table_deret".$d->id."'>
                        <input type='hidden' value='".$d->is_aktif."' name='table_isaktif' id='table_isaktif".$d->id."'>"];
    	}
        //<button type='button' class='btn btn-icon btn-default' onclick='btnEditData(".$d->id.")'><i class='icon ion-md-create'></i></button>

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'text_aktif'])
        ->make(true);
    }

    public function hapus(Request $request){
    	$id = $request->get('id');

        $log_s = DB::table('seats')->where('id','=',$id)->first();
        trigger_log($id,'seats','Kursi','Hapus Kursi '.$log_s->row.$log_s->kolom,3,1);

		$query = DB::table('seats')->where(['id'=>$id])->delete();

		if($query){
			$result = [
				"type"=>"success",
				"text"=>"Data berhasil dihapus"
			];
		}else{
			$result = [
				"type"=>"error",
				"text"=>"Terjadi kesalahan!"
			];
		}

		echo json_encode($result);
    }

    function simpan(Request $request){
        $id_seat = $request->get('popup_id');
        $data['nama'] = $request->get('popup_nama');
        $data['id_lokasi'] = $request->get('popup_lokasi');
        // $data['id_ruang'] = $request->get('popup_ruang');
        $data['area'] = $request->get('popup_area');
        $data['jum_baris'] = $request->get('popup_baris');
        $data['jum_kolom'] = $request->get('popup_kolom');
        $data['is_aktif'] = $request->get('popup_aktif');

        $table_nama = $request->get('popup_nama');
        $table_id_locations = $request->get('popup_lokasi');
        // $table_id_rooms = $request->get('popup_ruang');
        $table_area = $request->get('popup_area');
        $table_jum_baris = $request->get('popup_baris');
        $table_jum_deret = $request->get('popup_kolom');
        $table_is_aktif = $request->get('popup_aktif');
        
        DB::beginTransaction();
        try {
            if($id_seat == ''){
                $id = DB::table('seats_induk')->insertGetId($data);
                $log = DB::table('area')->where('id','=',$table_area)->first();
                trigger_log($id,'seats_induk','Kursi','Tambah kursi dengan Nama Area = '.$log->nama.' ('.$table_jum_baris.'x'.$table_jum_deret.')',1,1);

                $jum_baris = $request->get('popup_baris');
                $jum_deret = $request->get('popup_kolom');
                
                for($i=1;$i<=$jum_baris;$i++){
                    $count_akhir = '0';
                    $angka_baris = '0';
                    for ($x=1; $x<=$jum_deret; $x++) {

                        $cek_data = DB::table("seats")->where(["area" => $data['area'], "row" => $table_nama+($i-1), "is_aktif" => '1'])->select(DB::raw("COUNT(*) jum_akhir, CASE WHEN COUNT(*) = 0 THEN '0' ELSE baris END AS angka_baris"))->get();
                        $c_cdata = $cek_data->count();
                        
                        if($c_cdata > 0){
                            // if($count_akhir == ''){
                            //     $count_akhir = 0;                                   
                            // }else{
                            //     $count_akhir = $cek_data->first()->jum_akhir; 
                            // }
                            
                            // if($angka_baris == ''){
                            //     $angka_baris = 0;                                
                            // }else{
                            //     $angka_baris = ($cek_data->first()->angka_baris == 0) ? 0:$cek_data->first()->angka_baris;
                            // }

                            // if($count_akhir == 0){
                            //     $count_akhir = ($cek_data->first()->jum_akhir == 0) ? 1:$cek_data->first()->jum_akhir;
                                
                            // }

                            // if($angka_baris == 0){
                            //     $angka_baris = ($cek_data->first()->angka_baris == 0) ? 1:$cek_data->first()->angka_baris;
                            // }

                            if($cek_data->first()->jum_akhir == 0){
                                $kolom = $this->number_to_alphabet($x);
                                $deret = $x;
                            }else{
                                $kolom = $this->number_to_alphabet($cek_data->first()->jum_akhir+1);
                                $deret = $cek_data->first()->jum_akhir+1;
                            }

                            $baris = $i+($angka_baris);
                            if($cek_data->first()->angka_baris == 0){
                                $angka_baris = 0;
                                $d_baris = DB::table("seats")->where(["area" => $data['area'], "row" => $table_nama+($i-2), "is_aktif" => '1'])->select(DB::raw("COUNT(*) jum_akhir, CASE WHEN COUNT(*) = 0 THEN '0' ELSE baris END AS angka_baris"))->get();
                                if($d_baris->first()->jum_akhir == 0){
                                    $baris = $i+($angka_baris);
                                }else{
                                    $baris = $d_baris->first()->angka_baris+1;
                                }
                            }else{
                                $angka_baris = $cek_data->first()->angka_baris;
                                $baris = $angka_baris;
                            }
                            
                        }

                        $detail[$i][$x]['nama'] = $table_nama;
                        $detail[$i][$x]['id_locations'] = $table_id_locations;
                        $detail[$i][$x]['is_aktif'] = $table_is_aktif;
                        $detail[$i][$x]['area'] = $table_area;
                        $detail[$i][$x]['row'] = $table_nama+($i-1);
                        $detail[$i][$x]['kolom'] = $kolom;
                        $detail[$i][$x]['baris'] = $baris;
                        $detail[$i][$x]['deret'] = $deret;
                        $detail[$i][$x]['id_seat_induk'] = $id;

                        DB::table('seats')->insert($detail[$i][$x]);
                        
                    }
                }
            }else{
                DB::table('seats_induk')->where(array('id' => $id_seat))->update($data);
                DB::table('seats')->where(array('id' => $id_seat))->update();
                for($i=1;$i<=$jum_baris;$i++){
                    for ($x=1; $x<=$jum_deret; $x++) {
                        // echo 'no('.$i.') = ',$y = $this->number_to_alphabet($x),'; ';
                        // echo PHP_EOL;
                        $detail[$i][$x]['nama'] = $table_nama[$i];
                        $detail[$i][$x]['id_locations'] = $table_id_locations[$i];
                        $detail[$i][$x]['is_aktif'] = $table_is_aktif;
                        $detail[$i][$x]['area'] = $table_area[$i];
                        $detail[$i][$x]['row'] = $table_nama+($i-1);
                        $detail[$i][$x]['kolom'] = $this->number_to_alphabet($x);
                        $detail[$i][$x]['baris'] = $i;
                        $detail[$i][$x]['deret'] = $x;
                        $detail[$i][$x]['id_seat_induk'] = $id_seat;
                        DB::table('seats')->insert($detail);
                    }
                }
            }

            DB::commit();   
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    function number_to_alphabet($number) {
        $number = intval($number);
        if ($number <= 0) {
            return '';
        }
        $alphabet = '';
        while($number != 0) {
            $p = ($number - 1) % 26;
            $number = intval(($number - $p) / 26);
            $alphabet = chr(65 + $p) . $alphabet;
        }
        return $alphabet;
    }

    function alphabet_to_number($string) {
        $string = strtoupper($string);
        $length = strlen($string);
        $number = 0;
        $level = 1;
        while ($length >= $level ) {
            $char = $string[$length - $level];
            $c = ord($char) - 64;        
            $number += $c * (26 ** ($level-1));
            $level++;
        }
        return $number;
    }




}
