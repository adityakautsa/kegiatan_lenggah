<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class MemberController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['lokasi'] = get_lokasi()->where("tipe", "0")->get();
    	$data['prov'] = DB::table("master_prov")->orderBy("name", "asc")->get();
    	return view('contents.master_member.index')->with('data', $data);
    }

    function simpan(Request $request){
    	$id = $request->get('popup_id');
    	$data['nik'] = "";
    	$data['nama'] = $request->get('popup_nama');
    	$data['provinsi'] = $request->get('popup_prov');
    	$data['kabupaten'] = $request->get('popup_kab');
    	$data['kecamatan'] = $request->get('popup_kec');
    	$data['desa'] = $request->get('popup_kel');
    	$data['alamat'] = $request->get('popup_alamat');
    	$data['no_telp'] = $request->get('popup_telepon');
    	$data['is_aktif'] = $request->get('popup_aktif');
    	$data['is_petugas'] = '0';
    	// $data['id_lokasi'] = $request->get('popup_lokasi');
        // $data['id_wilayah'] = $request->get('popup_wilayah');
        // $data['id_lingkungan'] = $request->get('popup_lingkungan');
        $data['is_blacklist'] = $request->get('popup_blacklist');
        $data['tgl_registrasi'] = "0000-00-00";
        $data['tgl_aktivasi'] = "0000-00-00";

        $d_cek = DB::table("member")->where("no_telp", $data['no_telp'])->get()->count();
    	$arr = array();
        if($d_cek > 0){
            $arr = ["status" => '0', "keterangan" => "Nomor HP sudah pernah didaftarkan"];
        }else{
    	if($id == ''){
    		$id = DB::table('member')->insertGetId($data);
            trigger_log($id, 'member', 'Member', 'Tambah member '.$request->get('popup_nama'),1,1);  
    	}else{
            DB::table("user_accounts")->where("id_member", $id)->update(["active" => $data['is_aktif']]);
    		DB::table("member")->where("id", $id)->update($data);
            trigger_log($id, 'member', 'Member', 'Edit member '.$request->get('popup_nama'),2,1); 
    	}
            $arr = ["status" => "1", "keterangan" => "Data tersimpan"];
        }

    	echo json_encode($arr);
    }

    function get_kabupaten(Request $request){
    	$id_prop = $request->get("id");
    	print_r(_kabupatenselect($id_prop));
    }

    function get_kecamatan(Request $request){
    	$id_kab = $request->get("id");
    	print_r(_kecamatanselect($id_kab));
    }

    function get_kelurahan(Request $request){
    	$id_kab = $request->get("id");
    	print_r(_kelurahanselect($id_kab));
    }

    function get_data(){
    	$d_data = get_member();
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
    		$d->status = is_aktif($d->is_aktif);
    		$d->petugas = is_petugas($d->is_petugas);
            $d->blacklist = is_blacklist($d->is_blacklist);
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status', 'petugas', 'blacklist'])
        ->make(true);
    }

    function get_edit(Request $request){
		$id = $request->get('id');

		$d_data = get_member()->where('m.id', $id)->get();

		echo json_encode($d_data);
    }

    function hapus(Request $request){
    	$id = $request->get("id");

        $log = DB::table('member')->where("id", $id)->first();
        trigger_log($id, 'member', 'Member', 'Hapus member '.$log->nama,3,1); 

    	DB::table('member')->where("id", $id)->delete();
        DB::table('user_accounts')->where("id_member", $id)->delete();
    	$d_data = DB::table('member')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }

    function get_lingkungan(Request $request){
        $id = $request->get("id");
        $parent = $request->get("parent");
        print_r(_lokasiselect($id, $parent));
    }
}