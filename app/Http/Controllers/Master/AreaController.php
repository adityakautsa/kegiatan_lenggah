<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class AreaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['lokasi'] = DB::table("locations")->orderBy("nama", "asc")->where("is_aktif", '1')->where("tipe", "0")->get()->first();
        $data['pintu'] = DB::table("master_gate")->orderby("nama", "asc")->get();
    	return view('contents.master_area.index')->with('data', $data);
    }

    function get_ruang(Request $request){
    	$id_lokasi = $request->get("id");
    	print_r(_ruangselect($id_lokasi));
    }

    function simpan(Request $request){
    	$id = $request->get('popup_id');

        $d_member = DB::table("area")->where("id", $id)->get();
        $count = $d_member->count();
        $file_sebelum = '';
        if($count > 0){
            $file_sebelum = $d_member->first()->foto;
        }
        $count_file = ($request->get('img_foto') != '') ? "1":"";
        $nama_file = '';
        if($count_file > 0){
            $path = "images_area/";
            if (!is_dir($path )) {
                mkdir($path, 0777, true);
            }
            $data_foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->get('img_foto')));
            $nama_file = date("YmdHis").".png";
            
            if($file_sebelum != ''){
                if(file_exists($path.$file_sebelum)){
                    unlink($path.$file_sebelum);
                }
            }

            file_put_contents($path.$nama_file, $data_foto);
            $data['foto'] = $nama_file;
        }

    	$data['id_lokasi'] = $request->get('popup_gereja');
        $data['id_gate'] = $request->get('popup_pintu');
    	$data['nama'] = $request->get('popup_area');
    	
    	if($id == ''){
    		$id = DB::table('area')->insertGetId($data);
    	}else{
    		DB::table("area")->where("id", $id)->update($data);
    	}

    	echo json_encode(["status" => "1"]);
    }

    function get_data(){
    	$d_data = get_area()->orderBy("l.nama", "asc")->orderBy("mg.nama", "asc");
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status'])
        ->make(true);
    }

    function get_edit(Request $request){
		$id = $request->get('id');

		$d_data = get_area()->where('a.id', $id)->get();
        $arr = array();
        $file_foto = "";
        foreach($d_data as $d){
            $foto = $d->foto;
            if($foto != ""){
                $f = asset('images_area')."/".$d->foto;
                // if(file_exists($f)){
                    $file_foto = $f;
                // }
            }
            $d->file_foto = $file_foto;
            $arr[] = $d;
        }

		echo json_encode($arr);
    }

    function hapus(Request $request){
    	$id = $request->get("id");
        $d_foto = DB::table('area')->where("id", $id)->get()->first()->foto;

        $path = "images_area/";
        unlink_file($path, $d_foto);

    	DB::table('area')->where("id", $id)->delete();
    	$d_data = DB::table('area')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }
}
