<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class RuangController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['lokasi'] = get_lokasi()->get();
    	return view('contents.master_ruang.index')->with('data', $data);
    }

    function simpan(Request $request){
    	$id = $request->get('popup_id');
    	$data['id_locations'] = $request->get('popup_lokasi');
    	$data['nama'] = $request->get('popup_ruang');
    	$data['is_aktif'] = $request->get('popup_aktif');
    	
    	if($id == ''){
    		$id = DB::table('rooms')->insertGetId($data);
    	}else{
    		DB::table("rooms")->where("id", $id)->update($data);
    	}

    	echo json_encode(["status" => "1"]);
    }

    function get_data(){
    	$d_data = get_ruang();
        
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$d->aksi = "<div class='btn-group' role='group'><button class='btn btn-icon btn-warning' type='button' data-id='".$d->id."' onclick='edit($(this))'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-icon btn-danger' type='button' data-id='".$d->id."' onclick='hapus($(this))'><i class='fa fa-trash-o'></i></button></div>";
    		$d->status = is_aktif($d->is_aktif);
    		$arr[] = $d;
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi', 'status'])
        ->make(true);
    }

    function get_edit(Request $request){
		$id = $request->get('id');

		$d_data = get_ruang()->where('r.id', $id)->get();

		echo json_encode($d_data);
    }

        function hapus(Request $request){
    	$id = $request->get("id");

    	DB::table('rooms')->where("id", $id)->delete();
    	$d_data = DB::table('rooms')->where("id", $id)->get()->count();

    	if($d_data == 0){
    		$arr = ['status' => 1, "keterangan" => "Data berhasil dihapus"];
    	}else{
    		$arr = ['status' => 0, "keterangan" => "Data gagal dihapus"];
    	}

    	echo json_encode($arr);

    }
}