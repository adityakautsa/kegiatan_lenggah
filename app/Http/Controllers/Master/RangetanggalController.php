<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;

class RangetanggalController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$data['prov'] = DB::table("master_prov")->orderBy("name", "asc")->get();
    	return view('contents.master_range.index')->with('data', $data);
    }

    public function get_data(){
    	$d_data = DB::table('master_rangetanggal')->offset(0)->limit(1)->first();
    	// print_r($d_data);exit();
    	return response()->json($d_data);
    }

    public function simpan(Request $request){
    	$id = $request->id;
    	$data['range_tanggal'] = $request->range;

    	if($id==''){
    		DB::table('master_rangetanggal')->insert($data);
    	}else{
    		DB::table('master_rangetanggal')->where('id','=',$id)->update($data);
    	}

    	return redirect('rangetanggal');
    }
}
