<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class JadwalController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$auth = Auth::user();
    	$data['lokasi'] = DB::table('locations')->where(['id'=>'1'])->Select(DB::raw('*'))->get();
        $data['wilayah'] = DB::table('locations')->where(['tipe'=>'2','parents'=>$auth->id_lokasi])
                              ->select(DB::raw('*'))->get();
        $data['lingkungan'] = DB::table('locations')->where(['tipe'=>'3'])->select(DB::raw('*'))->get();
        $data['area'] = DB::table('area')->where(['id_lokasi'=>$auth->id_lokasi])->select(DB::raw('*'))->get();
        $data['auth'] = $auth;
    	return view('contents.master_jadwal.index')->with('data',$data);
    }

    public function get_ruang($id){
        $data = DB::table('rooms')->where(['id_locations'=>$id])->Select(DB::raw('*'))->get();
        echo json_encode($data);
    }

    public function get_lingkungan(Request $request){
        $id = $request->get('id');
        $data = DB::table('locations')->where(['tipe'=>'3','parents'=>$id])->SELECT(DB::raw('*'))->get();
        echo json_encode($data);
    }

    public function show(){
        $auth = Auth::user();
    	$d_data = DB::table('jadwal as j')->leftjoin('locations as l','j.id_location','l.id')->where(['id_location'=>$auth->id_lokasi])->select(DB::raw("j.*,l.nama as nama_lokasi"))->orderBy("j.id_jadwal", "ASC");
    	$arr = array();
    	foreach ($d_data->get() as $d) {
    		$arr[] = ["id_jadwal" => $d->id_jadwal,
                    "nama_lokasi" => $d->nama_lokasi,
    				"jenis" => $this->jenis($d->jenis_jadwal),
    				"tanggal" => ($d->jenis_jadwal==0)?tgl_full($d->tanggal,''):"",
    				"hari"=> $this->hari($d->hari),
    				"jam"=>$d->jam_mulai,
    				"nama"=>$d->nama,
    				"aktif"=>$d->is_aktif,
    				"aksi" => "<div class='btn-group' role='group'><button type='button' class='btn btn-icon btn-warning' onclick='edit(".$d->id_jadwal.")'><i class='fa fa-pencil-square-o'></i></button><button type='button' class='btn btn-icon btn-danger' onclick='hapus(".$d->id_jadwal.")'><i class='fa fa-trash-o'></i></button></div>
                        <input type='hidden' value='".$d->id_jadwal."' name='table_id' id='table_id".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->id_location."' name='table_idlokasi' id='table_idlokasi".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->id_area."' name='table_idarea' id='table_idarea".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->jenis_jadwal."' name='table_jenis' id='table_jenis".$d->id_jadwal."'>
                        <input type='hidden' value='".tgl_full($d->tanggal,'')."' name='table_tanggal' id='table_tanggal".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->hari."' name='table_hari' id='table_hari".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->jam_mulai."' name='table_jam' id='table_jam".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->nama."' name='table_nama' id='table_nama".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->keterangan."' name='table_keterangan' id='table_keterangan".$d->id_jadwal."'>
                        <input type='hidden' value='".$d->is_aktif."' name='table_aktif' id='table_aktif".$d->id_jadwal."'>"];
    	}

    	return Datatables::of($arr)
        ->rawColumns(['aksi'])
        ->make(true);
    }

    function simpan(Request $request){
        $id_jadwal = $request->get('popup_id');
        $jenis = $request->get('popup_jenis');
        if($jenis == 0){
        	$tanggal = tgl_full($request->get('popup_tanggal'),'99');
        	$hari = $request->get('popup_hari');
	    }else{
	    	$tanggal = '0000-00-00';
        	$hari = $request->get('popup_hari');
	    }
	    $data['id_location'] = $request->get('popup_lokasi');
        $data['tanggal'] = $tanggal;
        $data['hari']	 = $hari;
        $data['jam_mulai'] = $request->get('popup_jam');
        $data['jam_selesai'] = '00:00';
        $data['jenis_jadwal'] = $jenis;
        $data['nama'] = ($request->get('popup_nama')=='')?'-':$request->get('popup_nama');
        $data['is_aktif']	= $request->get('popup_aktif');
        $data['id_area'] = implode(',',$request->get('popup_area'));
        $data['keterangan'] = $request->get('popup_keterangan');

        DB::beginTransaction();
        try {
            if($id_jadwal == ''){
                DB::table('jadwal')->insert($data);
            }else{
                DB::table('jadwal')->where(array('id_jadwal' => $id_jadwal))->update($data);
            }
            DB::commit();   
        } catch (Exception $e) {
            DB::rollBack();
        }
        return $data;
    }

    public function hapus(Request $request){
        $id = $request->get('id');

        $query = DB::table('jadwal')->where(['id_jadwal'=>$id])->delete();

        if($query){
            $result = [
                "type"=>"success",
                "text"=>"Data berhasil dihapus"
            ];
        }else{
            $result = [
                "type"=>"error",
                "text"=>"Terjadi kesalahan!"
            ];
        }

        echo json_encode($result);
    }

    function hari($id){
    	$hari = array('Minggu','Senin','Selasa','Rabu','Kamis','Jum`at','Sabtu');
    	return $hari[$id];
    }
    function jenis($id){
    	$jenis = array('Khusus','Rutin');
    	return $jenis[$id];
    }
}