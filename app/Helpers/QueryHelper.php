<?php

function get_seat_detail($arr = array(), $date){
	$where = "";
	$where_bo = '';
	$order = '';
	$select = '';
	if(isset($arr['where'])){
		$where = $arr['where'];
	}

	if(!isset($date)){
		$date = date("Y-m-d");
	}

	if(isset($date)){
		$where_bo = "WHERE bo.tgl_show = '".$date."'";
	}

	if(isset($arr['order'])){
		$order = $arr['order'];
	}else{
		$order = "ORDER BY LENGTH(s.baris) ASC, s.baris ASC, LENGTH(s.deret) ASC, s.deret ASC";
	}

	if(isset($arr['select'])){
		$select = $arr['select'];
	}
	
	$d_data = DB::select("SELECT j.id_jadwal, j.tanggal, j.jam_mulai, j.jam_selesai, j.is_aktif, l.nama nama_lokasi, a.nama nama_area, s.nama nama_duduk, s.area, s.row, s.kolom, s.baris, s.deret, s.id_locations,
			CASE WHEN bo.id_booking IS NULL THEN '0' ELSE '1' END AS is_booking,
			CASE WHEN bo.id_booking IS NULL THEN '' ELSE bo.stat END AS status_pesan,
			CASE WHEN m.id IS NULL THEN '' ELSE m.id END AS id_pemesan,
			CASE WHEN m.id IS NULL THEN '' ELSE m.nik END AS nik_pemesan,
			CASE WHEN m.id IS NULL THEN '' ELSE m.nama END AS nama_pemesan, s.id id_duduk,
			CASE WHEN j.jenis_jadwal = 0 THEN 'Khusus' WHEN j.jenis_jadwal = 1 THEN 'Rutin' ELSE '' END AS text_jadwal,
			CASE WHEN j.jenis_jadwal = 1 THEN DATE_FORMAT('$date', '%Y-%m-%d') ELSE j.tanggal END AS tgl_show,
			j.jam_mulai, j.jenis_jadwal $select
			FROM jadwal AS j
			JOIN locations AS l ON j.id_location = l.id
			JOIN area AS a ON l.id = a.id_lokasi
			JOIN seats AS s ON l.id = s.id_locations AND a.id = s.area
			JOIN master_gate AS g ON a.id_gate = g.id_gate
			LEFT JOIN (
				SELECT bo.*, bd.id_seat id_s, bd.id_member id_m, bd.status stat
				FROM booking AS bo
				JOIN booking_detail AS bd ON bo.id_booking = bd.id_booking
				$where_bo
			) AS bo ON j.id_jadwal = bo.id_jadwal AND j.id_location = bo.id_location AND s.id = bo.id_s
			LEFT JOIN member AS m ON bo.id_m = m.id
			$where
			$order");
	return $d_data;
}

function get_riwayat_pesanan($arr = array()){
	$where = "";
	$group_by = "";
	$order_by = "";

	if(isset($arr['where'])){
		$where = $arr['where'];
	}

	if(isset($arr['group_by'])){
		$group_by = $arr['group_by'];
	}

	if(isset($arr['order_by'])){
		$order_by = $arr['order_by'];
	}

	$d_data = DB::select("SELECT b.*, m.nik AS nik_member, m.nama AS nama_member, loc.id id_lokasi, loc.nama AS nama_lokasi, a.id AS id_area, a.nama nama_area, du.row, du.kolom, j.tanggal AS tanggal_jadwal, j.jam_mulai, CASE WHEN jb.jumlah_kursi IS NULL THEN 0 ELSE jb.jumlah_kursi END AS jumlah_kursi
		FROM booking AS b 
		LEFT JOIN booking_detail AS bd ON b.id_booking = bd.id_booking
		LEFT JOIN locations AS loc ON b.id_location = loc.id
		LEFT JOIN seats_induk AS si ON si.id_lokasi = loc.id
		LEFT JOIN area AS a ON si.area = a.id AND a.id_lokasi = si.id_lokasi
		LEFT JOIN member AS m ON b.id_member = m.id
		LEFT JOIN seats AS du ON du.id_seat_induk = si.id AND du.id_locations = loc.id AND bd.id_seat = du.id
		LEFT JOIN jadwal AS j ON j.id_jadwal = b.id_jadwal
		LEFT JOIN (SELECT id_booking, count(*) AS jumlah_kursi FROM booking_detail GROUP BY id_booking) 
		AS jb ON b.id_booking = jb.id_booking
		$where $group_by $order_by
		");
	return $d_data;
}

function jum_pesanmember($id_jadwal, $tgl_show, $id_member){
        $where[] = "j.id_jadwal = '".$id_jadwal."'";
        $where[] = "m.id = '".$id_member."'";
        $where[] = "CASE WHEN j.jenis_jadwal = 0 THEN j.tanggal = '".$tgl_show."' ELSE j.hari = (DAYOFWEEK(DATE_FORMAT('".$tgl_show."', '%Y-%m-%d')) - 1) END";
        $where = "WHERE ".implode(" AND ", $where);
        
        $condition['where'] = $where;
        $d_map = get_seat_detail($condition, $tgl_show);

        return count($d_map);
    }

// ---------------------- Helper Wilayah -----------------------

function _kabupaten($id = ''){
	$where = array();
	if(isset($id)){
		$where += ["id_prov" => $id];
	}
	$d_data = DB::table("master_kab")->orderby("name", "asc")->where($where)->get();
	$arr = array();

	foreach($d_data as $d){
		$arr[] = ["id" => $d->id,
					"name" => $d->name];
	}
	return $arr;
}

function _kabupatenselect($id = ''){
	$where = array();
	if(isset($id)){
		$where += ["id_prov" => $id];
	}else{
		$where += ["id_prov" => ''];
	}
	$d_data = DB::table("master_kab")->orderby("name", "asc")->where($where)->get();
	$arr = array();

	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data as $d){
		$arr[] = "<option value='".$d->id."'>".$d->name."</option>";
	}
	return $arr;
}

function _kecamatanselect($id = ''){
	$where = array();
	if(isset($id)){
		$where += ["id_kab" => $id];
	}else{
		$where += ["id_kab" => ''];
	}
	$d_data = DB::table("master_kec")->orderby("name", "asc")->where($where)->get();
	$arr = array();
	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data as $d){
		$arr[] = "<option value='".$d->id."'>".$d->name."</option>";
	}
	return $arr;
}

function _kelurahanselect($id = ''){
	$where = array();
	if(isset($id)){
		$where += ["id_kec" => $id];
	}else{
		$where += ["id_kec" => ''];
	}
	$d_data = DB::table("master_kel")->orderby("name", "asc")->where($where)->get();
	$arr = array();
	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data as $d){
		$arr[] = "<option value='".$d->id."'>".$d->name."</option>";
	}
	return $arr;
}

// ---------------------- Helper Wilayah -----------------------

// ---------------------- Helper Lokasi -----------------------
function _lokasiselect($id = '', $parent = ''){
	$where = array();
	if(isset($id)){
		$where += ["tipe" => $id];
	}else{
		$where += ["tipe" => ''];
	}

	if(isset($id)){
		$where += ["parents" => $parent];
	}else{
		$where += ["parents" => ''];
	}

	$d_data = DB::table("locations")->orderby("nama", "asc")->where($where)->get();
	$arr = array();
	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data as $d){
		$arr[] = "<option value='".$d->id."'>".$d->nama."</option>";
	}
	return $arr;
}
// ---------------------- Helper Lokasi -----------------------

// ---------------------- Helper Lokasi -----------------------
function get_lokasi(){
	$d_data = DB::table("locations AS l")
	->join("master_prov AS prop", "l.provinsi", "prop.id")
	->join("master_kab AS kab", "l.kabupaten", "kab.id")
	->join("master_kec AS kec", "l.kecamatan", "kec.id")
	->join("master_kel AS kel", "l.desa", "kel.id")
	->orderby("l.nama", "asc")
	->select(DB::raw("l.*, prop.name AS nama_prov, kab.name AS nama_kab, kec.name AS nama_kec, kel.name AS nama_kel"));

	return $d_data;
}
// ---------------------- Helper Lokasi -----------------------

// ---------------------- Helper Member -----------------------
function get_member(){
	$d_data = DB::table("member AS m")
	->leftjoin("master_prov AS prop", "m.provinsi", "prop.id")
	->leftjoin("master_kab AS kab", "m.kabupaten", "kab.id")
	->leftjoin("master_kec AS kec", "m.kecamatan", "kec.id")
	->leftjoin("master_kel AS kel", "m.desa", "kel.id")
	->orderby("m.nama", "asc")
	->select(DB::raw("m.*, prop.name AS nama_prov, kab.name AS nama_kab, kec.name AS nama_kec, kel.name AS nama_kel"));

	return $d_data;
}

function _memberuser(){
	$d_data = DB::table("member AS m")
	->leftjoin("master_prov AS prop", "m.provinsi", "prop.id")
	->leftjoin("master_kab AS kab", "m.kabupaten", "kab.id")
	->leftjoin("master_kec AS kec", "m.kecamatan", "kec.id")
	->leftjoin("master_kel AS kel", "m.desa", "kel.id")
	->join("user_accounts AS ua", "m.id", "ua.id_member")
	->leftjoin("user_groups AS ug", "ua.id_group", "ug.id")
	->leftjoin("locations AS l", "l.id", "m.id_lokasi")
	->leftjoin("locations AS l2", "l2.id", "m.id_wilayah")
	->leftjoin("locations AS l3", "l3.id", "m.id_lingkungan")
	->orderby("m.nama", "asc")
	->select(DB::raw("m.*, ua.id id_user, ua.username, ua.active, CASE WHEN prop.name IS NULL THEN '' ELSE prop.name END AS nama_prov, CASE WHEN kab.name IS NULL THEN '' ELSE kab.name END AS nama_kab, CASE WHEN kec.name IS NULL THEN '' ELSE kec.name END AS nama_kec, CASE WHEN kel.name IS NULL THEN '' ELSE kel.name END AS nama_kel, ug.nama_group, CASE WHEN l.id IS NULL THEN m.id_lokasi ELSE l.nama END AS nama_lokasi, l2.nama nama_wilayah, l3.nama nama_lingkungan"));

	return $d_data;
}
// ---------------------- Helper Member -----------------------

// ---------------------- Helper Ruang -----------------------
function get_ruang(){
	$d_data = DB::table("rooms AS r")
	->join("locations AS l", "r.id_locations", "l.id")
	->orderby("r.nama", "asc")
	->select(DB::raw("r.*, l.nama nama_lokasi"));

	return $d_data;
}

function _ruangselect(){
	$d_data = DB::table("rooms AS r")
	->join("locations AS l", "r.id_locations", "l.id")
	->orderby("r.nama", "asc")
	->where('r.is_aktif', "1")
	->select(DB::raw("r.*, l.nama nama_lokasi"));

	$arr = array();
	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data->get() as $d){
		$arr[] = "<option value='".$d->id."'>".$d->nama."</option>";
	}
	return $arr;
}
// ---------------------- Helper Ruang -----------------------

// ---------------------- Helper Area -----------------------
function get_area(){
	$d_data = DB::table("area AS a")
	->join("locations AS l", "a.id_lokasi", "l.id")
	->join("master_gate AS mg", "a.id_gate", "mg.id_gate")
	->orderby("a.nama", "asc")
	->select(DB::raw("a.*, l.id id_lokasi, l.nama nama_lokasi, mg.nama nama_gate"));

	return $d_data;
}
// ---------------------- Helper Area -----------------------

// ---------------------- Helper User -----------------------
function get_useracc(){
	$d_data = DB::table("user_accounts AS ua")
	->join("user_groups AS ug", "ua.id_group", "ug.id")
	->leftjoin("member AS m", "ua.id_member", "m.id")
	->leftjoin("locations AS loc", "m.id_lokasi", "loc.id")
	->orderby("ua.nama", "asc")
	->select(DB::raw("ua.id AS id_user, ua.username, ua.id_lokasi, loc.nama nama_lokasi, ua.id_group, ug.nama_group, ua.id_member, ua.active, m.*"));

	return $d_data;
}
// ---------------------- Helper User -----------------------

// ---------------------- Helper Roles -----------------------

function _menuselect($id){
	$d_menu = DB::table("user_menu")->where("id_group", $id)->pluck("child_id")->toArray();

	$d_data = DB::table("master_menu")->whereNotIn("id", $d_menu)->get();
	
	$arr = array();
	$arr[] .= "<option value=''>-- Pilih --</option>";
	foreach($d_data as $d){
		$arr[] = "<option value='".$d->id."'>".$d->name.' [ '.$d->url.' ]'."</option>";
	}
	return $arr;
}
// ---------------------- Helper Roles -----------------------

// ---------------------- Helper Menu -----------------------
function menu_item(){
	$data = DB::table("master_menu")->orderby("master_menu.urutan")->orderby("master_menu.parent")->orderby("master_menu.id")->select("master_menu.*")->get();
	// hold all references 
	$ref = [];
	// hold all menu items
	$items = [];

	// loop over the result
	foreach ($data as $key => $value) {
		// Assign by reference 
		$this_ref = &$ref[$value->id];
		    
		// add the menu parent
		$this_ref['id'] 		= $value->id;
		$this_ref['url'] 		= ($value->url!="" && $value->url!=null) ? $value->url : "#";
		$this_ref['name'] 		= $value->name;
		$this_ref['icon'] 		= $value->icon;
		$this_ref['parent'] 	= $value->parent;
		$this_ref['tipe_site'] 	= $value->tipe_site;
		$this_ref['urutan'] 	= $value->urutan;
		$this_ref['modul'] 		= "MENU_".$value->id;
		    
		// if there is no parent push it into items array()
		if($value->parent == 0) {
		    $items[$value->id] = &$this_ref;
		} else {
		    $ref[$value->parent]['child'][$value->id] = &$this_ref;
		}
	}

	return $items;
}

function get_role(){
	$id_grup = Auth::user()->id_group;
		$master_menu = DB::table("master_menu")->get();
		$get_modul = DB::table("user_menu")->where("id_group", $id_grup)->get();

		$master_menu = collect($master_menu)->map(function($x){ return (array) $x; })->toArray();
		$get_modul = collect($get_modul)->map(function($x){ return (array) $x; })->toArray();
		if(sizeof($get_modul)>0){
			$idModul = array_reduce($get_modul, function($carry, $item){
				$carry[] = $item['child_id'];
				return $carry;
			});
		}else{
			$idModul = [];
		}

		$modul = [];
		foreach ($master_menu as $key => $value) {
			# code...
			$modul["MENU_".$value['id']] = in_array($value['id'], $idModul) ? '1111' : '0000';
		}
		//print_r($idModul);exit();
		// $this->session->set_userdata('modul', $modul);
		return $modul;
	}

	function menu_create_limitless($items, $modul, $class = 'sidebar-menu', $segment) {
		$html = "";
	    foreach($items as $key=>$value) {
	    	if( (isset($modul[$value['modul']]) && preg_match("/1\w{3}/", $modul[$value['modul']], $output_array)) ){

		        if(array_key_exists('child',$value)) {
		    		$html .= "";
		        }

		        $a_styled = "";
		    	if(strtolower($segment)===strtolower($value['name'])){
		    		$a_styled .= "color : #FFF; background : #1e282c";
		    	}
		    	$url = ($value['url']=="#")?'javascript:void(0)':url($value['url']);
		        $html.= "<li class='site-menu-item has-sub'><a href='".$url."'><i class='site-menu-icon $value[icon]'></i> <span class='site-menu-title'>$value[name]</span>";
		        

		        if(array_key_exists('child',$value)) {
		          $html .= "<span class='site-menu-arrow'></span> </a><ul class='site-menu-sub'>";
		          $html .= menu_create_limitless($value['child'], $modul, 'treeview-menu', $segment);
		          $html .= "</a></ul>";
		        }
		        
		        $html .= "</a></li>";

		    }
	    }
	    
	    return $html;
	}
// ---------------------- Helper Menu -----------------------
function get_menu(){
	$d_data = DB::table("master_menu as menu")->leftjoin("master_menu as parent", "parent.id", "menu.parent")
			->select(DB::raw("menu.*, parent.name AS parent_menu"));
	return $d_data;
}
// ---------------------- Helper Menu -----------------------
function get_akses($url){
	$id = Auth::user()->id_group;

	$d_akses = DB::table("user_menu AS um")->join("master_menu AS m1", function($join){
            $join->on("m1.id", "um.child_id")
            ->on("m1.parent", "um.parent_id");
        })->leftjoin("master_menu AS m2", "m2.id", "m1.parent")
        ->where("id_group", $id)
        ->where("m1.url", $url)
        ->orderby("m1.id", "asc")
        ->select(DB::raw("um.*, m1.name, m1.icon, m2.name AS parent_menu"));
    $count = $d_akses->get()->count();

    return $count;
}

// ---------------------- Helper Show Jadwal -----------------------
function get_show(){
	$d_data = DB::table("jadwal AS j")->join("locations AS loc", "loc.id", "j.id_location")->WHERE("j.is_aktif", '1');

	return $d_data;
}
// ---------------------- Helper Show Jadwal -----------------------

function trigger_log($id_table,$nama_table,$judul,$keterangan,$status,$show=1){
	if($status==1){
		$c_status = 'Tambah';
	}elseif($status==2){
		$c_status = 'Edit';
	}elseif($status==3){
		$c_status = 'Hapus';
	}elseif($status==4){
		$c_status = 'Scan';
	}elseif($status==5){
		$c_status = 'Download';
	}elseif($status==6){
		$c_status = 'Validasi';
	}elseif($status==7){
		$c_status = 'Cetak';
	}else{
		$c_status = '';
	}
	$id_member = Auth::user()->id_member;
	$member = DB::table('member')->where('id','=',$id_member)->first()->nama;

	$log['id_table'] 	= $id_table;
	$log['nama_table']	= $nama_table;
	$log['id_user']		= $id_member;
	$log['nama_user']	= $member;
	$log['tgl_log']		= date('Y-m-d H:i:s');
	$log['status']		= $c_status;
	$log['nama_log']	= $judul;
	$log['keterangan']	= $keterangan;
	$log['show']		= $show;

	// return $log;
	DB::table('log_history')->insert($log); 
}