function convert_image(var_foto, var_form){
    	var foto = document.getElementById(var_foto);
    	var max_width = 640;
	    var max_height = 480;
	    var form = document.getElementById(var_form);

	    function processfile(file, txt_image) {
	      
	        if( !( /image/i ).test( file.type ) ){
	                alert( "File "+ file.name +" is not an image." );
	                // foto.value = null;
	                return false;
	        }
	            var reader = new FileReader();
	            reader.readAsArrayBuffer(file);
	            
	            reader.onload = function (event) {
	                var blob = new Blob([event.target.result]);
	                window.URL = window.URL || window.webkitURL;
	                var blobURL = window.URL.createObjectURL(blob);
	              
	                var image = new Image();
	                image.src = blobURL;
	                image.onload = function() {
	                    var resized = resizeMe(image); 
	                    var newinput = document.createElement("input");
	                    newinput.type = 'hidden';
	                    newinput.name = txt_image;
	                    newinput.value = resized;
	                    form.appendChild(newinput);
	                }
	            };
	        
	    }

	    function readfiles(files, txt_image) {
	      
	        var existinginputs = document.getElementsByName(txt_image);
	        while (existinginputs.length > 0) { 
	            form.removeChild(existinginputs[0]);
	        } 
	      
	        for (var i = 0; i < files.length; i++) {
	            processfile(files[i], txt_image);
	        }
	    }

	    foto.onchange = function(){
	        if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
	            alert('The File APIs are not fully supported in this browser.');
	            return false;
	        }else{
	             readfiles(foto.files, 'img_foto');
	        }
	    }

	    function resizeMe(img) {
	      
	        var canvas = document.createElement('canvas');
	        var width = img.width;
	        var height = img.height;

	        if (width > height) {
	            if (width > max_width) {
	                height = Math.round(height *= max_width / width);
	                width = max_width;
	            }
	        } else {
	            if (height > max_height) {
	                width = Math.round(width *= max_height / height);
	                height = max_height;
	            }
	        }
	        canvas.width = width;
	        canvas.height = height;
	        var ctx = canvas.getContext("2d");
	        ctx.drawImage(img, 0, 0, width, height);
	      
	        return canvas.toDataURL("image/jpeg",0.7);

	    }
    }