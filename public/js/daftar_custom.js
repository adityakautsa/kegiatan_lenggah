$('#form_input').formValidation({
  framework: "bootstrap4",
  button: {
    selector: '#btn_daftar',
    disabled: 'disabled'
  },
  icon: null,
  fields: {
    nama : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
        stringLength: {
          min: 3,
          message: 'Minimal 3 karakter',
        },
      }
    },
    jenkel : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
      }
    },
    paroki : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
      }
    },
    hp : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
      }
    },
    foto : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
        file: {
          extension: 'jpeg,jpg,png',
          type: 'image/jpeg,image/png',
          message: 'File tidak diijinkan'
        },
      }
    },
    username : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
        stringLength: {
          min: 5,
          message: 'Minimal 5 karakter',
        },
        regexp: {
          regexp: /^[a-zA-Z0-9_]+$/,
          message: 'Gunakan huruf alfabet, angka dan garis bawah',
        },
      }
    },
    pass : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
        stringLength: {
          min: 5,
          message: 'Minimal 5 karakter',
        },
        regexp: {
          regexp: /^[a-zA-Z0-9_]+$/,
          message: 'Gunakan huruf alfabet, angka dan garis bawah',
        },
      }
    },
    pass_ : {
      validators: {
        notEmpty: {
          message: 'Wajib diisi'
        },
        stringLength: {
          min: 5,
          message: 'Minimal 5 karakter',
        },
        regexp: {
          regexp: /^[a-zA-Z0-9_]+$/,
          message: 'Gunakan huruf alfabet, angka dan garis bawah',
        },
        identical: {
          field: 'pass',
          message: 'Password tidak sama'
        }
      }
    },
  },
  err: {
    clazz: 'invalid-feedback'
  },
  control: {
    valid: 'is-valid',
    invalid: 'is-invalid'
  },
  row: {
  invalid: 'has-danger'
  }
})